/* function to draw randomly an integer*/
function getRndInteger(min, max) {
  return Math.floor(Math.random() * (max - min) ) + min;
}


/* calculate feedback and save results */

function provideFeedback(numCustomer, image){
    let count = -1;  // because inserting myMenu is counted
    let sorted2 = "";
    return function(event, ui) {
    study.feedback.classList.remove('alert-danger', 'empty', 'alert-warning', 'invisible', 'alert');
    study.feedback.innerHTML=('');
    study.tabVis.classList.remove('invisible');
    countAction = ++count;
    var result = $(this).sortable('toArray');
    sorted2 = result.map(Number).map(function(value) { 
      return value - 1; 
      } );
    sorted2 = sorted2.map(function(e) { 
        return e.toString();
      }); 
    result.unshift(numCustomer.toString()); // to prepend id of MyMenu to array  
    let cost = calculateCost(result); 
    let load = calculateLoad(result);
    function checkHome(arr) {
        return arr != numCustomer.toString()
        };
    let customer = result.filter(checkHome).length;
    /*pack all relevant values and conditions in one object*/
    let conditions={
      goal: {
        value: customer, 
        limit: study.MINCUSTOMERS
        },
      constraints: {
        time: {
          value: cost, 
          limit: study.MAXTIME
          },
        load: {
          value: load, 
          limit: study.MAXLOAD
          },
        pull: {
          value: countAction, 
          limit: study.MAXPULLS
          }
      },
      result: result,
      numCustomer: numCustomer
    };           
    let feedbackAlertObject = giveFeedbackAlert(conditions=conditions);
    if (typeof feedbackAlertObject !== 'undefined'){
      study.feedback.innerHTML = feedbackAlertObject.messageContent;
      $(study.feedback).addClass(feedbackAlertObject.messageStyle);
      console.log(feedbackAlertObject.messageStyle);
      };
    let locFBValues =  giveFeedbackValues(conditions=conditions, configuration=study.configuration);
    if (typeof locFBValues !== 'undefined'){
      study.locGoalLabel.innerHTML = locFBValues.goalLabel;
      study.locGoalValue.innerHTML = locFBValues.goalValue;
      study.locTimeLabel.innerHTML = locFBValues.timeLabel;
      study.locTimeValue.innerHTML = locFBValues.timeValue;
      study.locLoadLabel.innerHTML = locFBValues.loadLabel;
      study.locLoadValue.innerHTML = locFBValues.loadValue;
      study.locPullLabel.innerHTML = locFBValues.pullLabel;
      study.locPullValue.innerHTML = locFBValues.pullValue;
      $(study.tabVis).addClass(locFBValues.tabVis);
      } else {};
    study.conditions = conditions;  
    if (typeof sorted2 != "undefined") {
          study.parameters.sorted2 = sorted2};
    study.parameters.route = result.toString();
    study.parameters.numberAttempts = countAction;
    study.options.datastore.set({'intermRoute': result.toString(),'image': image, 'sortTimestamp': event.timeStamp});
    study.options.datastore.commit();
    console.log(study.conditions.constraints.pull.value + study.pullAlert);
    if (study.numPotRoutes % 3 == 0 && study.conditions.constraints.pull.value >= study.pullAlert) {
      document.getElementById("pause").disabled = false;
        };   
    if (study.numPotRoutes % 3 == 0 && study.conditions.constraints.pull.value == study.pullAlert) {
      alert("Bitte übernehmen Sie!");
     }; 
    };
  };