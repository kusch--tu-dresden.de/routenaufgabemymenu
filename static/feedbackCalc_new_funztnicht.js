function giveFeedbackAlert(conditions) {
// no edge available
if (isNaN(conditions.constraints.time.value)){
    return {
        messageContent: 'Keine gültige Verbindung!', 
        messageStyle: 'alert alert-danger'
    };
    } else if (0 < conditions.result.length-1 && conditions.result.length-1 <= conditions.goal.limit && 
    conditions.result[conditions.result.length-1]==conditions.numCustomer){
        return {
            messageContent: 'Ihre Route ist unvollständig. Ihnen fehlen noch Kunden!',
            messageStyle: 'alert alert-warning',
        };
    } else if (
        conditions.constraints.time.value > conditions.constraints.time.limit ||
        (conditions.constraints.time.value == conditions.constraints.time.limit &&
            conditions.result[conditions.result.length-1]!=conditions.numCustomer)){
            return {
                messageContent: 'Die Zeit reicht nicht!',
                messageStyle: 'alert alert-warning',
            };
        } else if (
            conditions.constraints.pull.value > conditions.constraints.pull.limit ||
            (conditions.constraints.pull.value == conditions.constraints.pull.limit &&
                conditions.result[conditions.result.length-1]!=conditions.numCustomer)){
                return {
                    messageContent: 'Ihre Züge sind aufgebraucht. Überspringen Sie diese Aufgabe!',
                    messageStyle: 'alert alert-danger',
                };
            } else if (
            conditions.constraints.load.value > conditions.constraints.load.limit){
                return {
                    messageContent: 'Die Essensportionen reichen nicht!!',
                    messageStyle: 'alert alert-warning',
                };
                } else if (
                    (conditions.constraints.time.value > conditions.constraints.time.limit ||
                    (conditions.constraints.time.value == conditions.constraints.time.limit &&
                        conditions.result[conditions.result.length-1]!=conditions.numCustomer)) &&
                        conditions.constraints.load.value > conditions.constraints.load.limit){
                        return {
                            messageContent: 'Weder Zeit noch Essensportionen reichen!',
                            messageStyle: 'alert alert-warning',
                        };                
                                           
                    } else if (conditions.goal.value == conditions.goal.limit){
                            if (conditions.result[conditions.result.length-1]==conditions.numCustomer){
                                return {
                                    messageContent: 'Sie haben die Aufgabe gelöst!',
                                    messageStyle: 'alert',
                                };
                            } else {
                                return {
                                    messageContent:  'Sie benötigen keine weiteren Kunden mehr und können zu MyMenu zurückkehren.',
                                    messageStyle: 'alert',
                                };                       
                            }
                        } else if (conditions.goal.value > conditions.goal.limit && 
                            conditions.result[conditions.result.length-1]==conditions.numCustomer){
                                return {
                                    messageContent: 
                                    'Toll, Sie haben die Erwartungen übertroffen!',
                                    messageStyle: 'alert',
                                };
                            } else {
                                    return {
                                    messageContent: '',
                                    messageStyle: 'invisible',
                                };
                            }
                        }


function giveFeedbackValues(conditions, configuration) {
    // no edge available
    if (isNaN(conditions.constraints.time.value)){
        return {
            goalLabel: '',
            goalValue: '',
            timeLabel: '',
            timeValue: '',
            loadLabel: '',
            loadValue: '',
            pullLabel: '',
            pullValue: '',
            tabVis: 'invisible',};
        } else if (configuration.declarative === "easy"){
                let neededCustomer = conditions.goal.limit - conditions.goal.value;
                let remainingTime = conditions.constraints.time.limit - conditions.constraints.time.value;
                let remainingLoad = conditions.constraints.load.limit - conditions.constraints.load.value;
                let remainingPulls = conditions.constraints.pull.limit - conditions.constraints.pull.value;
                return {
                    goalLabel: 'Benötigte Kunden:',
                    goalValue: neededCustomer.toString(),
                    timeLabel: 'Verfügbare Zeit:',
                    timeValue: remainingTime.toString(),
                    loadLabel: 'Verfügbare Menge:',
                    loadValue: remainingLoad.toString(),
                    pullLabel: 'Verfügbare Züge:',
                    pullValue: remainingPulls.toString(),
                    tabVis: 'empty', };
                } else {
                    let neededCustomer = conditions.goal.value;
                    let remainingTime = conditions.constraints.time.value;
                    let remainingLoad = conditions.constraints.load.value;
                    let remainingPulls = conditions.constraints.pull.value;
                    return {
                        goalLabel: 'Berücksichtigte Kunden:',
                        goalValue: neededCustomer.toString(),
                        timeLabel: 'Verbrauchte Zeit:',
                        timeValue: remainingTime.toString(),
                        loadLabel: 'Gelieferte Menge:',
                        loadValue: remainingLoad.toString(),
                        pullLabel: 'Getätigte Züge:',
                        pullValue: remainingPulls.toString(),
                        tabVis: 'empty',}
                    }

            };

function isSolutionValid(conditions) {
    if (conditions.goal.value >= conditions.goal.limit &&
        conditions.constraints.time.value <= conditions.constraints.time.limit &&
        conditions.constraints.load.value <= conditions.constraints.load.limit &&
        conditions.constraints.pull.value <= conditions.constraints.pull.limit &&
        conditions.result[conditions.result.length-1]==conditions.numCustomer)
        {return true;
        } else {
            //alert("ungültige Route");
            //returnToPreviousPage();
            return false;
        };
    }

    //calculate Costs 
function costOf(nodeindex1, nodeindex2){
    var key = nodeindex1.toString().concat("-", nodeindex2.toString());
    return study.edges[key];
  }
  
  function calculateCost(arr){
    var sum = 0;
    for (i = 0; i<arr.length-1; i++){
      let j=i+1
      sum += costOf(arr[i], arr[j]);
     /* console.log('i ist:' + i.toString() + 'und arr[i] ist: ' + arr[i] +
      *'und j ist:' + j.toString() + 'und arr[j] ist: ' + arr[j] + 'sum ist: ' + sum)*/
    }
    return sum;
  }
  
  function calculateLoad(arr){
    var sum = 0;
    for (i = 1; i<arr.length; i++){
      var key=arr[i];
      sum += study.nodes[key];
    }
    return sum;
  } 

  
/* add My menu to the second list */
function addMyMenu(numCustomer) { 
    // erstelle ein neues listen Element
    // und gib ihm etwas Inhalt
    var newListItem = document.createElement("li"); 
    var newContent = document.createTextNode("MyMenu"); 
    newListItem.appendChild(newContent); // füge den Textknoten zum neu erstellten div hinzu.
    newListItem.setAttribute("id", numCustomer.toString());
    newListItem.setAttribute("class", "unsortable ui-state-default");
  
    // füge das neu erstellte Element und seinen Inhalt ins DOM ein
    var domLocation = document.getElementById("sortable2"); 
    domLocation.append(newListItem); 
  }