// Define study
const study = lab.util.fromObject({
  "title": "root",
  "type": "lab.flow.Sequence",
  "parameters": {},
  "plugins": [
    {
      "type": "lab.plugins.Metadata",
      "path": undefined
    },
    {
      "type": "lab.plugins.PostMessage",
      "path": undefined
    }
  ],
  "metadata": {
    "title": "",
    "description": "",
    "repository": "",
    "contributors": ""
  },
  "files": {},
  "responses": {},
  "content": [
    {
      "type": "lab.html.Form",
      "content": "\n\u003Cform\u003E\n  \u003Cinput name=\"variable\"\u003E\n  \u003Cbutton type=\"submit\"\u003ESubmit\u003C\u002Fbutton\u003E\n\u003C\u002Fform\u003E",
      "scrollTop": true,
      "files": {},
      "responses": {},
      "parameters": {},
      "messageHandlers": {},
      "title": "Form",
      "skip": true
    },
    {
      "type": "lab.html.Form",
      "content": "\u003Cheader\u003E\n  \u003Ch1\u003EEingabe Probandencode\u003C\u002Fh1\u003E\n\u003C\u002Fheader\u003E\n\n\u003Cmain\u003E\n  \u003Cform\u003E\n    \u003Ctable class=\"tab-configuration\"\u003E\n      \u003Ctr\u003E\n        \u003Ctd\u003E\n          \u003Clabel for=\"participant-id\"\u003EProbandencode: \u003C\u002Flabel\u003E \n        \u003C\u002Ftd\u003E\n        \u003Ctd\u003E\n          \u003Cinput type=\"text\" name=\"participant-id\" id=\"participant-id\"\u003E\n        \u003C\u002Ftd\u003E\n      \u003C\u002Ftr\u003E\n    \u003C!--  \u003Ctr\u003E\n        \u003Ctd\u003E\n          \u003Clabel for=\"ndt-between-condition\"\u003EAufgabenbedingung | Zwischensubjekt: \u003C\u002Flabel\u003E \n        \u003C\u002Ftd\u003E\n        \u003Ctd\u003E\n          \u003Cselect name=\"ndt-between-condition\" id=\"ndt-between-condition\"\u003E\n            \u003Coption value=\"\" disabled selected\u003EBitte auswählen\u003C\u002Foption\u003E\n            \u003Coption value=\"phys-\"\u003Ephys - \u003C\u002Foption\u003E\n            \u003Coption value=\"phys+\"\u003Ephys + \u003C\u002Foption\u003E\n          \u003C\u002Fselect\u003E\n        \u003C\u002Ftd\u003E\n      \u003C\u002Ftr\u003E\n      \u003Ctr\u003E\n        \u003Ctd\u003E\n          \u003Clabel for=\"ndt-within-condition-decl\"\u003EAufgabenbedingung | Innersubjekt | decl: \u003C\u002Flabel\u003E\n        \u003C\u002Ftd\u003E\n        \u003Ctd\u003E\n          \u003Cselect name=\"ndt-within-condition-decl\" id=\"ndt-within-condition-decl\"\u003E\n            \u003Coption value=\"\" disabled selected\u003EBitte auswählen\u003C\u002Foption\u003E\n            \u003Coption value=\"decl-\"\u003Edecl - \u003C\u002Foption\u003E\n            \u003Coption value=\"decl+\"\u003Edecl + \u003C\u002Foption\u003E\n          \u003C\u002Fselect\u003E\n        \u003C\u002Ftd\u003E\n      \u003Ctr\u003E\n        \u003Ctd\u003E\n          \u003Clabel for=\"ndt-within-condition-prob\"\u003EAufgabenbedingung | Innersubjekt | prob: \u003C\u002Flabel\u003E \n        \u003C\u002Ftd\u003E\n        \u003Ctd\u003E\n          \u003Cselect name=\"ndt-within-condition-prob\" id=\"ndt-within-condition-prob\"\u003E\n            \u003Coption value=\"\" disabled selected\u003EBitte auswählen\u003C\u002Foption\u003E\n            \u003Coption value=\"prob-\"\u003Eprob - \u003C\u002Foption\u003E\n            \u003Coption value=\"prob+\"\u003Eprob + \u003C\u002Foption\u003E\n          \u003C\u002Fselect\u003E\n        \u003C\u002Ftd\u003E\n      \u003C\u002Ftr\u003E\n      \u003C!--\u003Ctr\u003E\n        \u003Ctd\u003E\n          \u003Clabel for=\"sim-condition\"\u003EFahrtbedingung: \u003C\u002Flabel\u003E\n      \u003Cselect name=\"sim-condition\" id=\"sim-condition\"\u003E\n        \u003Coption value=\"warmUp\"\u003EwarmUp\u003C\u002Foption\u003E\n        \u003Coption value=\"trial1\"\u003ETrial 1\u003C\u002Foption\u003E\n        \u003Coption value=\"trial2\"\u003ETrial 2\u003C\u002Foption\u003E\n        \u003Coption value=\"trial3\"\u003ETrial 3\u003C\u002Foption\u003E\n      \u003C\u002Fselect\u003E--\u003E\n  \u003C\u002Ftable\u003E\n  \u003C\u002Fform\u003E\n\u003C\u002Fmain\u003E\n\n\u003Cfooter\u003E\n  \u003Cform\u003E\n    \u003Cbutton class=\"right\" type=\"submit\"\u003EWeiter\u003C\u002Fbutton\u003E\n  \u003C\u002Fform\u003E\n\u003C\u002Ffooter\u003E\n\n\n",
      "scrollTop": true,
      "files": {},
      "responses": {},
      "parameters": {},
      "messageHandlers": {
        "before:prepare": function anonymous(
) {
window.addEventListener('beforeunload', function (e) {
  // Cancel the event
  e.preventDefault(); // If you prevent default behavior in Mozilla Firefox prompt will always be shown
  // Chrome requires returnValue to be set
  e.returnValue = '';
});

},
        "after:end": function anonymous(
) {
/*function levelOfFactor(label, easyLevel, hardLevel){
  factor = study.options.datastore.get(label);
  if (factor === easyLevel) {
    result = "easy";
  } else if (factor === hardLevel){
    result = "hard";
  } else {result = "";};
  return result;
}

const physical = levelOfFactor("ndt-between-condition", "phys-", "phys+");
const declarative = levelOfFactor("ndt-within-condition-decl", "decl-", "decl+");
const problemState = levelOfFactor("ndt-within-condition-prob", "prob-", "prob+");


 let configuration={
   physical: physical, 
   declarative: declarative,
   problemState: problemState,
   }; 
  study.configuration = configuration; */        
}
      },
      "title": "ProbandenID"
    },
    {
      "type": "lab.flow.Sequence",
      "files": {},
      "responses": {},
      "parameters": {},
      "messageHandlers": {},
      "title": "Sequence_Instruction",
      "skip": true,
      "content": [
        {
          "type": "lab.html.Screen",
          "files": {},
          "responses": {
            "click button#continue": "continue"
          },
          "parameters": {},
          "messageHandlers": {},
          "title": "Screen_Instruction_01",
          "content": "\u003Cheader\u003E\r\n  \u003Ch1\u003EAnleitung\u003C\u002Fh1\u003E \r\n\u003C\u002Fheader\u003E\r\n\r\n\u003Cmain\u003E\r\n\u003Cp class=\"instruction\"\u003E\r\n  Nachfolgend werden Sie die Routenplanungsaufgabe kennenlernen. \u003C\u002Fp\u003E\r\n\u003Cp class=\"instruction\"\u003EZunächst erfahren Sie, wie die Aufgabe funktioniert. Danach können Sie das Bearbeiten der Aufgaben dreimal üben.\r\n\u003C\u002Fp\u003E\r\n\r\n\r\n\r\n\u003C\u002Fmain\u003E\r\n\u003Cfooter\u003E \r\n\r\n\u003Cdiv class=\"btn-group right\"\u003E\r\n   \u003Cbutton type=\"submit\" id=\"continue\"\u003E\r\n    Weiter\r\n  \u003C\u002Fbutton\u003E\r\n\u003C\u002Fdiv\u003E\r\n\u003C\u002Ffooter\u003E\r\n\r\n "
        },
        {
          "type": "lab.html.Screen",
          "files": {},
          "responses": {
            "click button#continue": "continue"
          },
          "parameters": {},
          "messageHandlers": {},
          "title": "Screen_Instruction_02",
          "content": "\u003Cheader\u003E\r\n  \u003Ch1\u003EZiele und Randbedingungen\u003C\u002Fh1\u003E \r\n\u003C\u002Fheader\u003E\r\n\r\n\u003Cmain\u003E\r\n\u003Cp class=\"instruction\"\u003E\r\nVersetzen Sie sich bitte in die folgende Situation: \u003C\u002Fp\u003E\r\n\r\n\u003Cp class=\"instruction\"\u003ESie arbeiten für MyMenu. Das Unternehmen beliefert Betriebe und Einrichtungen mit Mittagessen. Sie sind unter anderem dafür zuständig, die Routen der Fahrenden zu planen. \u003C\u002Fp\u003E\r\n\r\n\u003Cp class=\"instruction\"\u003EEs ist Freitagnachmittag. Für die kommende Woche sollen Sie noch zügig die Routenpläne festlegen. Obwohl es an Personal und Fahrzeugen mangelt, sollen Sie mit Ihren Routen möglichst viele Kunden zufriedenstellen. \u003C!--Daher ist es Ihr Ziel, möglichst \u003Cstrong\u003Eviele Kunden\u003C\u002Fstrong\u003E in einer Route aufzunehmen.--\u003E\r\n\u003C\u002Fp\u003E\r\n\u003Cp class=\"instruction\"\u003E\r\nBeachten Sie dabei, dass\r\n\u003Cul class=\"instruction\"\u003E\r\n  \u003Cli\u003E die Route stets bei der Firma \u003Cstrong\u003EMyMenu\u003C\u002Fstrong\u003E beginnen und auch wieder bei der Firma MyMenu enden muss,\u003C\u002Fli\u003E\r\n\r\n  \u003Cli\u003E an jedem Kunden nur \u003Cstrong\u003Eeinmal\u003C\u002Fstrong\u003E vorbeigefahren werden darf,\u003C\u002Fli\u003E\r\n\r\n  \u003Cli\u003E die Fahrzeuge nur eine \u003Cstrong\u003Ebegrenzte Menge\u003C\u002Fstrong\u003E an Essensportionen fassen und\u003C\u002Fli\u003E\r\n\r\n  \u003Cli\u003E die Fahrenden unbedingt die \u003Cstrong\u003Evorgegebene Fahrtdauer\u003C\u002Fstrong\u003E einhalten müssen.\u003C\u002Fli\u003E\r\n\r\n  \u003Cli\u003E Aus technischen Gründen können Sie nur begrenzt häufig einen Kunden ein-oder umsortieren. Ihnen stehen stets vier Züge mehr zur Verfügung als zur Lösung der Aufgabe notwendig. \u003C\u002Fli\u003E\r\n\u003C\u002Ful\u003E\r\n\u003C\u002Fp\u003E\r\n\r\n\u003C\u002Fmain\u003E\r\n\u003Cfooter\u003E \r\n\r\n\u003Cdiv class=\"btn-group right\"\u003E\r\n   \u003Cbutton type=\"submit\" id=\"continue\"\u003E\r\n    Weiter\r\n  \u003C\u002Fbutton\u003E\r\n\u003C\u002Fdiv\u003E\r\n\u003C\u002Ffooter\u003E\r\n\r\n "
        },
        {
          "type": "lab.html.Screen",
          "files": {
            "Instruktion_Randbedingungen.png": "embedded\u002F4940afe7c2b51ef11b361222ddd8276f7e2e6e1be976c96ad3bc9e6c9e55811a.png"
          },
          "responses": {
            "click button#continue": "continue"
          },
          "parameters": {},
          "messageHandlers": {},
          "title": "Screen_Instruction_03",
          "content": "\u003Cheader\u003E\r\n  \u003Ch1\u003EZiele und Randbedingungen\u003C\u002Fh1\u003E \r\n\u003C\u002Fheader\u003E\r\n\r\n\u003Cmain\u003E\r\n\u003Cp class=\"instruction\"\u003E\r\nBei jeder Aufgabe werden Ihnen die angestrebte Menge zu beliefernder Kunden und die jeweils gültigen Randbedingungen links angezeigt.\r\n\u003C\u002Fp\u003E\r\n\u003Cimg  class=\"left instruction\" src=${this.files[\"Instruktion_Randbedingungen.png\"]}\u003E\r\n\u003C\u002Fmain\u003E\r\n\u003Cfooter\u003E \r\n\r\n\u003Cdiv class=\"btn-group right\"\u003E\r\n   \u003Cbutton type=\"submit\" id=\"continue\"\u003E\r\n    Weiter\r\n  \u003C\u002Fbutton\u003E\r\n\u003C\u002Fdiv\u003E\r\n\u003C\u002Ffooter\u003E\r\n\r\n "
        },
        {
          "type": "lab.html.Screen",
          "files": {
            "Instruktion_Streckenübersicht.png": "embedded\u002Ff26d469fc00577d5ad663c2ec5bafe555fdeb71a159a0c141ceac95ab8f2d6f7.png"
          },
          "responses": {
            "click button#continue": "continue"
          },
          "parameters": {},
          "messageHandlers": {},
          "title": "Screen_Instruction_04",
          "content": "\u003Cheader\u003E\r\n  \u003Ch1\u003EStreckenübersicht\u003C\u002Fh1\u003E \r\n\u003C\u002Fheader\u003E\r\n\r\n\u003Cmain\u003E\r\n\u003Cp class=\"instruction\"\u003E\r\nSie erhalten jedes Mal eine Übersicht über die Kunden, die beliefert \r\nwerden möchten, und über die verfügbaren Fahrtstrecken. Diese Ansicht wird Ihnen nachfolgend näher erläutert.\r\n\u003C\u002Fp\u003E\r\n\r\n\u003Cimg class=\"left\" src=${this.files[\"Instruktion_Streckenübersicht.png\"]}\u003E\r\n\r\n\u003C\u002Fmain\u003E\r\n\u003Cfooter\u003E \r\n\r\n\u003Cdiv class=\"btn-group right\"\u003E\r\n   \u003Cbutton type=\"submit\" id=\"continue\"\u003E\r\n    Weiter\r\n  \u003C\u002Fbutton\u003E\r\n\u003C\u002Fdiv\u003E\r\n\u003C\u002Ffooter\u003E\r\n\r\n "
        },
        {
          "type": "lab.html.Screen",
          "files": {
            "Instruktion_Start.png": "embedded\u002F6516eb3ce7d259fcfa29c375a36e98445beda85dc9865a4364fbbbe2e92149ed.png"
          },
          "responses": {
            "click button#continue": "continue"
          },
          "parameters": {},
          "messageHandlers": {},
          "title": "Screen_Instruction_05",
          "content": "\u003Cheader\u003E\r\n  \u003Ch1\u003EStreckenübersicht\u003C\u002Fh1\u003E \r\n\u003C\u002Fheader\u003E\r\n\r\n\u003Cmain\u003E\r\n\u003Cp class=\"instruction\"\u003E\r\nStart- und Endpunkt der Route ist stets MyMenu. \r\nSie können es an dem Kreis mit doppelter Linie erkennen.\r\n\u003C\u002Fp\u003E\r\n\u003Cimg class=\"left\" src=${this.files[\"Instruktion_Start.png\"]}\u003E\r\n\r\n\r\n\u003C\u002Fmain\u003E\r\n\u003Cfooter\u003E \r\n\r\n\u003Cdiv class=\"btn-group right\"\u003E\r\n   \u003Cbutton type=\"submit\" id=\"continue\"\u003E\r\n    Weiter\r\n  \u003C\u002Fbutton\u003E\r\n\u003C\u002Fdiv\u003E\r\n\u003C\u002Ffooter\u003E\r\n\r\n "
        },
        {
          "type": "lab.html.Screen",
          "files": {
            "Instruktion_Strecken.png": "embedded\u002Fdda716dacb9d154940ed295b438bcde7aa932d4bbcd652d60fbcb1e3d122986a.png"
          },
          "responses": {
            "click button#continue": "continue"
          },
          "parameters": {},
          "messageHandlers": {},
          "title": "Screen_Instruction_06",
          "content": "\u003Cheader\u003E\r\n  \u003Ch1\u003EStreckenübersicht\u003C\u002Fh1\u003E \r\n\u003C\u002Fheader\u003E\r\n\r\n\u003Cmain\u003E\r\n\u003Cp class=\"instruction\"\u003E\r\nDie Strecken, die das Fahrzeug nutzen kann, sind als Linien zwischen \r\nden Kunden abgebildet. An der Mitte der Strecke steht eine Zahl. Diese gibt an, \r\nwie viele Minuten die Fahrt auf dieser Strecke dauert. Die Strecken und \r\nzugehörigen Dauern sind farbig. Das soll Ihnen helfen, die Strecken \r\nvoneinander zu unterscheiden. Die Farbe hängt aber nicht von der \r\nbenötigten Fahrtdauer ab. \r\n\u003C\u002Fp\u003E\r\n\r\n\u003Cimg class=\"left\" src=${this.files[\"Instruktion_Strecken.png\"]}\u003E\r\n\r\n\u003C\u002Fmain\u003E\r\n\u003Cfooter\u003E \r\n\r\n\u003Cdiv class=\"btn-group right\"\u003E\r\n   \u003Cbutton type=\"submit\" id=\"continue\"\u003E\r\n    Weiter\r\n  \u003C\u002Fbutton\u003E\r\n\u003C\u002Fdiv\u003E\r\n\u003C\u002Ffooter\u003E\r\n\r\n "
        },
        {
          "type": "lab.html.Screen",
          "files": {
            "Instruktion_Routenerstellung.png": "embedded\u002Fae038787442852896eef51b38ffcbb225406e49580c322387ba2a84bca47520e.png"
          },
          "responses": {
            "click button#continue": "continue"
          },
          "parameters": {},
          "messageHandlers": {},
          "title": "Screen_Instruction_07",
          "content": "\u003Cheader\u003E\r\n  \u003Ch1\u003ERoutenerstellung\u003C\u002Fh1\u003E \r\n\u003C\u002Fheader\u003E\r\n\r\n\u003Cmain\u003E\r\n  \u003Cp class=\"instruction\"\u003E\r\nNun haben Sie einen Eindruck, wie die Ihnen zur Verfügung gestellten \r\nInformationen aussehen. Nachfolgend wird Ihnen erklärt, wie Sie die \r\nRoute zusammenstellen können.\u003C\u002Fp\u003E\r\n\u003Cp class=\"instruction\"\u003E\r\nAuf der linken Seite sehen Sie eine Liste mit den Kunden und der dazugehörigen Bestellmenge. Auf der rechten Seite befindet sich eine noch fast leere Liste. Hier sollen Sie die Kunden mit Ihrem Finger reinziehen. So ergibt sich von oben nach unten die Reihenfolge der anzufahrenden Kunden.\r\n\u003C\u002Fp\u003E\r\n\u003Cimg class=\"left\" src=${this.files[\"Instruktion_Routenerstellung.png\"]}\u003E\r\n\u003C\u002Fmain\u003E\r\n\u003Cfooter\u003E \r\n\r\n\u003C\u002Fmain\u003E\r\n\u003Cfooter\u003E \r\n\r\n\u003Cdiv class=\"btn-group right\"\u003E\r\n   \u003Cbutton type=\"submit\" id=\"continue\"\u003E\r\n    Weiter\r\n  \u003C\u002Fbutton\u003E\r\n\u003C\u002Fdiv\u003E\r\n\u003C\u002Ffooter\u003E\r\n\r\n "
        },
        {
          "type": "lab.html.Screen",
          "files": {
            "Instruktion_Abschicken.png": "embedded\u002F22adca6475eae7cf2e4d86d6036c85a8c7b6170a414296e1bcd9545e11d4ebbc.png"
          },
          "responses": {
            "click button#continue": "continue"
          },
          "parameters": {},
          "messageHandlers": {},
          "title": "Screen_Instruction_08",
          "content": "\u003Cheader\u003E\r\n  \u003Ch1\u003EAufgabe beenden\u003C\u002Fh1\u003E \r\n\u003C\u002Fheader\u003E\r\n\r\n\u003Cmain\u003E\r\n\u003Cp class=\"instruction\"\u003E\r\nAuf der rechten Seite erhalten Sie Rückmeldung zum aktuellen Bearbeitungsstand. Sie erfahren, wie viele Kunden Sie noch benötigen und wie viel Zeit und Essensportionen Ihnen noch zur Verfügung stehen. \u003C\u002Fp\u003E\r\n\u003Cp class=\"instruction\"\u003E\r\n  Sobald Sie eine gültige Route erstellt haben, können Sie die Aufgabe beenden, indem Sie auf \u003Ckbd\u003EWeiter\u003C\u002Fkbd\u003E tippen. Danach wird Ihnen die nächste Aufgabe angezeigt.\r\n\u003C\u002Fp\u003E\r\n\u003Cimg class=\"left\" src=${this.files[\"Instruktion_Abschicken.png\"]}\u003E\r\n\u003C\u002Fmain\u003E\r\n\u003Cfooter\u003E \r\n\r\n\u003C\u002Fmain\u003E\r\n\u003Cfooter\u003E \r\n\r\n\u003Cdiv class=\"btn-group right\"\u003E\r\n   \u003Cbutton type=\"submit\" id=\"continue\"\u003E\r\n    Weiter\r\n  \u003C\u002Fbutton\u003E\r\n\u003C\u002Fdiv\u003E\r\n\u003C\u002Ffooter\u003E\r\n\r\n "
        }
      ]
    },
    {
      "type": "lab.flow.Sequence",
      "files": {},
      "responses": {},
      "parameters": {},
      "messageHandlers": {
        "before:prepare": function anonymous(
) {
  let configuration={
   physical: "easy", 
   declarative: "easy",
   problemState: "easy",
   }; 
  study.configuration = configuration;  
}
      },
      "title": "Sequence_Exercise",
      "content": [
        {
          "type": "lab.html.Screen",
          "files": {},
          "responses": {
            "click button#continue": "continue"
          },
          "parameters": {},
          "messageHandlers": {},
          "title": "Screen_Zwischeninformation_Experiment",
          "content": "\u003Cheader\u003E\r\n\r\n  \u003Ch1\u003EEigentliche Routenplanungsaufgabe\u003C\u002Fh1\u003E\r\n\u003C\u002Fheader\u003E\r\n\r\n\u003Cmain\u003E\r\n\u003Cp class=\"instruction\"\u003ENun beginnt die eigentliche Routenplanungsaufgabe. \r\nHier sind noch einmal die wichtigsten Regeln, die Sie einhalten müssen: \u003C\u002Fp\u003E\r\n\u003Cp class=\"instruction\"\u003E\r\nAchten Sie darauf, dass\r\n\u003Cul class=\"instruction\"\u003E\r\n  \u003Cli\u003E möglichst \u003Cstrong\u003Eviele Kunden\u003C\u002Fstrong\u003E beliefert werden, \u003C\u002Fli\u003E\r\n  \u003Cli\u003E die Route stets bei der Firma \u003Cstrong\u003EMyMenu\u003C\u002Fstrong\u003E beginnt und dort auch wieder endet,\u003C\u002Fli\u003E\r\n  \u003Cli\u003E jeder Kunde nur \u003Cstrong\u003Eeinmal\u003C\u002Fstrong\u003E besucht wird,\u003C\u002Fli\u003E\r\n  \u003Cli\u003E nur eine \u003Cstrong\u003Ebegrenzte Fahrtzeit\u003C\u002Fstrong\u003E zur Verfügung steht und \u003C\u002Fli\u003E \r\n  \u003Cli\u003E die Fahrzeuge nur eine \u003Cstrong\u003Ebegrenzte Essensmenge\u003C\u002Fstrong\u003E fassen.   \u003C\u002Fli\u003E \r\n\u003C\u002Ful\u003E\r\n\u003C\u002Fp\u003E\r\n\u003Cp class=\"instruction\"\u003E\r\nVersuchen Sie nun so schnell wie möglich Routen zu planen, die die oben genannten Bedingungen erfüllen.\r\n\u003C\u002Fp\u003E\r\n\r\n\r\n\r\n\u003C\u002Fmain\u003E\r\n\u003Cfooter\u003E \r\n\r\n\u003Cdiv class=\"btn-group right\"\u003E\r\n   \u003Cbutton type=\"submit\" id=\"continue\"\u003E\r\n    Weiter\r\n  \u003C\u002Fbutton\u003E\r\n\u003C\u002Fdiv\u003E\r\n\u003C\u002Ffooter\u003E\r\n\r\n ",
          "skip": true
        },
        {
          "type": "lab.flow.Loop",
          "templateParameters": [
            {
              "customer_01": "Dibaf [50]",
              "customer_02": "EGU [10]",
              "customer_03": "Gege [50]",
              "customer_04": "Necego [40]",
              "customer_05": "Ninim [10]",
              "customer_06": "Sare [20]",
              "customer_07": "MyMenu",
              "customer_08": "",
              "customer_09": "",
              "customer_10": "",
              "image": "rand_graph_7_0000.gv.png",
              "json": "{\"edges\": [{\"name\": \"1-3\", \"value\": 5}, {\"name\": \"1-7\", \"value\": 10}, {\"name\": \"1-5\", \"value\": 25}, {\"name\": \"2-3\", \"value\": 15}, {\"name\": \"2-5\", \"value\": 15}, {\"name\": \"3-1\", \"value\": 5}, {\"name\": \"3-2\", \"value\": 15}, {\"name\": \"3-7\", \"value\": 15}, {\"name\": \"7-1\", \"value\": 10}, {\"name\": \"7-3\", \"value\": 15}, {\"name\": \"7-5\", \"value\": 25}, {\"name\": \"7-6\", \"value\": 10}, {\"name\": \"4-5\", \"value\": 20}, {\"name\": \"4-6\", \"value\": 25}, {\"name\": \"5-1\", \"value\": 25}, {\"name\": \"5-2\", \"value\": 15}, {\"name\": \"5-7\", \"value\": 25}, {\"name\": \"5-4\", \"value\": 20}, {\"name\": \"6-7\", \"value\": 10}, {\"name\": \"6-4\", \"value\": 25}]}",
              "node_json": "{\"nodes\": [{\"name\": \"1\", \"value\": 50}, {\"name\": \"2\", \"value\": 10}, {\"name\": \"3\", \"value\": 50}, {\"name\": \"4\", \"value\": 40}, {\"name\": \"5\", \"value\": 10}, {\"name\": \"6\", \"value\": 20}, {\"name\": \"7\", \"value\": 0}]}",
              "n_customer": "2",
              "time_max": "30",
              "load_max": "100"
            },
            {
              "customer_01": "Budem [10]",
              "customer_02": "ETE [20]",
              "customer_03": "Hisitu [10]",
              "customer_04": "Nited [10]",
              "customer_05": "Surag [10]",
              "customer_06": "Tedi [30]",
              "customer_07": "MyMenu",
              "customer_08": "",
              "customer_09": "",
              "customer_10": "",
              "image": "rand_graph_7_0001.gv.png",
              "json": "{\"edges\": [{\"name\": \"1-2\", \"value\": 15}, {\"name\": \"1-7\", \"value\": 5}, {\"name\": \"2-1\", \"value\": 15}, {\"name\": \"2-3\", \"value\": 20}, {\"name\": \"2-4\", \"value\": 25}, {\"name\": \"2-6\", \"value\": 15}, {\"name\": \"3-2\", \"value\": 20}, {\"name\": \"3-5\", \"value\": 5}, {\"name\": \"7-1\", \"value\": 5}, {\"name\": \"7-4\", \"value\": 10}, {\"name\": \"7-5\", \"value\": 20}, {\"name\": \"7-6\", \"value\": 25}, {\"name\": \"4-2\", \"value\": 25}, {\"name\": \"4-7\", \"value\": 10}, {\"name\": \"5-3\", \"value\": 5}, {\"name\": \"5-7\", \"value\": 20}, {\"name\": \"5-6\", \"value\": 10}, {\"name\": \"6-2\", \"value\": 15}, {\"name\": \"6-7\", \"value\": 25}, {\"name\": \"6-5\", \"value\": 10}]}",
              "node_json": "{\"nodes\": [{\"name\": \"1\", \"value\": 10}, {\"name\": \"2\", \"value\": 20}, {\"name\": \"3\", \"value\": 10}, {\"name\": \"4\", \"value\": 10}, {\"name\": \"5\", \"value\": 10}, {\"name\": \"6\", \"value\": 30}, {\"name\": \"7\", \"value\": 0}]}",
              "n_customer": "2",
              "time_max": "55",
              "load_max": "40"
            }
          ],
          "sample": {
            "mode": "sequential",
            "n": ""
          },
          "files": {},
          "responses": {},
          "parameters": {},
          "messageHandlers": {
            "before:prepare": function anonymous(
) {

study.numRoutes = 0;
study.numPotRoutes = 0;
}
          },
          "title": "Exercise_1",
          "shuffleGroups": [],
          "template": {
            "type": "lab.html.Screen",
            "files": {
              "rand_graph_7_0000.gv.png": "embedded\u002F8fe593bc06eba233e01f250b251204a31a8a30a60a7cddab9f51a627ff803e4b.png",
              "rand_graph_7_0001.gv.png": "embedded\u002Fa4925f5e114f68289b17d6ca1c5acb7b7d3cd9b862eefc3f4eef7dfec6e3df52.png"
            },
            "responses": {
              "click button#end": "end",
              "click button#pause": "pause"
            },
            "parameters": {},
            "messageHandlers": {
              "run": function anonymous(
) {
//change layout according th selected factor level
if (study.configuration.declarative === "hard") {
		document.querySelector(".constraints").setAttribute('style','visibility:hidden');
	} else {
		document.querySelector(".constraints").removeAttribute('style');
	}

/*btnPause = document.querySelector("#pause")
if (study.configuration.problemState === "easy") {
		btnPause.classList.add("invisible");
};*/

elementList = document.querySelectorAll("ol, img");
if (study.configuration.physical === "hard"){
  elementList.forEach(element => element.classList.add("phys--hard", "blur"));
  } else {
    elementList.forEach(element => element.classList.add("phys--easy"))
    };


study.conditions = undefined;

if (study.configuration.declarative == "easy"){
  study.numPotRoutes = ++ study.numPotRoutes;
}

// make dom elements available for manipulations
let trialCounter = document.getElementById("trial-count");
let fbNumRoutes = document.getElementById("numRoutes");
let fbPotRoutes = document.getElementById("potRoutes");
let feedback = document.getElementById("feedback");
study.feedback = feedback;

let locGoalLabel = document.getElementById("loc-fb-goal-label");
study.locGoalLabel = locGoalLabel;
let locGoalValue = document.getElementById("loc-fb-goal-value");
study.locGoalValue = locGoalValue;
let locTimeLabel = document.getElementById("loc-fb-time-label");
study.locTimeLabel = locTimeLabel;
let locTimeValue = document.getElementById("loc-fb-time-value");
study.locTimeValue = locTimeValue;
let locPullLabel = document.getElementById("loc-fb-pull-label");
study.locPullLabel = locPullLabel;
let locPullValue = document.getElementById("loc-fb-pull-value");
study.locPullValue = locPullValue;
let locLoadLabel = document.getElementById("loc-fb-load-label");
study.locLoadLabel = locLoadLabel;
let locLoadValue = document.getElementById("loc-fb-load-value");
study.locLoadValue = locLoadValue;
let tabVis = document.getElementById("loc-fb");
study.tabVis = tabVis;


trialCounter.innerHTML = study.numPotRoutes;
fbNumRoutes.innerHTML = study.numRoutes;
fbPotRoutes.innerHTML = study.numPotRoutes -1;
 

//read graph edges and nodes and make them available
let json = this.parameters.json;
let data = JSON.parse(json);
let edges = {}
data.edges.forEach(element => {
  edges[element.name]=element.value
});
study.edges = edges; 

let nodeJson = this.parameters.node_json;
let nodeData = JSON.parse(nodeJson);
let nodes = {}
nodeData.nodes.forEach(element => {
  nodes[element.name]=element.value
});
study.nodes = nodes; 

/*get number of customers*/
let numCustomer = Object.keys(study.nodes).length; //.toString();
study.numCustomer = numCustomer;
addMyMenu(numCustomer=study.numCustomer);
/*define variables for feedback*/
const MAXTIME = parseInt(this.parameters.time_max);
study.MAXTIME = MAXTIME;
const MAXLOAD = parseInt(this.parameters.load_max);
study.MAXLOAD = MAXLOAD;
const MINCUSTOMERS = parseInt(this.parameters.n_customer);
study.MINCUSTOMERS = MINCUSTOMERS;
const MAXPULLS = parseInt(this.parameters.n_customer) + 4;
study.parameters.maxpulls = MAXPULLS;
study.MAXPULLS = MAXPULLS;
study.fbMinCustomers = parseInt(study.MINCUSTOMERS);
const IMAGE = this.parameters.image;

// make lists sortable and save sort result and costs

$(function() {
    $('#sortable2').sortable({
        connectWith: '#sortable1',
        items: "li:not(.unsortable)",
        placeholder: "ui-state-highlight",        
        over: function(event, ui) {
          if (study.configuration.problemState === "hard") {
            ui.helper.children($("span.list-item")).addClass(`invisible`)};
            },
        update: provideFeedback(numCustomer=numCustomer, image=IMAGE),
    }).disableSelection();
    $('#sortable1').sortable({
      opacity: 0.6,
      connectWith: '#sortable2',
      placeholder: "ui-state-highlight",
      over: function(event, ui) {
              if (study.configuration.problemState === "hard"){ 
                ui.helper.children($("span.list-item")).removeClass(`invisible`)};
      },  
    }).disableSelection();
});

// to exit loop
if (this.state.response === 'end'){
  this.parent.parent.parent.parent.end()
};

/*if (typeof study.conditions !== 'undefined' && study.conditions.constraints.pull.value == parseInt(Math.round(study.numCustomer)/2))
  {alert("Bitte übernehmen Sie!")}*/

/*if (typeof study.conditions !== 'undefined'){
  if(study.numPotRoutes % 2 == 0 && study.conditions.constraints.pull.value == Math.round(numCustomer)/2)
  {alert("Bitte übernehmen Sie!")};
}*/
},
              "before:prepare": function anonymous(
) {
  this.options.events['click button#continue'] = e => {
    this.options.datastore.state.response = 'continue'; 
    if (typeof study.conditions !== "undefined" && isSolutionValid(conditions=study.conditions)) {
     // study.numPotCustomers = study.numPotCustomers + study.fbMinCustomers;
      study.numRoutes = ++study.numRoutes;
    //study.numSatCustomers = study.numSatCustomers + study.conditions.result.length-2;
      this.end();
      } else {
        alert("ungültige Route");
        e.preventDefault();
};
};

this.options.events['click button#skip'] = e => {
    this.options.datastore.state.response = 'skip';
    //study.numPotCustomers = study.numPotCustomers + study.fbMinCustomers;
    this.end();};

}
            },
            "title": "Screen_Exercise1",
            "content": "\u003Cmain class=\"experiment\"\u003E\r\n  \u003Cdiv class=wrapper\u003E\r\n\r\n\r\n    \u003Cdiv class=\"trial\"\u003E\r\n      \u003Ch4 class=\"trial\"\u003E \u003Cspan id=\"trial-count\"\u003E\u003C\u002Fspan\u003E. Route \u003C\u002Fh4\u003E\r\n    \u003C\u002Fdiv\u003E\r\n\r\n    \u003Cdiv class=\"constraints\"\u003E\r\n      \u003Ch4\u003EVorgaben\u003C\u002Fh4\u003E\r\n       \u003Ctable class=\"tab-constraints\"\u003E\r\n          \u003Ctr\u003E\r\n            \u003Ctd\u003EZiel:\u003C\u002Ftd\u003E\r\n            \u003Ctd\u003E${this.parameters.n_customer} \u003C\u002Ftd\u003E\r\n            \u003Ctd\u003E Kunden\u003C\u002Ftd\u003E\r\n          \u003C\u002Ftr\u003E\r\n          \u003Ctr\u003E\r\n            \u003Ctd\u003EVerfügbare Züge:\u003C\u002Ftd\u003E\r\n            \u003Ctd\u003E${parseInt(this.parameters.n_customer) + 5}\u003C\u002Ftd\u003E\r\n            \u003Ctd\u003E \u003C\u002Ftd\u003E\r\n          \u003C\u002Ftr\u003E\r\n          \u003Ctr\u003E\r\n            \u003Ctd\u003EVerfügbare Zeit:\u003C\u002Ftd\u003E\r\n            \u003Ctd\u003E${this.parameters.time_max}\u003C\u002Ftd\u003E\r\n            \u003Ctd\u003EMinuten\u003C\u002Ftd\u003E\r\n          \u003C\u002Ftr\u003E\r\n          \u003Ctr\u003E\r\n            \u003Ctd\u003EVerfügbare Menge:\u003C\u002Ftd\u003E\r\n            \u003Ctd\u003E${this.parameters.load_max}\u003C\u002Ftd\u003E\r\n            \u003Ctd\u003EPortionen\u003C\u002Ftd\u003E\r\n          \u003C\u002Ftr\u003E\r\n        \u003C\u002Ftable\u003E\r\n     \u003C\u002Fdiv\u003E\r\n\r\n    \u003Cdiv class=\"graphdisplay\"\u003E\r\n        \u003Cimg class=\"graph\" src=${this.files[this.parameters.image]}\u003E\r\n    \u003C\u002Fdiv\u003E\r\n  \r\n    \u003Cdiv class=\"skip\"\u003E\r\n      \u003Cbutton id=\"skip\" class=\"left\"\u003E\r\n        Überspringen \u003C\u002Fbutton\u003E\r\n    \u003C\u002Fdiv\u003E\r\n\r\n    \u003Cdiv class=\"list1 left available\"\u003E\r\n        \u003Ch4\u003EVerfügbare Orte\u003C\u002Fh4\u003E\r\n        \u003Col id=\"sortable1\" class=\"connectedSortable\"\u003E \u003C!-- phys--hard\"\u003E--\u003E\r\n          \u003Cli id=\"1\" class=\"ui-state-default easy\"\u003E\u003Cspan class=\"list-item\"\u003E${this.parameters.customer_01}\u003C\u002Fspan\u003E\u003C\u002Fli\u003E\r\n          \u003Cli id=\"2\" class=\"ui-state-default\"\u003E\u003Cspan class=\"list-item\"\u003E${this.parameters.customer_02}\u003C\u002Fspan\u003E\u003C\u002Fli\u003E\r\n          \u003Cli id=\"3\" class=\"ui-state-default\"\u003E\u003Cspan class=\"list-item\"\u003E${this.parameters.customer_03}\u003C\u002Fspan\u003E\u003C\u002Fli\u003E\r\n          \u003Cli id=\"4\" class=\"ui-state-default\"\u003E\u003Cspan class=\"list-item\"\u003E${this.parameters.customer_04}\u003C\u002Fspan\u003E\u003C\u002Fli\u003E\r\n          \u003Cli id=\"5\" class=\"ui-state-default\"\u003E\u003Cspan class=\"list-item\"\u003E${this.parameters.customer_05}\u003C\u002Fspan\u003E\u003C\u002Fli\u003E\r\n          \u003Cli id=\"6\" class=\"ui-state-default\"\u003E\u003Cspan class=\"list-item\"\u003E${this.parameters.customer_06}\u003C\u002Fspan\u003E\u003C\u002Fli\u003E\r\n          \u003Cli id=\"7\" class=\"ui-state-default\"\u003EMyMenu\u003C\u002Fli\u003E\r\n        \u003C\u002Fol\u003E\r\n    \u003C\u002Fdiv\u003E\r\n\r\n    \u003Cdiv class=\"list2 arranged\"\u003E\r\n        \u003Ch4\u003EGeplante Route\u003C\u002Fh4\u003E\r\n        \u003Col id=\"sortable2\" class=\"connectedSortable\" start=\"0\"\u003E\r\n        \u003C\u002Fol\u003E\r\n    \u003C\u002Fdiv\u003E\r\n\r\n    \u003Cdiv class=\"exit\"\u003E\r\n      \u003Cbutton id=\"end\" class=\"end left\" type=\"submit\"\u003E\r\n        Abbrechen\r\n      \u003C\u002Fbutton\u003E\r\n    \u003C\u002Fdiv\u003E\r\n\r\n  \u003Cdiv class=\"feedback\"\u003E\r\n      \u003Ch4\u003EStand dieser Route\u003C\u002Fh4\u003E\r\n\r\n       \u003Cp id=\"feedback\"\u003E\u003C\u002Fp\u003E\r\n       \r\n       \u003Ctable id=\"loc-fb\" class=\"tab-local-feedback invisible\"\u003E\r\n          \u003Ctr\u003E\r\n            \u003Ctd id=\"loc-fb-goal-label\"\u003EBenötigte Kunden:\u003C\u002Ftd\u003E\r\n            \u003Ctd id=\"loc-fb-goal-value\"\u003E${this.parameters.n_customer} \u003C\u002Ftd\u003E\r\n            \u003Ctd\u003E\u003C\u002Ftd\u003E\r\n          \u003C\u002Ftr\u003E\r\n          \u003Ctr\u003E\r\n            \u003Ctd id=\"loc-fb-pull-label\"\u003EVerfügbare Züge:\u003C\u002Ftd\u003E\r\n            \u003Ctd id=\"loc-fb-pull-value\"\u003E ${parseInt(this.parameters.n_customer) + 4}\u003C\u002Ftd\u003E\r\n            \u003Ctd\u003E\u003C\u002Ftd\u003E\r\n          \u003C\u002Ftr\u003E\r\n          \u003Ctr\u003E\r\n            \u003Ctd id=\"loc-fb-time-label\"\u003EVerfügbare Zeit:\u003C\u002Ftd\u003E\r\n            \u003Ctd id=\"loc-fb-time-value\"\u003E${this.parameters.time_max} \u003C\u002Ftd\u003E\r\n            \u003Ctd\u003EMin.\u003C\u002Ftd\u003E\r\n          \u003C\u002Ftr\u003E\r\n          \u003Ctr\u003E\r\n            \u003Ctd id=\"loc-fb-load-label\"\u003EVerfügbare Menge:\u003C\u002Ftd\u003E\r\n            \u003Ctd id=\"loc-fb-load-value\"\u003E${this.parameters.load_max} \u003C\u002Ftd\u003E\r\n            \u003Ctd\u003EPort.\u003C\u002Ftd\u003E\r\n          \u003C\u002Ftr\u003E\r\n        \u003C\u002Ftable\u003E\r\n     \u003C\u002Fdiv\u003E\r\n\r\n     \u003Cdiv class=\"global-feedback\"\u003E\r\n      \u003Ch4\u003E Ergebnis aller Routen\u003C\u002Fh4\u003E\r\n      \u003Cdiv class=\"alert\"\u003E\r\n        \u003Ctable class=\"tab-global-feedback\"\u003E\r\n          \u003Ctr\u003E\r\n            \u003Ctd\u003EFertige Routen:\u003C\u002Ftd\u003E\r\n            \u003Ctd id=\"numRoutes\"\u003E\u003C\u002Ftd\u003E\r\n          \u003C\u002Ftr\u003E\r\n          \u003Ctr\u003E\r\n            \u003Ctd\u003E Mögliche Routen \u003C\u002Ftd\u003E\r\n            \u003Ctd id=\"potRoutes\"\u003E\u003C\u002Ftd\u003E\r\n          \u003C\u002Ftr\u003E\r\n\r\n        \u003C\u002Ftable\u003E\r\n      \u003C\u002Fdiv\u003E\r\n     \u003C\u002Fdiv\u003E\r\n     \r\n        \u003Cdiv class=\"pause\"\u003E\r\n      \u003Cbutton disabled id=\"pause\" class=\"left\"\u003E\r\n        Pause \r\n      \u003C\u002Fbutton\u003E\r\n    \u003C\u002Fdiv\u003E\r\n    \u003Cdiv class=\"forward\"\u003E\r\n      \u003Cbutton id=\"continue\" class=\"right\"\u003E \r\n        Weiter\r\n      \u003C\u002Fbutton\u003E   \r\n    \u003C\u002Fdiv\u003E\r\n  \u003C\u002Fdiv\u003E\r\n\u003C\u002Fmain\u003E\r\n",
            "timeline": []
          }
        },
        {
          "type": "lab.flow.Loop",
          "templateParameters": [
            {
              "customer_01": "Duner [10]",
              "customer_02": "Enitum [30]",
              "customer_03": "Enogec [20]",
              "customer_04": "Idome [50]",
              "customer_05": "Imer [40]",
              "customer_06": "Surag [40]",
              "customer_07": "Woned [30]",
              "customer_08": "MyMenu",
              "customer_09": "",
              "customer_10": "",
              "image": "rand_graph_8_0000.gv.png",
              "json": "{\"edges\": [{\"name\": \"1-4\", \"value\": 25}, {\"name\": \"1-5\", \"value\": 25}, {\"name\": \"1-8\", \"value\": 5}, {\"name\": \"2-3\", \"value\": 10}, {\"name\": \"2-5\", \"value\": 25}, {\"name\": \"2-7\", \"value\": 20}, {\"name\": \"3-2\", \"value\": 10}, {\"name\": \"3-4\", \"value\": 20}, {\"name\": \"3-6\", \"value\": 15}, {\"name\": \"4-1\", \"value\": 25}, {\"name\": \"4-3\", \"value\": 20}, {\"name\": \"4-5\", \"value\": 20}, {\"name\": \"4-8\", \"value\": 15}, {\"name\": \"4-6\", \"value\": 20}, {\"name\": \"5-1\", \"value\": 25}, {\"name\": \"5-2\", \"value\": 25}, {\"name\": \"5-4\", \"value\": 20}, {\"name\": \"8-1\", \"value\": 5}, {\"name\": \"8-4\", \"value\": 15}, {\"name\": \"8-6\", \"value\": 5}, {\"name\": \"8-7\", \"value\": 20}, {\"name\": \"6-3\", \"value\": 15}, {\"name\": \"6-4\", \"value\": 20}, {\"name\": \"6-8\", \"value\": 5}, {\"name\": \"6-7\", \"value\": 10}, {\"name\": \"7-2\", \"value\": 20}, {\"name\": \"7-8\", \"value\": 20}, {\"name\": \"7-6\", \"value\": 10}]}",
              "node_json": "{\"nodes\": [{\"name\": \"1\", \"value\": 10}, {\"name\": \"2\", \"value\": 30}, {\"name\": \"3\", \"value\": 20}, {\"name\": \"4\", \"value\": 50}, {\"name\": \"5\", \"value\": 40}, {\"name\": \"6\", \"value\": 40}, {\"name\": \"7\", \"value\": 30}, {\"name\": \"8\", \"value\": 0}]}",
              "n_customer": "5",
              "time_max": "100",
              "load_max": "155"
            }
          ],
          "sample": {
            "mode": "sequential",
            "n": ""
          },
          "files": {},
          "responses": {},
          "parameters": {},
          "messageHandlers": {
            "before:prepare": function anonymous(
) {

study.numRoutes = 0;
study.numPotRoutes = 0;
}
          },
          "title": "Exercise_2",
          "shuffleGroups": [],
          "template": {
            "type": "lab.html.Screen",
            "files": {
              "rand_graph_8_0000.gv.png": "embedded\u002F6f079b58104efdd85de276d42696c0bddc8211b0e2cff388643dd242f1818e9a.png"
            },
            "responses": {
              "click button#end": "end",
              "click button#pause": "pause"
            },
            "parameters": {},
            "messageHandlers": {
              "run": function anonymous(
) {
//change layout according th selected factor level
if (study.configuration.declarative === "hard") {
		document.querySelector(".constraints").setAttribute('style','visibility:hidden');
	} else {
		document.querySelector(".constraints").removeAttribute('style');
	}

/*btnPause = document.querySelector("#pause")
if (study.configuration.problemState === "easy") {
		btnPause.classList.add("invisible");
};*/

elementList = document.querySelectorAll("ol, img");
if (study.configuration.physical === "hard"){
  elementList.forEach(element => element.classList.add("phys--hard", "blur"));
  } else {
    elementList.forEach(element => element.classList.add("phys--easy"))
    };


study.conditions = undefined;

if (study.configuration.declarative == "easy"){
  study.numPotRoutes = ++ study.numPotRoutes;
}

// make dom elements available for manipulations
let trialCounter = document.getElementById("trial-count");
let fbNumRoutes = document.getElementById("numRoutes");
let fbPotRoutes = document.getElementById("potRoutes");
let feedback = document.getElementById("feedback");
study.feedback = feedback;

let locGoalLabel = document.getElementById("loc-fb-goal-label");
study.locGoalLabel = locGoalLabel;
let locGoalValue = document.getElementById("loc-fb-goal-value");
study.locGoalValue = locGoalValue;
let locTimeLabel = document.getElementById("loc-fb-time-label");
study.locTimeLabel = locTimeLabel;
let locTimeValue = document.getElementById("loc-fb-time-value");
study.locTimeValue = locTimeValue;
let locPullLabel = document.getElementById("loc-fb-pull-label");
study.locPullLabel = locPullLabel;
let locPullValue = document.getElementById("loc-fb-pull-value");
study.locPullValue = locPullValue;
let locLoadLabel = document.getElementById("loc-fb-load-label");
study.locLoadLabel = locLoadLabel;
let locLoadValue = document.getElementById("loc-fb-load-value");
study.locLoadValue = locLoadValue;
let tabVis = document.getElementById("loc-fb");
study.tabVis = tabVis;


trialCounter.innerHTML = study.numPotRoutes;
fbNumRoutes.innerHTML = study.numRoutes;
fbPotRoutes.innerHTML = study.numPotRoutes -1;
 

//read graph edges and nodes and make them available
let json = this.parameters.json;
let data = JSON.parse(json);
let edges = {}
data.edges.forEach(element => {
  edges[element.name]=element.value
});
study.edges = edges; 

let nodeJson = this.parameters.node_json;
let nodeData = JSON.parse(nodeJson);
let nodes = {}
nodeData.nodes.forEach(element => {
  nodes[element.name]=element.value
});
study.nodes = nodes; 

/*get number of customers*/
let numCustomer = Object.keys(study.nodes).length; //.toString();
study.numCustomer = numCustomer;
addMyMenu(numCustomer=study.numCustomer);
/*define variables for feedback*/
const MAXTIME = parseInt(this.parameters.time_max);
study.MAXTIME = MAXTIME;
const MAXLOAD = parseInt(this.parameters.load_max);
study.MAXLOAD = MAXLOAD;
const MINCUSTOMERS = parseInt(this.parameters.n_customer);
study.MINCUSTOMERS = MINCUSTOMERS;
const MAXPULLS = parseInt(this.parameters.n_customer) + 4;
study.parameters.maxpulls = MAXPULLS;
study.MAXPULLS = MAXPULLS;
study.fbMinCustomers = parseInt(study.MINCUSTOMERS);
const IMAGE = this.parameters.image;

// make lists sortable and save sort result and costs

$(function() {
    $('#sortable2').sortable({
        connectWith: '#sortable1',
        items: "li:not(.unsortable)",
        placeholder: "ui-state-highlight",        
        over: function(event, ui) {
          if (study.configuration.problemState === "hard") {
            ui.helper.children($("span.list-item")).addClass(`invisible`)};
            },
        update: provideFeedback(numCustomer=numCustomer, image=IMAGE),
    }).disableSelection();
    $('#sortable1').sortable({
      opacity: 0.6,
      connectWith: '#sortable2',
      placeholder: "ui-state-highlight",
      over: function(event, ui) {
              if (study.configuration.problemState === "hard"){ 
                ui.helper.children($("span.list-item")).removeClass(`invisible`)};
      },  
    }).disableSelection();
});

// to exit loop
if (this.state.response === 'end'){
  this.parent.parent.parent.parent.end()
};

/*if (typeof study.conditions !== 'undefined' && study.conditions.constraints.pull.value == parseInt(Math.round(study.numCustomer)/2))
  {alert("Bitte übernehmen Sie!")}*/

/*if (typeof study.conditions !== 'undefined'){
  if(study.numPotRoutes % 2 == 0 && study.conditions.constraints.pull.value == Math.round(numCustomer)/2)
  {alert("Bitte übernehmen Sie!")};
}*/
},
              "before:prepare": function anonymous(
) {
  this.options.events['click button#continue'] = e => {
    this.options.datastore.state.response = 'continue'; 
    if (typeof study.conditions !== "undefined" && isSolutionValid(conditions=study.conditions)) {
     // study.numPotCustomers = study.numPotCustomers + study.fbMinCustomers;
      study.numRoutes = ++study.numRoutes;
    //study.numSatCustomers = study.numSatCustomers + study.conditions.result.length-2;
      this.end();
      } else {
        alert("ungültige Route");
        e.preventDefault();
};
};

this.options.events['click button#skip'] = e => {
    this.options.datastore.state.response = 'skip';
    //study.numPotCustomers = study.numPotCustomers + study.fbMinCustomers;
    this.end();};

}
            },
            "title": "Screen_Exercise2",
            "content": "\u003Cmain class=\"experiment\"\u003E\r\n  \u003Cdiv class=wrapper\u003E\r\n\r\n\r\n    \u003Cdiv class=\"trial\"\u003E\r\n      \u003Ch4 class=\"trial\"\u003E \u003Cspan id=\"trial-count\"\u003E\u003C\u002Fspan\u003E. Route \u003C\u002Fh4\u003E\r\n    \u003C\u002Fdiv\u003E\r\n\r\n    \u003Cdiv class=\"constraints\"\u003E\r\n      \u003Ch4\u003EVorgaben\u003C\u002Fh4\u003E\r\n       \u003Ctable class=\"tab-constraints\"\u003E\r\n          \u003Ctr\u003E\r\n            \u003Ctd\u003EZiel:\u003C\u002Ftd\u003E\r\n            \u003Ctd\u003E${this.parameters.n_customer} \u003C\u002Ftd\u003E\r\n            \u003Ctd\u003E Kunden\u003C\u002Ftd\u003E\r\n          \u003C\u002Ftr\u003E\r\n          \u003Ctr\u003E\r\n            \u003Ctd\u003EVerfügbare Züge:\u003C\u002Ftd\u003E\r\n            \u003Ctd\u003E${parseInt(this.parameters.n_customer) + 5}\u003C\u002Ftd\u003E\r\n            \u003Ctd\u003E \u003C\u002Ftd\u003E\r\n          \u003C\u002Ftr\u003E\r\n          \u003Ctr\u003E\r\n            \u003Ctd\u003EVerfügbare Zeit:\u003C\u002Ftd\u003E\r\n            \u003Ctd\u003E${this.parameters.time_max}\u003C\u002Ftd\u003E\r\n            \u003Ctd\u003EMinuten\u003C\u002Ftd\u003E\r\n          \u003C\u002Ftr\u003E\r\n          \u003Ctr\u003E\r\n            \u003Ctd\u003EVerfügbare Menge:\u003C\u002Ftd\u003E\r\n            \u003Ctd\u003E${this.parameters.load_max}\u003C\u002Ftd\u003E\r\n            \u003Ctd\u003EPortionen\u003C\u002Ftd\u003E\r\n          \u003C\u002Ftr\u003E\r\n        \u003C\u002Ftable\u003E\r\n     \u003C\u002Fdiv\u003E\r\n\r\n    \u003Cdiv class=\"graphdisplay\"\u003E\r\n        \u003Cimg class=\"graph\" src=${this.files[this.parameters.image]}\u003E\r\n    \u003C\u002Fdiv\u003E\r\n  \r\n    \u003Cdiv class=\"skip\"\u003E\r\n      \u003Cbutton id=\"skip\" class=\"left\"\u003E\r\n        Überspringen \u003C\u002Fbutton\u003E\r\n    \u003C\u002Fdiv\u003E\r\n\r\n    \u003Cdiv class=\"list1 left available\"\u003E\r\n        \u003Ch4\u003EVerfügbare Orte\u003C\u002Fh4\u003E\r\n        \u003Col id=\"sortable1\" class=\"connectedSortable\"\u003E \u003C!-- phys--hard\"\u003E--\u003E\r\n          \u003Cli id=\"1\" class=\"ui-state-default easy\"\u003E\u003Cspan class=\"list-item\"\u003E${this.parameters.customer_01}\u003C\u002Fspan\u003E\u003C\u002Fli\u003E\r\n          \u003Cli id=\"2\" class=\"ui-state-default\"\u003E\u003Cspan class=\"list-item\"\u003E${this.parameters.customer_02}\u003C\u002Fspan\u003E\u003C\u002Fli\u003E\r\n          \u003Cli id=\"3\" class=\"ui-state-default\"\u003E\u003Cspan class=\"list-item\"\u003E${this.parameters.customer_03}\u003C\u002Fspan\u003E\u003C\u002Fli\u003E\r\n          \u003Cli id=\"4\" class=\"ui-state-default\"\u003E\u003Cspan class=\"list-item\"\u003E${this.parameters.customer_04}\u003C\u002Fspan\u003E\u003C\u002Fli\u003E\r\n          \u003Cli id=\"5\" class=\"ui-state-default\"\u003E\u003Cspan class=\"list-item\"\u003E${this.parameters.customer_05}\u003C\u002Fspan\u003E\u003C\u002Fli\u003E\r\n          \u003Cli id=\"6\" class=\"ui-state-default\"\u003E\u003Cspan class=\"list-item\"\u003E${this.parameters.customer_06}\u003C\u002Fspan\u003E\u003C\u002Fli\u003E\r\n          \u003Cli id=\"7\" class=\"ui-state-default\"\u003E\u003Cspan class=\"list-item\"\u003E${this.parameters.customer_07}\u003C\u002Fspan\u003E\u003C\u002Fli\u003E\r\n          \u003Cli id=\"8\" class=\"ui-state-default\"\u003EMyMenu\u003C\u002Fli\u003E\r\n        \u003C\u002Fol\u003E\r\n    \u003C\u002Fdiv\u003E\r\n\r\n    \u003Cdiv class=\"list2 arranged\"\u003E\r\n        \u003Ch4\u003EGeplante Route\u003C\u002Fh4\u003E\r\n        \u003Col id=\"sortable2\" class=\"connectedSortable\" start=\"0\"\u003E\r\n        \u003C\u002Fol\u003E\r\n    \u003C\u002Fdiv\u003E\r\n\r\n    \u003Cdiv class=\"exit\"\u003E\r\n      \u003Cbutton id=\"end\" class=\"end left\" type=\"submit\"\u003E\r\n        Abbrechen\r\n      \u003C\u002Fbutton\u003E\r\n    \u003C\u002Fdiv\u003E\r\n\r\n  \u003Cdiv class=\"feedback\"\u003E\r\n      \u003Ch4\u003EStand dieser Route\u003C\u002Fh4\u003E\r\n\r\n       \u003Cp id=\"feedback\"\u003E\u003C\u002Fp\u003E\r\n       \r\n       \u003Ctable id=\"loc-fb\" class=\"tab-local-feedback invisible\"\u003E\r\n          \u003Ctr\u003E\r\n            \u003Ctd id=\"loc-fb-goal-label\"\u003EBenötigte Kunden:\u003C\u002Ftd\u003E\r\n            \u003Ctd id=\"loc-fb-goal-value\"\u003E${this.parameters.n_customer} \u003C\u002Ftd\u003E\r\n            \u003Ctd\u003E\u003C\u002Ftd\u003E\r\n          \u003C\u002Ftr\u003E\r\n          \u003Ctr\u003E\r\n            \u003Ctd id=\"loc-fb-pull-label\"\u003EVerfügbare Züge:\u003C\u002Ftd\u003E\r\n            \u003Ctd id=\"loc-fb-pull-value\"\u003E ${parseInt(this.parameters.n_customer) + 4}\u003C\u002Ftd\u003E\r\n            \u003Ctd\u003E\u003C\u002Ftd\u003E\r\n          \u003C\u002Ftr\u003E\r\n          \u003Ctr\u003E\r\n            \u003Ctd id=\"loc-fb-time-label\"\u003EVerfügbare Zeit:\u003C\u002Ftd\u003E\r\n            \u003Ctd id=\"loc-fb-time-value\"\u003E${this.parameters.time_max} \u003C\u002Ftd\u003E\r\n            \u003Ctd\u003EMin.\u003C\u002Ftd\u003E\r\n          \u003C\u002Ftr\u003E\r\n          \u003Ctr\u003E\r\n            \u003Ctd id=\"loc-fb-load-label\"\u003EVerfügbare Menge:\u003C\u002Ftd\u003E\r\n            \u003Ctd id=\"loc-fb-load-value\"\u003E${this.parameters.load_max} \u003C\u002Ftd\u003E\r\n            \u003Ctd\u003EPort.\u003C\u002Ftd\u003E\r\n          \u003C\u002Ftr\u003E\r\n        \u003C\u002Ftable\u003E\r\n     \u003C\u002Fdiv\u003E\r\n\r\n     \u003Cdiv class=\"global-feedback\"\u003E\r\n      \u003Ch4\u003E Ergebnis aller Routen\u003C\u002Fh4\u003E\r\n      \u003Cdiv class=\"alert\"\u003E\r\n        \u003Ctable class=\"tab-global-feedback\"\u003E\r\n          \u003Ctr\u003E\r\n            \u003Ctd\u003EFertige Routen:\u003C\u002Ftd\u003E\r\n            \u003Ctd id=\"numRoutes\"\u003E\u003C\u002Ftd\u003E\r\n          \u003C\u002Ftr\u003E\r\n          \u003Ctr\u003E\r\n            \u003Ctd\u003E Mögliche Routen \u003C\u002Ftd\u003E\r\n            \u003Ctd id=\"potRoutes\"\u003E\u003C\u002Ftd\u003E\r\n          \u003C\u002Ftr\u003E\r\n\r\n        \u003C\u002Ftable\u003E\r\n      \u003C\u002Fdiv\u003E\r\n     \u003C\u002Fdiv\u003E\r\n     \r\n    \u003Cdiv class=\"pause\"\u003E\r\n      \u003Cbutton disabled id=\"pause\" class=\"left\"\u003E\r\n        Pause \r\n      \u003C\u002Fbutton\u003E\r\n    \u003C\u002Fdiv\u003E\r\n    \u003Cdiv class=\"forward\"\u003E\r\n      \u003Cbutton id=\"continue\" class=\"right\"\u003E \r\n        Weiter\r\n      \u003C\u002Fbutton\u003E   \r\n    \u003C\u002Fdiv\u003E\r\n  \u003C\u002Fdiv\u003E\r\n\u003C\u002Fmain\u003E\r\n",
            "timeline": []
          }
        },
        {
          "type": "lab.flow.Loop",
          "templateParameters": [
            {
              "customer_01": "Anibil [30]",
              "customer_02": "BUL [50]",
              "customer_03": "Ekiseh [20]",
              "customer_04": "Enahit [40]",
              "customer_05": "Esisiv [40]",
              "customer_06": "HES [10]",
              "customer_07": "IWE [50]",
              "customer_08": "Narene [20]",
              "customer_09": "TET [40]",
              "customer_10": "MyMenu",
              "image": "rand_graph_0119.gv.png",
              "json": "{\"edges\": [{\"name\": \"1-4\", \"value\": 15}, {\"name\": \"1-5\", \"value\": 15}, {\"name\": \"1-6\", \"value\": 10}, {\"name\": \"1-9\", \"value\": 20}, {\"name\": \"2-4\", \"value\": 25}, {\"name\": \"2-7\", \"value\": 10}, {\"name\": \"2-9\", \"value\": 5}, {\"name\": \"3-4\", \"value\": 20}, {\"name\": \"3-6\", \"value\": 15}, {\"name\": \"3-10\", \"value\": 15}, {\"name\": \"3-8\", \"value\": 10}, {\"name\": \"4-1\", \"value\": 15}, {\"name\": \"4-2\", \"value\": 25}, {\"name\": \"4-3\", \"value\": 20}, {\"name\": \"4-6\", \"value\": 5}, {\"name\": \"4-7\", \"value\": 15}, {\"name\": \"4-8\", \"value\": 15}, {\"name\": \"5-1\", \"value\": 15}, {\"name\": \"5-8\", \"value\": 15}, {\"name\": \"5-9\", \"value\": 25}, {\"name\": \"6-1\", \"value\": 10}, {\"name\": \"6-3\", \"value\": 15}, {\"name\": \"6-4\", \"value\": 5}, {\"name\": \"6-7\", \"value\": 25}, {\"name\": \"6-10\", \"value\": 15}, {\"name\": \"6-9\", \"value\": 25}, {\"name\": \"7-2\", \"value\": 10}, {\"name\": \"7-4\", \"value\": 15}, {\"name\": \"7-6\", \"value\": 25}, {\"name\": \"7-10\", \"value\": 10}, {\"name\": \"7-9\", \"value\": 5}, {\"name\": \"10-3\", \"value\": 15}, {\"name\": \"10-6\", \"value\": 15}, {\"name\": \"10-7\", \"value\": 10}, {\"name\": \"10-9\", \"value\": 10}, {\"name\": \"8-3\", \"value\": 10}, {\"name\": \"8-4\", \"value\": 15}, {\"name\": \"8-5\", \"value\": 15}, {\"name\": \"9-1\", \"value\": 20}, {\"name\": \"9-2\", \"value\": 5}, {\"name\": \"9-5\", \"value\": 25}, {\"name\": \"9-6\", \"value\": 25}, {\"name\": \"9-7\", \"value\": 5}, {\"name\": \"9-10\", \"value\": 10}]}",
              "node_json": "{\"nodes\": [{\"name\": \"1\", \"value\": 30}, {\"name\": \"2\", \"value\": 50}, {\"name\": \"3\", \"value\": 20}, {\"name\": \"4\", \"value\": 40}, {\"name\": \"5\", \"value\": 40}, {\"name\": \"6\", \"value\": 10}, {\"name\": \"7\", \"value\": 50}, {\"name\": \"8\", \"value\": 20}, {\"name\": \"9\", \"value\": 40}, {\"name\": \"10\", \"value\": 0}]}",
              "n_customer": "6",
              "time_max": "115",
              "load_max": "245"
            }
          ],
          "sample": {
            "mode": "sequential",
            "n": ""
          },
          "files": {},
          "responses": {},
          "parameters": {},
          "messageHandlers": {
            "before:prepare": function anonymous(
) {

study.numRoutes = 0;
study.numPotRoutes = 0;
}
          },
          "title": "Exercise_3",
          "shuffleGroups": [],
          "template": {
            "type": "lab.html.Screen",
            "files": {
              "rand_graph_0119.gv.png": "embedded\u002F0167745eb138b063a478b95384967e03fa44fcff510bd1289d2a2fa647c5d759.png"
            },
            "responses": {
              "click button#end": "end",
              "click button#pause": "pause"
            },
            "parameters": {},
            "messageHandlers": {
              "run": function anonymous(
) {
//change layout according th selected factor level
if (study.configuration.declarative === "hard") {
		document.querySelector(".constraints").setAttribute('style','visibility:hidden');
	} else {
		document.querySelector(".constraints").removeAttribute('style');
	}

/*btnPause = document.querySelector("#pause")
if (study.configuration.problemState === "easy") {
		btnPause.classList.add("invisible");
};*/

elementList = document.querySelectorAll("ol, img");
if (study.configuration.physical === "hard"){
  elementList.forEach(element => element.classList.add("phys--hard", "blur"));
  } else {
    elementList.forEach(element => element.classList.add("phys--easy"))
    };


study.conditions = undefined;

if (study.configuration.declarative == "easy"){
  study.numPotRoutes = ++ study.numPotRoutes;
}

// make dom elements available for manipulations
let trialCounter = document.getElementById("trial-count");
let fbNumRoutes = document.getElementById("numRoutes");
let fbPotRoutes = document.getElementById("potRoutes");
let feedback = document.getElementById("feedback");
study.feedback = feedback;

let locGoalLabel = document.getElementById("loc-fb-goal-label");
study.locGoalLabel = locGoalLabel;
let locGoalValue = document.getElementById("loc-fb-goal-value");
study.locGoalValue = locGoalValue;
let locTimeLabel = document.getElementById("loc-fb-time-label");
study.locTimeLabel = locTimeLabel;
let locTimeValue = document.getElementById("loc-fb-time-value");
study.locTimeValue = locTimeValue;
let locPullLabel = document.getElementById("loc-fb-pull-label");
study.locPullLabel = locPullLabel;
let locPullValue = document.getElementById("loc-fb-pull-value");
study.locPullValue = locPullValue;
let locLoadLabel = document.getElementById("loc-fb-load-label");
study.locLoadLabel = locLoadLabel;
let locLoadValue = document.getElementById("loc-fb-load-value");
study.locLoadValue = locLoadValue;
let tabVis = document.getElementById("loc-fb");
study.tabVis = tabVis;


trialCounter.innerHTML = study.numPotRoutes;
fbNumRoutes.innerHTML = study.numRoutes;
fbPotRoutes.innerHTML = study.numPotRoutes -1;
 

//read graph edges and nodes and make them available
let json = this.parameters.json;
let data = JSON.parse(json);
let edges = {}
data.edges.forEach(element => {
  edges[element.name]=element.value
});
study.edges = edges; 

let nodeJson = this.parameters.node_json;
let nodeData = JSON.parse(nodeJson);
let nodes = {}
nodeData.nodes.forEach(element => {
  nodes[element.name]=element.value
});
study.nodes = nodes; 

/*get number of customers*/
let numCustomer = Object.keys(study.nodes).length; //.toString();
study.numCustomer = numCustomer;
addMyMenu(numCustomer=study.numCustomer);
/*define variables for feedback*/
const MAXTIME = parseInt(this.parameters.time_max);
study.MAXTIME = MAXTIME;
const MAXLOAD = parseInt(this.parameters.load_max);
study.MAXLOAD = MAXLOAD;
const MINCUSTOMERS = parseInt(this.parameters.n_customer);
study.MINCUSTOMERS = MINCUSTOMERS;
const MAXPULLS = parseInt(this.parameters.n_customer) + 4;
study.parameters.maxpulls = MAXPULLS;
study.MAXPULLS = MAXPULLS;
study.fbMinCustomers = parseInt(study.MINCUSTOMERS);
const IMAGE = this.parameters.image;

// make lists sortable and save sort result and costs

$(function() {
    $('#sortable2').sortable({
        connectWith: '#sortable1',
        items: "li:not(.unsortable)",
        placeholder: "ui-state-highlight",        
        over: function(event, ui) {
          if (study.configuration.problemState === "hard") {
            ui.helper.children($("span.list-item")).addClass(`invisible`)};
            },
        update: provideFeedback(numCustomer=numCustomer, image=IMAGE),
    }).disableSelection();
    $('#sortable1').sortable({
      opacity: 0.6,
      connectWith: '#sortable2',
      placeholder: "ui-state-highlight",
      over: function(event, ui) {
              if (study.configuration.problemState === "hard"){ 
                ui.helper.children($("span.list-item")).removeClass(`invisible`)};
      },  
    }).disableSelection();
});

// to exit loop
if (this.state.response === 'end'){
  this.parent.parent.parent.parent.end()
};

/*if (typeof study.conditions !== 'undefined' && study.conditions.constraints.pull.value == parseInt(Math.round(study.numCustomer)/2))
  {alert("Bitte übernehmen Sie!")}*/

/*if (typeof study.conditions !== 'undefined'){
  if(study.numPotRoutes % 2 == 0 && study.conditions.constraints.pull.value == Math.round(numCustomer)/2)
  {alert("Bitte übernehmen Sie!")};
}*/
},
              "before:prepare": function anonymous(
) {
  this.options.events['click button#continue'] = e => {
    this.options.datastore.state.response = 'continue'; 
    if (typeof study.conditions !== "undefined" && isSolutionValid(conditions=study.conditions)) {
     // study.numPotCustomers = study.numPotCustomers + study.fbMinCustomers;
      study.numRoutes = ++study.numRoutes;
    //study.numSatCustomers = study.numSatCustomers + study.conditions.result.length-2;
      this.end();
      } else {
        alert("ungültige Route");
        e.preventDefault();
};
};

this.options.events['click button#skip'] = e => {
    this.options.datastore.state.response = 'skip';
    //study.numPotCustomers = study.numPotCustomers + study.fbMinCustomers;
    this.end();};

}
            },
            "title": "Screen_Exercise3",
            "content": "\u003Cmain class=\"experiment\"\u003E\r\n  \u003Cdiv class=wrapper\u003E\r\n\r\n\r\n    \u003Cdiv class=\"trial\"\u003E\r\n      \u003Ch4 class=\"trial\"\u003E \u003Cspan id=\"trial-count\"\u003E\u003C\u002Fspan\u003E. Route \u003C\u002Fh4\u003E\r\n    \u003C\u002Fdiv\u003E\r\n\r\n    \u003Cdiv class=\"constraints\"\u003E\r\n      \u003Ch4\u003EVorgaben\u003C\u002Fh4\u003E\r\n       \u003Ctable class=\"tab-constraints\"\u003E\r\n          \u003Ctr\u003E\r\n            \u003Ctd\u003EZiel:\u003C\u002Ftd\u003E\r\n            \u003Ctd\u003E${this.parameters.n_customer} \u003C\u002Ftd\u003E\r\n            \u003Ctd\u003E Kunden\u003C\u002Ftd\u003E\r\n          \u003C\u002Ftr\u003E\r\n          \u003Ctr\u003E\r\n            \u003Ctd\u003EVerfügbare Züge:\u003C\u002Ftd\u003E\r\n            \u003Ctd\u003E${parseInt(this.parameters.n_customer) + 5}\u003C\u002Ftd\u003E\r\n            \u003Ctd\u003E \u003C\u002Ftd\u003E\r\n          \u003C\u002Ftr\u003E\r\n          \u003Ctr\u003E\r\n            \u003Ctd\u003EVerfügbare Zeit:\u003C\u002Ftd\u003E\r\n            \u003Ctd\u003E${this.parameters.time_max}\u003C\u002Ftd\u003E\r\n            \u003Ctd\u003EMinuten\u003C\u002Ftd\u003E\r\n          \u003C\u002Ftr\u003E\r\n          \u003Ctr\u003E\r\n            \u003Ctd\u003EVerfügbare Menge:\u003C\u002Ftd\u003E\r\n            \u003Ctd\u003E${this.parameters.load_max}\u003C\u002Ftd\u003E\r\n            \u003Ctd\u003EPortionen\u003C\u002Ftd\u003E\r\n          \u003C\u002Ftr\u003E\r\n        \u003C\u002Ftable\u003E\r\n     \u003C\u002Fdiv\u003E\r\n\r\n    \u003Cdiv class=\"graphdisplay\"\u003E\r\n        \u003Cimg class=\"graph\" src=${this.files[this.parameters.image]}\u003E\r\n    \u003C\u002Fdiv\u003E\r\n  \r\n    \u003Cdiv class=\"skip\"\u003E\r\n      \u003Cbutton id=\"skip\" class=\"left\"\u003E\r\n        Überspringen \u003C\u002Fbutton\u003E\r\n    \u003C\u002Fdiv\u003E\r\n\r\n    \u003Cdiv class=\"list1 left available\"\u003E\r\n        \u003Ch4\u003EVerfügbare Orte\u003C\u002Fh4\u003E\r\n        \u003Col id=\"sortable1\" class=\"connectedSortable\"\u003E \u003C!-- phys--hard\"\u003E--\u003E\r\n          \u003Cli id=\"1\" class=\"ui-state-default easy\"\u003E\u003Cspan class=\"list-item\"\u003E${this.parameters.customer_01}\u003C\u002Fspan\u003E\u003C\u002Fli\u003E\r\n          \u003Cli id=\"2\" class=\"ui-state-default\"\u003E\u003Cspan class=\"list-item\"\u003E${this.parameters.customer_02}\u003C\u002Fspan\u003E\u003C\u002Fli\u003E\r\n          \u003Cli id=\"3\" class=\"ui-state-default\"\u003E\u003Cspan class=\"list-item\"\u003E${this.parameters.customer_03}\u003C\u002Fspan\u003E\u003C\u002Fli\u003E\r\n          \u003Cli id=\"4\" class=\"ui-state-default\"\u003E\u003Cspan class=\"list-item\"\u003E${this.parameters.customer_04}\u003C\u002Fspan\u003E\u003C\u002Fli\u003E\r\n          \u003Cli id=\"5\" class=\"ui-state-default\"\u003E\u003Cspan class=\"list-item\"\u003E${this.parameters.customer_05}\u003C\u002Fspan\u003E\u003C\u002Fli\u003E\r\n          \u003Cli id=\"6\" class=\"ui-state-default\"\u003E\u003Cspan class=\"list-item\"\u003E${this.parameters.customer_06}\u003C\u002Fspan\u003E\u003C\u002Fli\u003E\r\n          \u003Cli id=\"7\" class=\"ui-state-default\"\u003E\u003Cspan class=\"list-item\"\u003E${this.parameters.customer_07}\u003C\u002Fspan\u003E\u003C\u002Fli\u003E\r\n          \u003Cli id=\"8\" class=\"ui-state-default\"\u003E\u003Cspan class=\"list-item\"\u003E${this.parameters.customer_08}\u003C\u002Fspan\u003E\u003C\u002Fli\u003E\r\n          \u003Cli id=\"9\" class=\"ui-state-default\"\u003E\u003Cspan class=\"list-item\"\u003E${this.parameters.customer_09}\u003C\u002Fspan\u003E\u003C\u002Fli\u003E\r\n          \u003Cli id=\"10\" class=\"ui-state-default\"\u003EMyMenu\u003C\u002Fli\u003E\r\n        \u003C\u002Fol\u003E\r\n    \u003C\u002Fdiv\u003E\r\n\r\n    \u003Cdiv class=\"list2 arranged\"\u003E\r\n        \u003Ch4\u003EGeplante Route\u003C\u002Fh4\u003E\r\n        \u003Col id=\"sortable2\" class=\"connectedSortable\" start=\"0\"\u003E\r\n        \u003C\u002Fol\u003E\r\n    \u003C\u002Fdiv\u003E\r\n\r\n    \u003Cdiv class=\"exit\"\u003E\r\n      \u003Cbutton id=\"end\" class=\"end left\" type=\"submit\"\u003E\r\n        Abbrechen\r\n      \u003C\u002Fbutton\u003E\r\n    \u003C\u002Fdiv\u003E\r\n\r\n  \u003Cdiv class=\"feedback\"\u003E\r\n      \u003Ch4\u003EStand dieser Route\u003C\u002Fh4\u003E\r\n\r\n       \u003Cp id=\"feedback\"\u003E\u003C\u002Fp\u003E\r\n       \r\n       \u003Ctable id=\"loc-fb\" class=\"tab-local-feedback invisible\"\u003E\r\n          \u003Ctr\u003E\r\n            \u003Ctd id=\"loc-fb-goal-label\"\u003EBenötigte Kunden:\u003C\u002Ftd\u003E\r\n            \u003Ctd id=\"loc-fb-goal-value\"\u003E${this.parameters.n_customer} \u003C\u002Ftd\u003E\r\n            \u003Ctd\u003E\u003C\u002Ftd\u003E\r\n          \u003C\u002Ftr\u003E\r\n          \u003Ctr\u003E\r\n            \u003Ctd id=\"loc-fb-pull-label\"\u003EVerfügbare Züge:\u003C\u002Ftd\u003E\r\n            \u003Ctd id=\"loc-fb-pull-value\"\u003E ${parseInt(this.parameters.n_customer) + 4}\u003C\u002Ftd\u003E\r\n            \u003Ctd\u003E\u003C\u002Ftd\u003E\r\n          \u003C\u002Ftr\u003E\r\n          \u003Ctr\u003E\r\n            \u003Ctd id=\"loc-fb-time-label\"\u003EVerfügbare Zeit:\u003C\u002Ftd\u003E\r\n            \u003Ctd id=\"loc-fb-time-value\"\u003E${this.parameters.time_max} \u003C\u002Ftd\u003E\r\n            \u003Ctd\u003EMin.\u003C\u002Ftd\u003E\r\n          \u003C\u002Ftr\u003E\r\n          \u003Ctr\u003E\r\n            \u003Ctd id=\"loc-fb-load-label\"\u003EVerfügbare Menge:\u003C\u002Ftd\u003E\r\n            \u003Ctd id=\"loc-fb-load-value\"\u003E${this.parameters.load_max} \u003C\u002Ftd\u003E\r\n            \u003Ctd\u003EPort.\u003C\u002Ftd\u003E\r\n          \u003C\u002Ftr\u003E\r\n        \u003C\u002Ftable\u003E\r\n     \u003C\u002Fdiv\u003E\r\n\r\n     \u003Cdiv class=\"global-feedback\"\u003E\r\n      \u003Ch4\u003E Ergebnis aller Routen\u003C\u002Fh4\u003E\r\n      \u003Cdiv class=\"alert\"\u003E\r\n        \u003Ctable class=\"tab-global-feedback\"\u003E\r\n          \u003Ctr\u003E\r\n            \u003Ctd\u003EFertige Routen:\u003C\u002Ftd\u003E\r\n            \u003Ctd id=\"numRoutes\"\u003E\u003C\u002Ftd\u003E\r\n          \u003C\u002Ftr\u003E\r\n          \u003Ctr\u003E\r\n            \u003Ctd\u003E Mögliche Routen \u003C\u002Ftd\u003E\r\n            \u003Ctd id=\"potRoutes\"\u003E\u003C\u002Ftd\u003E\r\n          \u003C\u002Ftr\u003E\r\n\r\n        \u003C\u002Ftable\u003E\r\n      \u003C\u002Fdiv\u003E\r\n     \u003C\u002Fdiv\u003E\r\n     \r\n    \u003Cdiv class=\"pause\"\u003E\r\n      \u003Cbutton disabled id=\"pause\" class=\"left\"\u003E\r\n        Pause \r\n      \u003C\u002Fbutton\u003E\r\n    \u003C\u002Fdiv\u003E\r\n    \u003Cdiv class=\"forward\"\u003E\r\n      \u003Cbutton id=\"continue\" class=\"right\"\u003E \r\n        Weiter\r\n      \u003C\u002Fbutton\u003E   \r\n    \u003C\u002Fdiv\u003E\r\n  \u003C\u002Fdiv\u003E\r\n\u003C\u002Fmain\u003E\r\n",
            "timeline": []
          }
        }
      ]
    },
    {
      "type": "lab.flow.Loop",
      "templateParameters": [
        {
          "TrafficScenario": "1.3.01-111.jpg",
          "TrafficQuestion": "Welches Verhalten ist richtig?",
          "OptionA": "Ich muss das Motorrad abbiegen lassen",
          "OptionB": "Ich darf erst als Letzter fahren",
          "OptionC": "Ich darf vor dem blauen Pkw fahren",
          "physical": "easy",
          "declarative": "easy",
          "problemState": "easy",
          "block": "1"
        },
        {
          "TrafficScenario": "1.2.37-015.jpg",
          "TrafficQuestion": "Was gilt hier?",
          "OptionA": "Ich muss in der Kreuzungsmitte anhalten",
          "OptionB": "Ich darf ungehindert abbiegen",
          "OptionC": "Ich muss den Gegenverkehr durchfahren lassen",
          "physical": "hard",
          "declarative": "easy",
          "problemState": "easy",
          "block": "2"
        },
        {
          "TrafficScenario": "1.3.01-022.jpg",
          "TrafficQuestion": "Welches Verhalten ist richtig?",
          "OptionA": "Ich muss die Straßenbahn vorbeilassen",
          "OptionB": "Ich darf vor der Straßenbahn abbiegen",
          "OptionC": "Ich muss den Traktor vorbeilassen",
          "physical": "easy",
          "declarative": "hard",
          "problemState": "easy",
          "block": "3"
        },
        {
          "TrafficScenario": "1.1.02-131.jpg",
          "TrafficQuestion": "Womit müssen Sie rechnen?",
          "OptionA": "Dass Fahrzeuge anfahren",
          "OptionB": "Dass Fußgänger plötzlich auf die Fahrbahn treten",
          "OptionC": "Dass Fahrzeugtüren geöffnet werden",
          "physical": "easy",
          "declarative": "easy",
          "problemState": "hard",
          "block": "4"
        },
        {
          "TrafficScenario": "1.4.41-137.jpg",
          "TrafficQuestion": "Was müssen Sie bei dieser Linie in der      Fahrbahnmitte beachten? Sie dürfen die Linie …",
          "OptionA": "… nur überqueren, wenn es der Gegenverkehr zulässt",
          "OptionB": "… nicht überqueren oder über ihr fahren",
          "OptionC": "… nur zum Überholen überqueren",
          "physical": "hard",
          "declarative": "hard",
          "problemState": "hard",
          "block": "5"
        }
      ],
      "sample": {
        "mode": "sequential"
      },
      "files": {},
      "responses": {},
      "parameters": {},
      "messageHandlers": {},
      "title": "Loop_Interruption",
      "shuffleGroups": [],
      "template": {
        "type": "lab.flow.Sequence",
        "files": {},
        "responses": {},
        "parameters": {},
        "messageHandlers": {
          "before:prepare": function anonymous(
) {
let interruptionContent={
   image: this.parameters.TrafficScenario,
   question: this.parameters.TrafficQuestion, 
   optionA: this.parameters.OptionA,
   optionB: this.parameters.OptionB,
   optionC: this.parameters.OptionC
   }; 
  study.interruptionContent = interruptionContent;

  let configuration={
   physical: this.parameters.physical, 
   declarative: this.parameters.declarative,
   problemState: this.parameters.problemState,
   }; 
  study.configuration = configuration;  

  study.numBlock = this.parameters.block;
}
        },
        "title": "Sequence",
        "tardy": true,
        "content": [
          {
            "type": "lab.html.Screen",
            "files": {},
            "responses": {
              "click button#continue": "continue"
            },
            "parameters": {},
            "messageHandlers": {
              "run": function anonymous(
) {
const trialInfo = document.querySelector(".trial-info");

if (study.configuration.physical === "hard"){
  trialMessagePhysical = "<p> Wundern Sie sich nicht: Durch eine technische Störung werden die Routeninformationen <strong>in verminderter Qualität angezeigt</strong>.</p>";
  } else {trialMessagePhysical = ""};

if (study.configuration.declarative === "hard"){
  trialMessageDeclarative = "<p> Die Auftragsdaten wurden unvollständig in das System eingepflegt. Nun werden die Routenvorgaben vor den Kundenlisten angezeigt. <strong> Prägen Sie sich die Routenvorgaben gut ein</strong>, damit Sie diese während der Routenplanung berücksichtigen können.</p>";
  } else {trialMessageDeclarative = ""};

if (study.configuration.problemState === "hard"){
    trialMessageProblemState = "<p> Ziel ist es, möglichst <strong>viele vollständige Routen </strong>zusammenzustellen und dabei möglichst<strong> wenige Kundenlisten ungenutzt </strong>zu lassen.</p><p>Ihr Vorgesetzter teilt Ihnen mit, dass die Auftragslage momentan schlecht ist. Die <strong>Menge möglicher Routen ist begrenzt</strong>. Jeder zufriedene Kunde ist besonders kostbar. Sie sollen möglichst aus jeder Kundenliste eine passende Route zusammenstellen. <strong>Beenden Sie nach einer Unterbrechung unbedingt die zuvor begonnene Route!</strong> </p>"
  } else {
     trialMessageProblemState = "<p>Ziel ist es, möglichst <strong>viele vollständige Routen</strong> zusammenzustellen. </p><p>Ihr Vorgesetzter teilt Ihnen mit, dass die Auftragslage momentan gut ist. Die <strong>Menge möglicher Routen ist umfangreich</strong>. Selbstverständlich sollen möglichst viele Kunden zufriedengestellt werden, aber es ist nicht zwingend erforderlich, dass Sie aus jeder Kundenliste eine passende Route zusammenstellen. <strong>Beginnen Sie nach einer Unterbrechung einfach die nächste Route!</strong> </p>"
  }

trialInfo.insertAdjacentHTML('beforeend', trialMessageProblemState);
trialInfo.insertAdjacentHTML('beforeend', trialMessagePhysical);
trialInfo.insertAdjacentHTML('beforeend', trialMessageDeclarative);



}
            },
            "title": "Screen_Information",
            "content": "\u003Cheader\u003E\r\n  \u003Ch1\u003EBesonderheiten der folgenden Aufgaben\u003C\u002Fh1\u003E\r\n\u003C\u002Fheader\u003E\r\n\r\n\u003Cmain\u003E\r\n\u003Cdiv class=\"trial-info\"\u003E\u003C\u002Fdiv\u003E\r\n\u003C\u002Fmain\u003E\r\n\r\n\r\n\u003Cfooter\u003E\r\n  \u003Cbutton id=\"continue\" class=\"right\"\u003E \r\n    Weiter\r\n  \u003C\u002Fbutton\u003E  \r\n\u003C\u002Ffooter\u003E"
          },
          {
            "type": "lab.flow.Loop",
            "templateParameters": [
              {
                "block_level": "1",
                "customer_01": "Arit [50]",
                "customer_02": "Atenun [30]",
                "customer_03": "Emete [50]",
                "customer_04": "Hatu [30]",
                "customer_05": "Mirem [10]",
                "customer_06": "Nehul [40]",
                "customer_07": "Rone [10]",
                "customer_08": "Tuneh [30]",
                "customer_09": "Uzen [30]",
                "customer_10": "MyMenu",
                "image": "rand_graph_0000.gv.png",
                "json": "{\"edges\": [{\"name\": \"1-2\", \"value\": 10}, {\"name\": \"1-3\", \"value\": 5}, {\"name\": \"1-4\", \"value\": 25}, {\"name\": \"1-10\", \"value\": 25}, {\"name\": \"1-7\", \"value\": 25}, {\"name\": \"2-1\", \"value\": 10}, {\"name\": \"2-3\", \"value\": 15}, {\"name\": \"2-5\", \"value\": 5}, {\"name\": \"2-6\", \"value\": 5}, {\"name\": \"2-8\", \"value\": 20}, {\"name\": \"3-1\", \"value\": 5}, {\"name\": \"3-2\", \"value\": 15}, {\"name\": \"3-8\", \"value\": 5}, {\"name\": \"4-1\", \"value\": 25}, {\"name\": \"4-5\", \"value\": 15}, {\"name\": \"4-6\", \"value\": 15}, {\"name\": \"4-7\", \"value\": 20}, {\"name\": \"4-8\", \"value\": 15}, {\"name\": \"4-9\", \"value\": 15}, {\"name\": \"5-2\", \"value\": 5}, {\"name\": \"5-4\", \"value\": 15}, {\"name\": \"5-9\", \"value\": 25}, {\"name\": \"10-1\", \"value\": 25}, {\"name\": \"10-6\", \"value\": 15}, {\"name\": \"10-8\", \"value\": 5}, {\"name\": \"10-9\", \"value\": 25}, {\"name\": \"6-2\", \"value\": 5}, {\"name\": \"6-4\", \"value\": 15}, {\"name\": \"6-10\", \"value\": 15}, {\"name\": \"6-9\", \"value\": 15}, {\"name\": \"7-1\", \"value\": 25}, {\"name\": \"7-4\", \"value\": 20}, {\"name\": \"7-9\", \"value\": 25}, {\"name\": \"8-2\", \"value\": 20}, {\"name\": \"8-3\", \"value\": 5}, {\"name\": \"8-4\", \"value\": 15}, {\"name\": \"8-10\", \"value\": 5}, {\"name\": \"8-9\", \"value\": 5}, {\"name\": \"9-4\", \"value\": 15}, {\"name\": \"9-5\", \"value\": 25}, {\"name\": \"9-10\", \"value\": 25}, {\"name\": \"9-6\", \"value\": 15}, {\"name\": \"9-7\", \"value\": 25}, {\"name\": \"9-8\", \"value\": 5}]}",
                "node_json": "{\"nodes\": [{\"name\": \"1\", \"value\": 50}, {\"name\": \"2\", \"value\": 30}, {\"name\": \"3\", \"value\": 50}, {\"name\": \"4\", \"value\": 30}, {\"name\": \"5\", \"value\": 10}, {\"name\": \"6\", \"value\": 40}, {\"name\": \"7\", \"value\": 10}, {\"name\": \"8\", \"value\": 30}, {\"name\": \"9\", \"value\": 30}, {\"name\": \"10\", \"value\": 0}]}",
                "n_customer": "7",
                "time_max": "145",
                "load_max": "235"
              },
              {
                "block_level": "1",
                "customer_01": "FES [30]",
                "customer_02": "Ihid [10]",
                "customer_03": "Irecem [20]",
                "customer_04": "Iwoci [50]",
                "customer_05": "Necego [40]",
                "customer_06": "Nelacu [20]",
                "customer_07": "Nusona [50]",
                "customer_08": "SAR [20]",
                "customer_09": "Simib [10]",
                "customer_10": "MyMenu",
                "image": "rand_graph_0001.gv.png",
                "json": "{\"edges\": [{\"name\": \"1-5\", \"value\": 10}, {\"name\": \"1-6\", \"value\": 25}, {\"name\": \"1-9\", \"value\": 15}, {\"name\": \"2-4\", \"value\": 15}, {\"name\": \"2-5\", \"value\": 25}, {\"name\": \"2-6\", \"value\": 25}, {\"name\": \"2-9\", \"value\": 10}, {\"name\": \"3-5\", \"value\": 10}, {\"name\": \"3-6\", \"value\": 20}, {\"name\": \"3-7\", \"value\": 10}, {\"name\": \"4-2\", \"value\": 15}, {\"name\": \"4-5\", \"value\": 15}, {\"name\": \"4-6\", \"value\": 10}, {\"name\": \"4-7\", \"value\": 25}, {\"name\": \"4-8\", \"value\": 25}, {\"name\": \"10-5\", \"value\": 20}, {\"name\": \"10-6\", \"value\": 25}, {\"name\": \"10-7\", \"value\": 5}, {\"name\": \"10-9\", \"value\": 20}, {\"name\": \"5-1\", \"value\": 10}, {\"name\": \"5-2\", \"value\": 25}, {\"name\": \"5-3\", \"value\": 10}, {\"name\": \"5-4\", \"value\": 15}, {\"name\": \"5-10\", \"value\": 20}, {\"name\": \"6-1\", \"value\": 25}, {\"name\": \"6-2\", \"value\": 25}, {\"name\": \"6-3\", \"value\": 20}, {\"name\": \"6-4\", \"value\": 10}, {\"name\": \"6-10\", \"value\": 25}, {\"name\": \"6-7\", \"value\": 10}, {\"name\": \"6-8\", \"value\": 25}, {\"name\": \"6-9\", \"value\": 15}, {\"name\": \"7-3\", \"value\": 10}, {\"name\": \"7-4\", \"value\": 25}, {\"name\": \"7-10\", \"value\": 5}, {\"name\": \"7-6\", \"value\": 10}, {\"name\": \"8-4\", \"value\": 25}, {\"name\": \"8-6\", \"value\": 25}, {\"name\": \"8-9\", \"value\": 5}, {\"name\": \"9-1\", \"value\": 15}, {\"name\": \"9-2\", \"value\": 10}, {\"name\": \"9-10\", \"value\": 20}, {\"name\": \"9-6\", \"value\": 15}, {\"name\": \"9-8\", \"value\": 5}]}",
                "node_json": "{\"nodes\": [{\"name\": \"1\", \"value\": 30}, {\"name\": \"2\", \"value\": 10}, {\"name\": \"3\", \"value\": 20}, {\"name\": \"4\", \"value\": 50}, {\"name\": \"5\", \"value\": 40}, {\"name\": \"6\", \"value\": 20}, {\"name\": \"7\", \"value\": 50}, {\"name\": \"8\", \"value\": 20}, {\"name\": \"9\", \"value\": 10}, {\"name\": \"10\", \"value\": 0}]}",
                "n_customer": "7",
                "time_max": "175",
                "load_max": "255"
              },
              {
                "block_level": "1",
                "customer_01": "Bagen [50]",
                "customer_02": "Genihe [10]",
                "customer_03": "Lebemu [20]",
                "customer_04": "Nanimi [20]",
                "customer_05": "Ninil [10]",
                "customer_06": "Nuretu [30]",
                "customer_07": "Relene [20]",
                "customer_08": "Reta [50]",
                "customer_09": "Toyah [20]",
                "customer_10": "MyMenu",
                "image": "rand_graph_0003.gv.png",
                "json": "{\"edges\": [{\"name\": \"1-3\", \"value\": 15}, {\"name\": \"1-4\", \"value\": 20}, {\"name\": \"1-5\", \"value\": 5}, {\"name\": \"1-6\", \"value\": 5}, {\"name\": \"1-7\", \"value\": 20}, {\"name\": \"1-9\", \"value\": 25}, {\"name\": \"2-4\", \"value\": 15}, {\"name\": \"2-7\", \"value\": 25}, {\"name\": \"2-9\", \"value\": 25}, {\"name\": \"3-1\", \"value\": 15}, {\"name\": \"3-6\", \"value\": 15}, {\"name\": \"10-4\", \"value\": 10}, {\"name\": \"10-6\", \"value\": 5}, {\"name\": \"10-7\", \"value\": 15}, {\"name\": \"10-8\", \"value\": 5}, {\"name\": \"4-1\", \"value\": 20}, {\"name\": \"4-2\", \"value\": 15}, {\"name\": \"4-10\", \"value\": 10}, {\"name\": \"4-5\", \"value\": 15}, {\"name\": \"4-6\", \"value\": 5}, {\"name\": \"4-7\", \"value\": 10}, {\"name\": \"4-8\", \"value\": 20}, {\"name\": \"5-1\", \"value\": 5}, {\"name\": \"5-4\", \"value\": 15}, {\"name\": \"5-8\", \"value\": 10}, {\"name\": \"6-1\", \"value\": 5}, {\"name\": \"6-3\", \"value\": 15}, {\"name\": \"6-10\", \"value\": 5}, {\"name\": \"6-4\", \"value\": 5}, {\"name\": \"6-7\", \"value\": 5}, {\"name\": \"7-1\", \"value\": 20}, {\"name\": \"7-2\", \"value\": 25}, {\"name\": \"7-10\", \"value\": 15}, {\"name\": \"7-4\", \"value\": 10}, {\"name\": \"7-6\", \"value\": 5}, {\"name\": \"7-9\", \"value\": 5}, {\"name\": \"8-10\", \"value\": 5}, {\"name\": \"8-4\", \"value\": 20}, {\"name\": \"8-5\", \"value\": 10}, {\"name\": \"8-9\", \"value\": 20}, {\"name\": \"9-1\", \"value\": 25}, {\"name\": \"9-2\", \"value\": 25}, {\"name\": \"9-7\", \"value\": 5}, {\"name\": \"9-8\", \"value\": 20}]}",
                "node_json": "{\"nodes\": [{\"name\": \"1\", \"value\": 50}, {\"name\": \"2\", \"value\": 10}, {\"name\": \"3\", \"value\": 20}, {\"name\": \"4\", \"value\": 20}, {\"name\": \"5\", \"value\": 10}, {\"name\": \"6\", \"value\": 30}, {\"name\": \"7\", \"value\": 20}, {\"name\": \"8\", \"value\": 50}, {\"name\": \"9\", \"value\": 20}, {\"name\": \"10\", \"value\": 0}]}",
                "n_customer": "8",
                "time_max": "150",
                "load_max": "235"
              },
              {
                "block_level": "2",
                "customer_01": "Anibil [50]",
                "customer_02": "Cotu [40]",
                "customer_03": "Ehiba [40]",
                "customer_04": "Girep [20]",
                "customer_05": "IWE [10]",
                "customer_06": "Ilan [20]",
                "customer_07": "Itur [30]",
                "customer_08": "Keviw [40]",
                "customer_09": "Nune [10]",
                "customer_10": "MyMenu",
                "image": "rand_graph_0004.gv.png",
                "json": "{\"edges\": [{\"name\": \"1-3\", \"value\": 15}, {\"name\": \"1-5\", \"value\": 5}, {\"name\": \"1-7\", \"value\": 20}, {\"name\": \"1-9\", \"value\": 15}, {\"name\": \"2-4\", \"value\": 5}, {\"name\": \"2-8\", \"value\": 20}, {\"name\": \"3-1\", \"value\": 15}, {\"name\": \"3-6\", \"value\": 10}, {\"name\": \"3-8\", \"value\": 25}, {\"name\": \"3-10\", \"value\": 25}, {\"name\": \"4-2\", \"value\": 5}, {\"name\": \"4-5\", \"value\": 15}, {\"name\": \"4-8\", \"value\": 20}, {\"name\": \"4-10\", \"value\": 20}, {\"name\": \"4-9\", \"value\": 15}, {\"name\": \"5-1\", \"value\": 5}, {\"name\": \"5-4\", \"value\": 15}, {\"name\": \"5-6\", \"value\": 20}, {\"name\": \"5-7\", \"value\": 15}, {\"name\": \"5-10\", \"value\": 20}, {\"name\": \"5-9\", \"value\": 5}, {\"name\": \"6-3\", \"value\": 10}, {\"name\": \"6-5\", \"value\": 20}, {\"name\": \"6-8\", \"value\": 20}, {\"name\": \"6-9\", \"value\": 15}, {\"name\": \"7-1\", \"value\": 20}, {\"name\": \"7-5\", \"value\": 15}, {\"name\": \"7-9\", \"value\": 10}, {\"name\": \"8-2\", \"value\": 20}, {\"name\": \"8-3\", \"value\": 25}, {\"name\": \"8-4\", \"value\": 20}, {\"name\": \"8-6\", \"value\": 20}, {\"name\": \"8-10\", \"value\": 15}, {\"name\": \"8-9\", \"value\": 20}, {\"name\": \"10-3\", \"value\": 25}, {\"name\": \"10-4\", \"value\": 20}, {\"name\": \"10-5\", \"value\": 20}, {\"name\": \"10-8\", \"value\": 15}, {\"name\": \"9-1\", \"value\": 15}, {\"name\": \"9-4\", \"value\": 15}, {\"name\": \"9-5\", \"value\": 5}, {\"name\": \"9-6\", \"value\": 15}, {\"name\": \"9-7\", \"value\": 10}, {\"name\": \"9-8\", \"value\": 20}]}",
                "node_json": "{\"nodes\": [{\"name\": \"1\", \"value\": 50}, {\"name\": \"2\", \"value\": 40}, {\"name\": \"3\", \"value\": 40}, {\"name\": \"4\", \"value\": 20}, {\"name\": \"5\", \"value\": 10}, {\"name\": \"6\", \"value\": 20}, {\"name\": \"7\", \"value\": 30}, {\"name\": \"8\", \"value\": 40}, {\"name\": \"9\", \"value\": 10}, {\"name\": \"10\", \"value\": 0}]}",
                "n_customer": "7",
                "time_max": "175",
                "load_max": "245"
              },
              {
                "block_level": "2",
                "customer_01": "EZA [10]",
                "customer_02": "Ezis [10]",
                "customer_03": "Ibeneg [50]",
                "customer_04": "Minan [40]",
                "customer_05": "Nuretu [30]",
                "customer_06": "Oseh [20]",
                "customer_07": "Selara [40]",
                "customer_08": "Tarebu [30]",
                "customer_09": "Tise [50]",
                "customer_10": "MyMenu",
                "image": "rand_graph_0005.gv.png",
                "json": "{\"edges\": [{\"name\": \"1-2\", \"value\": 20}, {\"name\": \"1-3\", \"value\": 20}, {\"name\": \"1-4\", \"value\": 20}, {\"name\": \"1-6\", \"value\": 20}, {\"name\": \"2-1\", \"value\": 20}, {\"name\": \"2-4\", \"value\": 15}, {\"name\": \"2-5\", \"value\": 15}, {\"name\": \"2-8\", \"value\": 15}, {\"name\": \"3-1\", \"value\": 20}, {\"name\": \"3-10\", \"value\": 5}, {\"name\": \"3-6\", \"value\": 10}, {\"name\": \"3-7\", \"value\": 20}, {\"name\": \"3-8\", \"value\": 25}, {\"name\": \"3-9\", \"value\": 15}, {\"name\": \"4-1\", \"value\": 20}, {\"name\": \"4-2\", \"value\": 15}, {\"name\": \"10-3\", \"value\": 5}, {\"name\": \"10-7\", \"value\": 15}, {\"name\": \"10-8\", \"value\": 10}, {\"name\": \"10-9\", \"value\": 25}, {\"name\": \"5-2\", \"value\": 15}, {\"name\": \"5-8\", \"value\": 20}, {\"name\": \"5-9\", \"value\": 5}, {\"name\": \"6-1\", \"value\": 20}, {\"name\": \"6-3\", \"value\": 10}, {\"name\": \"6-7\", \"value\": 10}, {\"name\": \"6-9\", \"value\": 10}, {\"name\": \"7-3\", \"value\": 20}, {\"name\": \"7-10\", \"value\": 15}, {\"name\": \"7-6\", \"value\": 10}, {\"name\": \"7-8\", \"value\": 15}, {\"name\": \"7-9\", \"value\": 25}, {\"name\": \"8-2\", \"value\": 15}, {\"name\": \"8-3\", \"value\": 25}, {\"name\": \"8-10\", \"value\": 10}, {\"name\": \"8-5\", \"value\": 20}, {\"name\": \"8-7\", \"value\": 15}, {\"name\": \"8-9\", \"value\": 20}, {\"name\": \"9-3\", \"value\": 15}, {\"name\": \"9-10\", \"value\": 25}, {\"name\": \"9-5\", \"value\": 5}, {\"name\": \"9-6\", \"value\": 10}, {\"name\": \"9-7\", \"value\": 25}, {\"name\": \"9-8\", \"value\": 20}]}",
                "node_json": "{\"nodes\": [{\"name\": \"1\", \"value\": 10}, {\"name\": \"2\", \"value\": 10}, {\"name\": \"3\", \"value\": 50}, {\"name\": \"4\", \"value\": 40}, {\"name\": \"5\", \"value\": 30}, {\"name\": \"6\", \"value\": 20}, {\"name\": \"7\", \"value\": 40}, {\"name\": \"8\", \"value\": 30}, {\"name\": \"9\", \"value\": 50}, {\"name\": \"10\", \"value\": 0}]}",
                "n_customer": "7",
                "time_max": "150",
                "load_max": "245"
              },
              {
                "block_level": "2",
                "customer_01": "Gedese [50]",
                "customer_02": "Gisom [50]",
                "customer_03": "Hirel [50]",
                "customer_04": "Iwitah [30]",
                "customer_05": "Lesar [50]",
                "customer_06": "MEM [40]",
                "customer_07": "NIN [10]",
                "customer_08": "Rire [40]",
                "customer_09": "Rofuk [30]",
                "customer_10": "MyMenu",
                "image": "rand_graph_0006.gv.png",
                "json": "{\"edges\": [{\"name\": \"1-3\", \"value\": 20}, {\"name\": \"1-6\", \"value\": 5}, {\"name\": \"1-10\", \"value\": 25}, {\"name\": \"1-8\", \"value\": 20}, {\"name\": \"1-9\", \"value\": 10}, {\"name\": \"2-3\", \"value\": 10}, {\"name\": \"2-10\", \"value\": 15}, {\"name\": \"2-8\", \"value\": 25}, {\"name\": \"3-1\", \"value\": 20}, {\"name\": \"3-2\", \"value\": 10}, {\"name\": \"3-4\", \"value\": 15}, {\"name\": \"3-5\", \"value\": 25}, {\"name\": \"3-10\", \"value\": 25}, {\"name\": \"3-7\", \"value\": 10}, {\"name\": \"3-8\", \"value\": 15}, {\"name\": \"3-9\", \"value\": 15}, {\"name\": \"4-3\", \"value\": 15}, {\"name\": \"4-5\", \"value\": 20}, {\"name\": \"4-7\", \"value\": 5}, {\"name\": \"5-3\", \"value\": 25}, {\"name\": \"5-4\", \"value\": 20}, {\"name\": \"5-8\", \"value\": 10}, {\"name\": \"6-1\", \"value\": 5}, {\"name\": \"6-7\", \"value\": 15}, {\"name\": \"10-1\", \"value\": 25}, {\"name\": \"10-2\", \"value\": 15}, {\"name\": \"10-3\", \"value\": 25}, {\"name\": \"10-9\", \"value\": 15}, {\"name\": \"7-3\", \"value\": 10}, {\"name\": \"7-4\", \"value\": 5}, {\"name\": \"7-6\", \"value\": 15}, {\"name\": \"7-8\", \"value\": 5}, {\"name\": \"7-9\", \"value\": 10}, {\"name\": \"8-1\", \"value\": 20}, {\"name\": \"8-2\", \"value\": 25}, {\"name\": \"8-3\", \"value\": 15}, {\"name\": \"8-5\", \"value\": 10}, {\"name\": \"8-7\", \"value\": 5}, {\"name\": \"8-9\", \"value\": 25}, {\"name\": \"9-1\", \"value\": 10}, {\"name\": \"9-3\", \"value\": 15}, {\"name\": \"9-10\", \"value\": 15}, {\"name\": \"9-7\", \"value\": 10}, {\"name\": \"9-8\", \"value\": 25}]}",
                "node_json": "{\"nodes\": [{\"name\": \"1\", \"value\": 50}, {\"name\": \"2\", \"value\": 50}, {\"name\": \"3\", \"value\": 50}, {\"name\": \"4\", \"value\": 30}, {\"name\": \"5\", \"value\": 50}, {\"name\": \"6\", \"value\": 40}, {\"name\": \"7\", \"value\": 10}, {\"name\": \"8\", \"value\": 40}, {\"name\": \"9\", \"value\": 30}, {\"name\": \"10\", \"value\": 0}]}",
                "n_customer": "5",
                "time_max": "110",
                "load_max": "235"
              },
              {
                "block_level": "3",
                "customer_01": "Arit [10]",
                "customer_02": "DED [10]",
                "customer_03": "EHU [50]",
                "customer_04": "Enena [50]",
                "customer_05": "Hegehe [30]",
                "customer_06": "Izigan [40]",
                "customer_07": "Nesis [20]",
                "customer_08": "Nuho [10]",
                "customer_09": "Ziri [40]",
                "customer_10": "MyMenu",
                "image": "rand_graph_0007.gv.png",
                "json": "{\"edges\": [{\"name\": \"1-3\", \"value\": 5}, {\"name\": \"1-4\", \"value\": 10}, {\"name\": \"1-5\", \"value\": 25}, {\"name\": \"1-8\", \"value\": 15}, {\"name\": \"2-6\", \"value\": 25}, {\"name\": \"2-10\", \"value\": 20}, {\"name\": \"2-7\", \"value\": 20}, {\"name\": \"2-8\", \"value\": 15}, {\"name\": \"3-1\", \"value\": 5}, {\"name\": \"3-6\", \"value\": 15}, {\"name\": \"3-9\", \"value\": 20}, {\"name\": \"4-1\", \"value\": 10}, {\"name\": \"4-5\", \"value\": 15}, {\"name\": \"4-6\", \"value\": 25}, {\"name\": \"4-10\", \"value\": 25}, {\"name\": \"4-8\", \"value\": 15}, {\"name\": \"4-9\", \"value\": 25}, {\"name\": \"5-1\", \"value\": 25}, {\"name\": \"5-4\", \"value\": 15}, {\"name\": \"5-8\", \"value\": 20}, {\"name\": \"5-9\", \"value\": 10}, {\"name\": \"6-2\", \"value\": 25}, {\"name\": \"6-3\", \"value\": 15}, {\"name\": \"6-4\", \"value\": 25}, {\"name\": \"6-10\", \"value\": 20}, {\"name\": \"6-7\", \"value\": 20}, {\"name\": \"10-2\", \"value\": 20}, {\"name\": \"10-4\", \"value\": 25}, {\"name\": \"10-6\", \"value\": 20}, {\"name\": \"10-8\", \"value\": 20}, {\"name\": \"7-2\", \"value\": 20}, {\"name\": \"7-6\", \"value\": 20}, {\"name\": \"7-8\", \"value\": 10}, {\"name\": \"7-9\", \"value\": 15}, {\"name\": \"8-1\", \"value\": 15}, {\"name\": \"8-2\", \"value\": 15}, {\"name\": \"8-4\", \"value\": 15}, {\"name\": \"8-5\", \"value\": 20}, {\"name\": \"8-10\", \"value\": 20}, {\"name\": \"8-7\", \"value\": 10}, {\"name\": \"9-3\", \"value\": 20}, {\"name\": \"9-4\", \"value\": 25}, {\"name\": \"9-5\", \"value\": 10}, {\"name\": \"9-7\", \"value\": 15}]}",
                "node_json": "{\"nodes\": [{\"name\": \"1\", \"value\": 10}, {\"name\": \"2\", \"value\": 10}, {\"name\": \"3\", \"value\": 50}, {\"name\": \"4\", \"value\": 50}, {\"name\": \"5\", \"value\": 30}, {\"name\": \"6\", \"value\": 40}, {\"name\": \"7\", \"value\": 20}, {\"name\": \"8\", \"value\": 10}, {\"name\": \"9\", \"value\": 40}, {\"name\": \"10\", \"value\": 0}]}",
                "n_customer": "7",
                "time_max": "145",
                "load_max": "255"
              },
              {
                "block_level": "3",
                "customer_01": "Girep [40]",
                "customer_02": "Isosil [50]",
                "customer_03": "Keviw [50]",
                "customer_04": "Kopil [40]",
                "customer_05": "NEH [30]",
                "customer_06": "Neneg [30]",
                "customer_07": "Oseh [40]",
                "customer_08": "Riseda [50]",
                "customer_09": "Semeg [40]",
                "customer_10": "MyMenu",
                "image": "rand_graph_0008.gv.png",
                "json": "{\"edges\": [{\"name\": \"1-10\", \"value\": 25}, {\"name\": \"1-6\", \"value\": 15}, {\"name\": \"1-7\", \"value\": 25}, {\"name\": \"1-8\", \"value\": 20}, {\"name\": \"1-9\", \"value\": 20}, {\"name\": \"2-4\", \"value\": 20}, {\"name\": \"2-5\", \"value\": 10}, {\"name\": \"2-7\", \"value\": 25}, {\"name\": \"2-9\", \"value\": 10}, {\"name\": \"3-4\", \"value\": 25}, {\"name\": \"3-7\", \"value\": 25}, {\"name\": \"3-8\", \"value\": 20}, {\"name\": \"4-2\", \"value\": 20}, {\"name\": \"4-3\", \"value\": 25}, {\"name\": \"4-10\", \"value\": 15}, {\"name\": \"4-5\", \"value\": 5}, {\"name\": \"4-6\", \"value\": 25}, {\"name\": \"4-7\", \"value\": 5}, {\"name\": \"4-9\", \"value\": 25}, {\"name\": \"10-1\", \"value\": 25}, {\"name\": \"10-4\", \"value\": 15}, {\"name\": \"10-5\", \"value\": 20}, {\"name\": \"10-6\", \"value\": 15}, {\"name\": \"5-2\", \"value\": 10}, {\"name\": \"5-4\", \"value\": 5}, {\"name\": \"5-10\", \"value\": 20}, {\"name\": \"5-6\", \"value\": 15}, {\"name\": \"6-1\", \"value\": 15}, {\"name\": \"6-4\", \"value\": 25}, {\"name\": \"6-10\", \"value\": 15}, {\"name\": \"6-5\", \"value\": 15}, {\"name\": \"6-9\", \"value\": 10}, {\"name\": \"7-1\", \"value\": 25}, {\"name\": \"7-2\", \"value\": 25}, {\"name\": \"7-3\", \"value\": 25}, {\"name\": \"7-4\", \"value\": 5}, {\"name\": \"7-8\", \"value\": 10}, {\"name\": \"8-1\", \"value\": 20}, {\"name\": \"8-3\", \"value\": 20}, {\"name\": \"8-7\", \"value\": 10}, {\"name\": \"9-1\", \"value\": 20}, {\"name\": \"9-2\", \"value\": 10}, {\"name\": \"9-4\", \"value\": 25}, {\"name\": \"9-6\", \"value\": 10}]}",
                "node_json": "{\"nodes\": [{\"name\": \"1\", \"value\": 40}, {\"name\": \"2\", \"value\": 50}, {\"name\": \"3\", \"value\": 50}, {\"name\": \"4\", \"value\": 40}, {\"name\": \"5\", \"value\": 30}, {\"name\": \"6\", \"value\": 30}, {\"name\": \"7\", \"value\": 40}, {\"name\": \"8\", \"value\": 50}, {\"name\": \"9\", \"value\": 40}, {\"name\": \"10\", \"value\": 0}]}",
                "n_customer": "5",
                "time_max": "130",
                "load_max": "245"
              },
              {
                "block_level": "3",
                "customer_01": "Ateneh [30]",
                "customer_02": "Ibeneg [20]",
                "customer_03": "Icitil [30]",
                "customer_04": "Isosil [20]",
                "customer_05": "Kuti [50]",
                "customer_06": "MIZ [40]",
                "customer_07": "Odami [20]",
                "customer_08": "Unale [50]",
                "customer_09": "VOH [40]",
                "customer_10": "MyMenu",
                "image": "rand_graph_0011.gv.png",
                "json": "{\"edges\": [{\"name\": \"1-5\", \"value\": 10}, {\"name\": \"1-6\", \"value\": 25}, {\"name\": \"1-10\", \"value\": 15}, {\"name\": \"1-7\", \"value\": 15}, {\"name\": \"1-8\", \"value\": 20}, {\"name\": \"2-4\", \"value\": 25}, {\"name\": \"2-5\", \"value\": 5}, {\"name\": \"2-10\", \"value\": 25}, {\"name\": \"2-7\", \"value\": 25}, {\"name\": \"2-9\", \"value\": 15}, {\"name\": \"3-5\", \"value\": 10}, {\"name\": \"3-8\", \"value\": 20}, {\"name\": \"3-9\", \"value\": 20}, {\"name\": \"4-2\", \"value\": 25}, {\"name\": \"4-5\", \"value\": 10}, {\"name\": \"4-7\", \"value\": 15}, {\"name\": \"4-8\", \"value\": 10}, {\"name\": \"5-1\", \"value\": 10}, {\"name\": \"5-2\", \"value\": 5}, {\"name\": \"5-3\", \"value\": 10}, {\"name\": \"5-4\", \"value\": 10}, {\"name\": \"5-6\", \"value\": 5}, {\"name\": \"6-1\", \"value\": 25}, {\"name\": \"6-5\", \"value\": 5}, {\"name\": \"6-7\", \"value\": 5}, {\"name\": \"10-1\", \"value\": 15}, {\"name\": \"10-2\", \"value\": 25}, {\"name\": \"10-8\", \"value\": 20}, {\"name\": \"10-9\", \"value\": 20}, {\"name\": \"7-1\", \"value\": 15}, {\"name\": \"7-2\", \"value\": 25}, {\"name\": \"7-4\", \"value\": 15}, {\"name\": \"7-6\", \"value\": 5}, {\"name\": \"7-8\", \"value\": 15}, {\"name\": \"7-9\", \"value\": 10}, {\"name\": \"8-1\", \"value\": 20}, {\"name\": \"8-3\", \"value\": 20}, {\"name\": \"8-4\", \"value\": 10}, {\"name\": \"8-10\", \"value\": 20}, {\"name\": \"8-7\", \"value\": 15}, {\"name\": \"9-2\", \"value\": 15}, {\"name\": \"9-3\", \"value\": 20}, {\"name\": \"9-10\", \"value\": 20}, {\"name\": \"9-7\", \"value\": 10}]}",
                "node_json": "{\"nodes\": [{\"name\": \"1\", \"value\": 30}, {\"name\": \"2\", \"value\": 20}, {\"name\": \"3\", \"value\": 30}, {\"name\": \"4\", \"value\": 20}, {\"name\": \"5\", \"value\": 50}, {\"name\": \"6\", \"value\": 40}, {\"name\": \"7\", \"value\": 20}, {\"name\": \"8\", \"value\": 50}, {\"name\": \"9\", \"value\": 40}, {\"name\": \"10\", \"value\": 0}]}",
                "n_customer": "6",
                "time_max": "150",
                "load_max": "255"
              },
              {
                "block_level": "4",
                "customer_01": "Egab [50]",
                "customer_02": "Egemir [40]",
                "customer_03": "Emelal [10]",
                "customer_04": "Hirel [40]",
                "customer_05": "Icefe [50]",
                "customer_06": "Lerit [20]",
                "customer_07": "MEM [50]",
                "customer_08": "Negibe [20]",
                "customer_09": "Nusona [10]",
                "customer_10": "MyMenu",
                "image": "rand_graph_0012.gv.png",
                "json": "{\"edges\": [{\"name\": \"1-2\", \"value\": 10}, {\"name\": \"1-4\", \"value\": 15}, {\"name\": \"1-10\", \"value\": 5}, {\"name\": \"2-1\", \"value\": 10}, {\"name\": \"2-3\", \"value\": 20}, {\"name\": \"2-4\", \"value\": 10}, {\"name\": \"2-6\", \"value\": 20}, {\"name\": \"2-7\", \"value\": 25}, {\"name\": \"2-10\", \"value\": 25}, {\"name\": \"3-2\", \"value\": 20}, {\"name\": \"3-7\", \"value\": 15}, {\"name\": \"3-9\", \"value\": 20}, {\"name\": \"4-1\", \"value\": 15}, {\"name\": \"4-2\", \"value\": 10}, {\"name\": \"4-5\", \"value\": 20}, {\"name\": \"4-6\", \"value\": 10}, {\"name\": \"4-7\", \"value\": 5}, {\"name\": \"4-10\", \"value\": 10}, {\"name\": \"4-8\", \"value\": 10}, {\"name\": \"4-9\", \"value\": 5}, {\"name\": \"5-4\", \"value\": 20}, {\"name\": \"5-7\", \"value\": 20}, {\"name\": \"5-8\", \"value\": 15}, {\"name\": \"6-2\", \"value\": 20}, {\"name\": \"6-4\", \"value\": 10}, {\"name\": \"6-8\", \"value\": 10}, {\"name\": \"6-9\", \"value\": 25}, {\"name\": \"7-2\", \"value\": 25}, {\"name\": \"7-3\", \"value\": 15}, {\"name\": \"7-4\", \"value\": 5}, {\"name\": \"7-5\", \"value\": 20}, {\"name\": \"7-10\", \"value\": 10}, {\"name\": \"10-1\", \"value\": 5}, {\"name\": \"10-2\", \"value\": 25}, {\"name\": \"10-4\", \"value\": 10}, {\"name\": \"10-7\", \"value\": 10}, {\"name\": \"8-4\", \"value\": 10}, {\"name\": \"8-5\", \"value\": 15}, {\"name\": \"8-6\", \"value\": 10}, {\"name\": \"8-9\", \"value\": 25}, {\"name\": \"9-3\", \"value\": 20}, {\"name\": \"9-4\", \"value\": 5}, {\"name\": \"9-6\", \"value\": 25}, {\"name\": \"9-8\", \"value\": 25}]}",
                "node_json": "{\"nodes\": [{\"name\": \"1\", \"value\": 50}, {\"name\": \"2\", \"value\": 40}, {\"name\": \"3\", \"value\": 10}, {\"name\": \"4\", \"value\": 40}, {\"name\": \"5\", \"value\": 50}, {\"name\": \"6\", \"value\": 20}, {\"name\": \"7\", \"value\": 50}, {\"name\": \"8\", \"value\": 20}, {\"name\": \"9\", \"value\": 10}, {\"name\": \"10\", \"value\": 0}]}",
                "n_customer": "5",
                "time_max": "150",
                "load_max": "245"
              },
              {
                "block_level": "4",
                "customer_01": "ECE [40]",
                "customer_02": "EMA [10]",
                "customer_03": "Etezen [50]",
                "customer_04": "Hisere [50]",
                "customer_05": "Lerit [40]",
                "customer_06": "Luhere [30]",
                "customer_07": "MIZ [10]",
                "customer_08": "Teba [30]",
                "customer_09": "Tihe [50]",
                "customer_10": "MyMenu",
                "image": "rand_graph_0014.gv.png",
                "json": "{\"edges\": [{\"name\": \"1-2\", \"value\": 20}, {\"name\": \"1-7\", \"value\": 25}, {\"name\": \"1-9\", \"value\": 10}, {\"name\": \"2-1\", \"value\": 20}, {\"name\": \"2-4\", \"value\": 5}, {\"name\": \"2-10\", \"value\": 10}, {\"name\": \"2-8\", \"value\": 20}, {\"name\": \"2-9\", \"value\": 25}, {\"name\": \"3-5\", \"value\": 15}, {\"name\": \"3-6\", \"value\": 20}, {\"name\": \"3-10\", \"value\": 10}, {\"name\": \"3-9\", \"value\": 20}, {\"name\": \"4-2\", \"value\": 5}, {\"name\": \"4-5\", \"value\": 20}, {\"name\": \"4-6\", \"value\": 20}, {\"name\": \"4-7\", \"value\": 5}, {\"name\": \"5-3\", \"value\": 15}, {\"name\": \"5-4\", \"value\": 20}, {\"name\": \"5-7\", \"value\": 5}, {\"name\": \"5-10\", \"value\": 15}, {\"name\": \"5-8\", \"value\": 25}, {\"name\": \"5-9\", \"value\": 5}, {\"name\": \"6-3\", \"value\": 20}, {\"name\": \"6-4\", \"value\": 20}, {\"name\": \"7-1\", \"value\": 25}, {\"name\": \"7-4\", \"value\": 5}, {\"name\": \"7-5\", \"value\": 5}, {\"name\": \"7-8\", \"value\": 10}, {\"name\": \"7-9\", \"value\": 10}, {\"name\": \"10-2\", \"value\": 10}, {\"name\": \"10-3\", \"value\": 10}, {\"name\": \"10-5\", \"value\": 15}, {\"name\": \"10-9\", \"value\": 20}, {\"name\": \"8-2\", \"value\": 20}, {\"name\": \"8-5\", \"value\": 25}, {\"name\": \"8-7\", \"value\": 10}, {\"name\": \"8-9\", \"value\": 5}, {\"name\": \"9-1\", \"value\": 10}, {\"name\": \"9-2\", \"value\": 25}, {\"name\": \"9-3\", \"value\": 20}, {\"name\": \"9-5\", \"value\": 5}, {\"name\": \"9-7\", \"value\": 10}, {\"name\": \"9-10\", \"value\": 20}, {\"name\": \"9-8\", \"value\": 5}]}",
                "node_json": "{\"nodes\": [{\"name\": \"1\", \"value\": 40}, {\"name\": \"2\", \"value\": 10}, {\"name\": \"3\", \"value\": 50}, {\"name\": \"4\", \"value\": 50}, {\"name\": \"5\", \"value\": 40}, {\"name\": \"6\", \"value\": 30}, {\"name\": \"7\", \"value\": 10}, {\"name\": \"8\", \"value\": 30}, {\"name\": \"9\", \"value\": 50}, {\"name\": \"10\", \"value\": 0}]}",
                "n_customer": "6",
                "time_max": "115",
                "load_max": "245"
              },
              {
                "block_level": "4",
                "customer_01": "Anitu [10]",
                "customer_02": "BAC [10]",
                "customer_03": "Esedes [50]",
                "customer_04": "Etin [40]",
                "customer_05": "Neneg [30]",
                "customer_06": "Pedeze [50]",
                "customer_07": "Tikore [20]",
                "customer_08": "Tirevu [30]",
                "customer_09": "Tomud [20]",
                "customer_10": "MyMenu",
                "image": "rand_graph_0015.gv.png",
                "json": "{\"edges\": [{\"name\": \"1-2\", \"value\": 20}, {\"name\": \"1-4\", \"value\": 25}, {\"name\": \"1-6\", \"value\": 25}, {\"name\": \"1-8\", \"value\": 25}, {\"name\": \"1-9\", \"value\": 10}, {\"name\": \"2-1\", \"value\": 20}, {\"name\": \"2-4\", \"value\": 15}, {\"name\": \"2-10\", \"value\": 15}, {\"name\": \"2-6\", \"value\": 10}, {\"name\": \"2-8\", \"value\": 5}, {\"name\": \"3-10\", \"value\": 15}, {\"name\": \"3-6\", \"value\": 25}, {\"name\": \"3-7\", \"value\": 10}, {\"name\": \"3-9\", \"value\": 5}, {\"name\": \"4-1\", \"value\": 25}, {\"name\": \"4-2\", \"value\": 15}, {\"name\": \"4-10\", \"value\": 10}, {\"name\": \"4-6\", \"value\": 25}, {\"name\": \"4-8\", \"value\": 20}, {\"name\": \"10-2\", \"value\": 15}, {\"name\": \"10-3\", \"value\": 15}, {\"name\": \"10-4\", \"value\": 10}, {\"name\": \"10-5\", \"value\": 20}, {\"name\": \"5-10\", \"value\": 20}, {\"name\": \"5-9\", \"value\": 25}, {\"name\": \"6-1\", \"value\": 25}, {\"name\": \"6-2\", \"value\": 10}, {\"name\": \"6-3\", \"value\": 25}, {\"name\": \"6-4\", \"value\": 25}, {\"name\": \"6-7\", \"value\": 25}, {\"name\": \"6-9\", \"value\": 5}, {\"name\": \"7-3\", \"value\": 10}, {\"name\": \"7-6\", \"value\": 25}, {\"name\": \"7-9\", \"value\": 10}, {\"name\": \"8-1\", \"value\": 25}, {\"name\": \"8-2\", \"value\": 5}, {\"name\": \"8-4\", \"value\": 20}, {\"name\": \"8-9\", \"value\": 10}, {\"name\": \"9-1\", \"value\": 10}, {\"name\": \"9-3\", \"value\": 5}, {\"name\": \"9-5\", \"value\": 25}, {\"name\": \"9-6\", \"value\": 5}, {\"name\": \"9-7\", \"value\": 10}, {\"name\": \"9-8\", \"value\": 10}]}",
                "node_json": "{\"nodes\": [{\"name\": \"1\", \"value\": 10}, {\"name\": \"2\", \"value\": 10}, {\"name\": \"3\", \"value\": 50}, {\"name\": \"4\", \"value\": 40}, {\"name\": \"5\", \"value\": 30}, {\"name\": \"6\", \"value\": 50}, {\"name\": \"7\", \"value\": 20}, {\"name\": \"8\", \"value\": 30}, {\"name\": \"9\", \"value\": 20}, {\"name\": \"10\", \"value\": 0}]}",
                "n_customer": "7",
                "time_max": "140",
                "load_max": "235"
              },
              {
                "block_level": "5",
                "customer_01": "EDI [50]",
                "customer_02": "EFI [10]",
                "customer_03": "IWE [50]",
                "customer_04": "OBA [50]",
                "customer_05": "Selara [20]",
                "customer_06": "Tihe [30]",
                "customer_07": "Ugal [10]",
                "customer_08": "Usel [50]",
                "customer_09": "Vemin [30]",
                "customer_10": "MyMenu",
                "image": "rand_graph_0020.gv.png",
                "json": "{\"edges\": [{\"name\": \"1-6\", \"value\": 25}, {\"name\": \"1-8\", \"value\": 25}, {\"name\": \"1-9\", \"value\": 20}, {\"name\": \"2-4\", \"value\": 20}, {\"name\": \"2-6\", \"value\": 5}, {\"name\": \"2-8\", \"value\": 15}, {\"name\": \"2-9\", \"value\": 25}, {\"name\": \"3-10\", \"value\": 25}, {\"name\": \"3-5\", \"value\": 10}, {\"name\": \"3-6\", \"value\": 20}, {\"name\": \"3-8\", \"value\": 10}, {\"name\": \"3-9\", \"value\": 20}, {\"name\": \"10-3\", \"value\": 25}, {\"name\": \"10-4\", \"value\": 5}, {\"name\": \"10-6\", \"value\": 5}, {\"name\": \"10-9\", \"value\": 5}, {\"name\": \"4-2\", \"value\": 20}, {\"name\": \"4-10\", \"value\": 5}, {\"name\": \"4-5\", \"value\": 25}, {\"name\": \"4-6\", \"value\": 10}, {\"name\": \"4-7\", \"value\": 25}, {\"name\": \"4-9\", \"value\": 5}, {\"name\": \"5-3\", \"value\": 10}, {\"name\": \"5-4\", \"value\": 25}, {\"name\": \"5-7\", \"value\": 20}, {\"name\": \"6-1\", \"value\": 25}, {\"name\": \"6-2\", \"value\": 5}, {\"name\": \"6-3\", \"value\": 20}, {\"name\": \"6-10\", \"value\": 5}, {\"name\": \"6-4\", \"value\": 10}, {\"name\": \"6-8\", \"value\": 20}, {\"name\": \"7-4\", \"value\": 25}, {\"name\": \"7-5\", \"value\": 20}, {\"name\": \"7-8\", \"value\": 15}, {\"name\": \"8-1\", \"value\": 25}, {\"name\": \"8-2\", \"value\": 15}, {\"name\": \"8-3\", \"value\": 10}, {\"name\": \"8-6\", \"value\": 20}, {\"name\": \"8-7\", \"value\": 15}, {\"name\": \"9-1\", \"value\": 20}, {\"name\": \"9-2\", \"value\": 25}, {\"name\": \"9-3\", \"value\": 20}, {\"name\": \"9-10\", \"value\": 5}, {\"name\": \"9-4\", \"value\": 5}]}",
                "node_json": "{\"nodes\": [{\"name\": \"1\", \"value\": 50}, {\"name\": \"2\", \"value\": 10}, {\"name\": \"3\", \"value\": 50}, {\"name\": \"4\", \"value\": 50}, {\"name\": \"5\", \"value\": 20}, {\"name\": \"6\", \"value\": 30}, {\"name\": \"7\", \"value\": 10}, {\"name\": \"8\", \"value\": 50}, {\"name\": \"9\", \"value\": 30}, {\"name\": \"10\", \"value\": 0}]}",
                "n_customer": "5",
                "time_max": "120",
                "load_max": "255"
              },
              {
                "block_level": "5",
                "customer_01": "BUL [30]",
                "customer_02": "Dami [50]",
                "customer_03": "FES [10]",
                "customer_04": "Irine [10]",
                "customer_05": "Luhab [20]",
                "customer_06": "Niten [40]",
                "customer_07": "TEJ [30]",
                "customer_08": "Tilin [10]",
                "customer_09": "Zirot [50]",
                "customer_10": "MyMenu",
                "image": "rand_graph_0024.gv.png",
                "json": "{\"edges\": [{\"name\": \"1-2\", \"value\": 10}, {\"name\": \"1-5\", \"value\": 15}, {\"name\": \"1-10\", \"value\": 10}, {\"name\": \"1-6\", \"value\": 5}, {\"name\": \"1-7\", \"value\": 20}, {\"name\": \"1-8\", \"value\": 25}, {\"name\": \"1-9\", \"value\": 20}, {\"name\": \"2-1\", \"value\": 10}, {\"name\": \"2-3\", \"value\": 5}, {\"name\": \"2-9\", \"value\": 10}, {\"name\": \"3-2\", \"value\": 5}, {\"name\": \"3-4\", \"value\": 25}, {\"name\": \"3-6\", \"value\": 15}, {\"name\": \"3-7\", \"value\": 25}, {\"name\": \"3-8\", \"value\": 25}, {\"name\": \"4-3\", \"value\": 25}, {\"name\": \"4-5\", \"value\": 5}, {\"name\": \"4-10\", \"value\": 5}, {\"name\": \"4-8\", \"value\": 10}, {\"name\": \"5-1\", \"value\": 15}, {\"name\": \"5-4\", \"value\": 5}, {\"name\": \"5-10\", \"value\": 25}, {\"name\": \"10-1\", \"value\": 10}, {\"name\": \"10-4\", \"value\": 5}, {\"name\": \"10-5\", \"value\": 25}, {\"name\": \"10-9\", \"value\": 5}, {\"name\": \"6-1\", \"value\": 5}, {\"name\": \"6-3\", \"value\": 15}, {\"name\": \"6-8\", \"value\": 25}, {\"name\": \"6-9\", \"value\": 10}, {\"name\": \"7-1\", \"value\": 20}, {\"name\": \"7-3\", \"value\": 25}, {\"name\": \"7-8\", \"value\": 15}, {\"name\": \"8-1\", \"value\": 25}, {\"name\": \"8-3\", \"value\": 25}, {\"name\": \"8-4\", \"value\": 10}, {\"name\": \"8-6\", \"value\": 25}, {\"name\": \"8-7\", \"value\": 15}, {\"name\": \"8-9\", \"value\": 20}, {\"name\": \"9-1\", \"value\": 20}, {\"name\": \"9-2\", \"value\": 10}, {\"name\": \"9-10\", \"value\": 5}, {\"name\": \"9-6\", \"value\": 10}, {\"name\": \"9-8\", \"value\": 20}]}",
                "node_json": "{\"nodes\": [{\"name\": \"1\", \"value\": 30}, {\"name\": \"2\", \"value\": 50}, {\"name\": \"3\", \"value\": 10}, {\"name\": \"4\", \"value\": 10}, {\"name\": \"5\", \"value\": 20}, {\"name\": \"6\", \"value\": 40}, {\"name\": \"7\", \"value\": 30}, {\"name\": \"8\", \"value\": 10}, {\"name\": \"9\", \"value\": 50}, {\"name\": \"10\", \"value\": 0}]}",
                "n_customer": "8",
                "time_max": "140",
                "load_max": "255"
              },
              {
                "block_level": "5",
                "customer_01": "Alume [10]",
                "customer_02": "Benuz [30]",
                "customer_03": "Izigan [50]",
                "customer_04": "Keviw [10]",
                "customer_05": "Leti [10]",
                "customer_06": "Meduf [10]",
                "customer_07": "Nisun [40]",
                "customer_08": "TEL [30]",
                "customer_09": "Usel [50]",
                "customer_10": "MyMenu",
                "image": "rand_graph_0025.gv.png",
                "json": "{\"edges\": [{\"name\": \"1-3\", \"value\": 5}, {\"name\": \"1-7\", \"value\": 10}, {\"name\": \"1-9\", \"value\": 20}, {\"name\": \"2-3\", \"value\": 20}, {\"name\": \"2-6\", \"value\": 25}, {\"name\": \"2-8\", \"value\": 25}, {\"name\": \"2-9\", \"value\": 10}, {\"name\": \"3-1\", \"value\": 5}, {\"name\": \"3-2\", \"value\": 20}, {\"name\": \"3-5\", \"value\": 25}, {\"name\": \"3-8\", \"value\": 15}, {\"name\": \"3-9\", \"value\": 15}, {\"name\": \"4-10\", \"value\": 5}, {\"name\": \"4-7\", \"value\": 25}, {\"name\": \"4-8\", \"value\": 15}, {\"name\": \"4-9\", \"value\": 5}, {\"name\": \"5-3\", \"value\": 25}, {\"name\": \"5-6\", \"value\": 15}, {\"name\": \"5-10\", \"value\": 15}, {\"name\": \"5-7\", \"value\": 20}, {\"name\": \"5-9\", \"value\": 5}, {\"name\": \"6-2\", \"value\": 25}, {\"name\": \"6-5\", \"value\": 15}, {\"name\": \"6-9\", \"value\": 10}, {\"name\": \"10-4\", \"value\": 5}, {\"name\": \"10-5\", \"value\": 15}, {\"name\": \"10-8\", \"value\": 20}, {\"name\": \"10-9\", \"value\": 15}, {\"name\": \"7-1\", \"value\": 10}, {\"name\": \"7-4\", \"value\": 25}, {\"name\": \"7-5\", \"value\": 20}, {\"name\": \"8-2\", \"value\": 25}, {\"name\": \"8-3\", \"value\": 15}, {\"name\": \"8-4\", \"value\": 15}, {\"name\": \"8-10\", \"value\": 20}, {\"name\": \"8-9\", \"value\": 10}, {\"name\": \"9-1\", \"value\": 20}, {\"name\": \"9-2\", \"value\": 10}, {\"name\": \"9-3\", \"value\": 15}, {\"name\": \"9-4\", \"value\": 5}, {\"name\": \"9-5\", \"value\": 5}, {\"name\": \"9-6\", \"value\": 10}, {\"name\": \"9-10\", \"value\": 15}, {\"name\": \"9-8\", \"value\": 10}]}",
                "node_json": "{\"nodes\": [{\"name\": \"1\", \"value\": 10}, {\"name\": \"2\", \"value\": 30}, {\"name\": \"3\", \"value\": 50}, {\"name\": \"4\", \"value\": 10}, {\"name\": \"5\", \"value\": 10}, {\"name\": \"6\", \"value\": 10}, {\"name\": \"7\", \"value\": 40}, {\"name\": \"8\", \"value\": 30}, {\"name\": \"9\", \"value\": 50}, {\"name\": \"10\", \"value\": 0}]}",
                "n_customer": "7",
                "time_max": "150",
                "load_max": "204"
              }
            ],
            "sample": {
              "mode": "sequential",
              "n": ""
            },
            "files": {},
            "responses": {},
            "parameters": {},
            "messageHandlers": {
              "before:prepare": function anonymous(
) {

study.numRoutes = 0;
study.numPotRoutes = 0;
study.numSatCustomers = 0;
study.numPotCustomers = 0;


this.options.templateParameters = this.options.templateParameters.filter(row => row.block_level === this.parameters.block)
}
            },
            "title": "Experiment_Loop",
            "shuffleGroups": [],
            "template": {
              "type": "lab.flow.Sequence",
              "files": {},
              "responses": {},
              "parameters": {},
              "messageHandlers": {
                "run": function anonymous(
) {
study.pullAlert = parseInt(Math.round(this.parameters.n_customer/2)+
    (getRndInteger(-2, 1)+1));
console.log(study.pullAlert);
}
              },
              "title": "Experiment_Sequence",
              "content": [
                {
                  "type": "lab.html.Screen",
                  "files": {},
                  "responses": {
                    "click button#continue": "continue",
                    "click button#end": "end"
                  },
                  "parameters": {},
                  "messageHandlers": {
                    "before:prepare": function anonymous(
) {
this.options.skip = (study.configuration.declarative === "easy");

},
                    "run": function anonymous(
) {
study.numPotRoutes = ++ study.numPotRoutes;

let blockCounter = document.getElementById("block-count");
blockCounter.innerHTML = study.numBlock;
let trialCounter = document.getElementById("trial-count");
trialCounter.innerHTML = study.numPotRoutes;

// to exit loop
if (this.state.response === 'end'){
  this.parent.parent.parent.parent.end()
};
}
                  },
                  "title": "Screen_Constraints_before_Trial",
                  "content": "\u003Cmain class=\"experiment\"\u003E\r\n  \u003Cdiv class=wrapper\u003E\r\n\r\n    \u003Cdiv class=\"exit\"\u003E\r\n      \u003Cbutton id=\"end\" class=\"end left\" type=\"submit\"\u003E\r\n        Abbrechen\r\n      \u003C\u002Fbutton\u003E\r\n    \u003C\u002Fdiv\u003E\r\n\r\n  \u003Cdiv class=\"trial\"\u003E\r\n      \u003Ch4 class=\"trial\"\u003E\r\n        \u003Cspan id=\"block-count\"\u003E\u003C\u002Fspan\u003E. Block | \r\n        \u003Cspan id=\"trial-count\"\u003E\u003C\u002Fspan\u003E. Route\r\n      \u003C\u002Fh4\u003E\r\n  \u003C\u002Fdiv\u003E\r\n\r\n     \u003Cdiv class=\"constraints\"\u003E\r\n      \u003Ch4\u003EVorgaben\u003C\u002Fh4\u003E\r\n       \u003Ctable class=\"tab-constraints\"\u003E\r\n          \u003Ctr\u003E\r\n            \u003Ctd\u003EZiel:\u003C\u002Ftd\u003E\r\n            \u003Ctd\u003E${this.parameters.n_customer} \u003C\u002Ftd\u003E\r\n            \u003Ctd\u003E Kunden\u003C\u002Ftd\u003E\r\n          \u003C\u002Ftr\u003E\r\n          \u003Ctr\u003E\r\n            \u003Ctd\u003EVerfügbare Züge:\u003C\u002Ftd\u003E\r\n            \u003Ctd\u003E${parseInt(this.parameters.n_customer) + 5}\u003C\u002Ftd\u003E\r\n            \u003Ctd\u003E \u003C\u002Ftd\u003E\r\n          \u003C\u002Ftr\u003E\r\n          \u003Ctr\u003E\r\n            \u003Ctd\u003EVerfügbare Zeit:\u003C\u002Ftd\u003E\r\n            \u003Ctd\u003E${this.parameters.time_max}\u003C\u002Ftd\u003E\r\n            \u003Ctd\u003EMinuten\u003C\u002Ftd\u003E\r\n          \u003C\u002Ftr\u003E\r\n          \u003Ctr\u003E\r\n            \u003Ctd\u003EVerfügbare Menge:\u003C\u002Ftd\u003E\r\n            \u003Ctd\u003E${this.parameters.load_max}\u003C\u002Ftd\u003E\r\n            \u003Ctd\u003EPortionen\u003C\u002Ftd\u003E\r\n          \u003C\u002Ftr\u003E\r\n        \u003C\u002Ftable\u003E\r\n     \u003C\u002Fdiv\u003E\r\n\r\n    \u003Cdiv class=\"feedback\"\u003E\r\n      \u003Ch4\u003E\u003C\u002Fh4\u003E\r\n     \u003Cp id=\"feedback\" class=\"alert\"\u003E Merken Sie sich unbedingt diese Vorgaben! Um die folgende Aufgabe bearbeiten zu können, müssen Sie sich an die Werte erinnern. Sie werden nicht noch einmal angezeigt.\u003C\u002Fp\u003E\r\n    \u003C\u002Fdiv\u003E\r\n\r\n    \u003Cdiv class=\"forward\"\u003E\r\n      \u003Cbutton id=\"continue\" class=\"right\"\u003E \r\n        Weiter\r\n      \u003C\u002Fbutton\u003E   \r\n    \u003C\u002Fdiv\u003E\r\n    \r\n  \u003C\u002Fdiv\u003E\r\n\u003C\u002Fmain\u003E",
                  "tardy": true
                },
                {
                  "type": "lab.html.Screen",
                  "files": {
                    "rand_graph_0000.gv.png": "embedded\u002Fdbcf04b8e3309ce13f87a97d7a1dc20090b74499250e8775e7a3c1288cc9c1bc.png",
                    "rand_graph_0001.gv.png": "embedded\u002Fbf8e8a9b3b00590d3fa143135ed39abfa7730085d6de83709ff4dda2cdfad91b.png",
                    "rand_graph_0003.gv.png": "embedded\u002Fb4c0aa3034e7bc48cf40771839f691bd2da8ba3b67ec1779a05828e7c1c2bad8.png",
                    "rand_graph_0004.gv.png": "embedded\u002Fee3db615dac92a794bbe875e99dee27e2e0cdd3072bdf8f7c5728a45ac52ffec.png",
                    "rand_graph_0005.gv.png": "embedded\u002F593c495be8ef5adcfcf02a6ba19133620d68582c5b14539348bee1cc060f87e3.png",
                    "rand_graph_0006.gv.png": "embedded\u002F0f7500a30ec515674f4208de6bc93849f1beb334bf72fb296dbc98d13bdec358.png",
                    "rand_graph_0007.gv.png": "embedded\u002Fbebdf5317abe85c956fb5ba8cfaba7590f658ae9c7498e9cc0e9c57d5beb4cc9.png",
                    "rand_graph_7_0000.gv.png": "embedded\u002F8fe593bc06eba233e01f250b251204a31a8a30a60a7cddab9f51a627ff803e4b.png",
                    "rand_graph_7_0001.gv.png": "embedded\u002Fa4925f5e114f68289b17d6ca1c5acb7b7d3cd9b862eefc3f4eef7dfec6e3df52.png",
                    "rand_graph_0008.gv.png": "embedded\u002F9bd010485a15f39389fd6c1a73546d1d2eb3806bc4b52e125fba6c0f2ed8cc00.png",
                    "rand_graph_8_0000.gv.png": "embedded\u002F6f079b58104efdd85de276d42696c0bddc8211b0e2cff388643dd242f1818e9a.png",
                    "rand_graph_0011.gv.png": "embedded\u002F47852cd58fe8a66b200817379384a4c42c6c806341529b4e48f958de61695c08.png",
                    "rand_graph_0012.gv.png": "embedded\u002F9f83e0a294dfa17d7dbda828a377d91c66a07875c0754a24460b9fba8e2088a2.png",
                    "rand_graph_0014.gv.png": "embedded\u002F700085881f4f0c1be0c4757a618ccbb5b0b083d8381a3ff026c4c85ee4ca2e4f.png",
                    "rand_graph_0015.gv.png": "embedded\u002F36a5262b5466f738f30205aff3155c441acff39cb2e558a1e9fbaf8ec4799213.png",
                    "rand_graph_0020.gv.png": "embedded\u002Febfb39a80ea46eae9ce7c95b071f6ba1385a564976f73b656f5074a511a60345.png",
                    "rand_graph_0024.gv.png": "embedded\u002F0bdac2c38b5c1afafb5c2dc9a263f6a8da4109e3be416ad998bedc3a3a32ed87.png",
                    "rand_graph_0025.gv.png": "embedded\u002F593b4af410281dd78f5e50c97a6da2a204882efa0e26239b3f7e6c5c5ad468c6.png",
                    "rand_graph_0119.gv.png": "embedded\u002F0167745eb138b063a478b95384967e03fa44fcff510bd1289d2a2fa647c5d759.png"
                  },
                  "responses": {
                    "click button#end": "end",
                    "click button#pause": "pause"
                  },
                  "parameters": {},
                  "messageHandlers": {
                    "run": function anonymous(
) {
//change layout according th selected factor level
if (study.configuration.declarative === "hard") {
		document.querySelector(".constraints").setAttribute('style','visibility:hidden');
	} else {
		document.querySelector(".constraints").removeAttribute('style');
	}

/*btnPause = document.querySelector("#pause")
if (study.configuration.problemState === "easy") {
		btnPause.classList.add("invisible");
};*/

elementList = document.querySelectorAll("ol, img");
if (study.configuration.physical === "hard"){
  elementList.forEach(element => element.classList.add("phys--hard", "blur"));
  } else {
    elementList.forEach(element => element.classList.add("phys--easy"))
    };

study.conditions = undefined;

if (study.configuration.declarative == "easy"){
  study.numPotRoutes = ++ study.numPotRoutes;
}

// make dom elements available for manipulations
let blockCounter = document.getElementById("block-count");
let trialCounter = document.getElementById("trial-count");
let fbNumRoutes = document.getElementById("numRoutes");
let fbPotRoutes = document.getElementById("potRoutes");
let feedback = document.getElementById("feedback");
study.feedback = feedback;

let locGoalLabel = document.getElementById("loc-fb-goal-label");
study.locGoalLabel = locGoalLabel;
let locGoalValue = document.getElementById("loc-fb-goal-value");
study.locGoalValue = locGoalValue;
let locTimeLabel = document.getElementById("loc-fb-time-label");
study.locTimeLabel = locTimeLabel;
let locTimeValue = document.getElementById("loc-fb-time-value");
study.locTimeValue = locTimeValue;
let locPullLabel = document.getElementById("loc-fb-pull-label");
study.locPullLabel = locPullLabel;
let locPullValue = document.getElementById("loc-fb-pull-value");
study.locPullValue = locPullValue;
let locLoadLabel = document.getElementById("loc-fb-load-label");
study.locLoadLabel = locLoadLabel;
let locLoadValue = document.getElementById("loc-fb-load-value");
study.locLoadValue = locLoadValue;
let tabVis = document.getElementById("loc-fb");
study.tabVis = tabVis;


blockCounter.innerHTML = study.numBlock;
trialCounter.innerHTML = study.numPotRoutes;
fbNumRoutes.innerHTML = study.numRoutes;
fbPotRoutes.innerHTML = study.numPotRoutes -1;
 

//read graph edges and nodes and make them available
let json = this.parameters.json;
let data = JSON.parse(json);
let edges = {}
data.edges.forEach(element => {
  edges[element.name]=element.value
});
study.edges = edges; 

let nodeJson = this.parameters.node_json;
let nodeData = JSON.parse(nodeJson);
let nodes = {}
nodeData.nodes.forEach(element => {
  nodes[element.name]=element.value
});
study.nodes = nodes; 

/*get number of customers*/
let numCustomer = Object.keys(study.nodes).length; //.toString();
study.numCustomer = numCustomer;
addMyMenu(numCustomer=study.numCustomer);
/*define variables for feedback*/
const MAXTIME = parseInt(this.parameters.time_max);
study.MAXTIME = MAXTIME;
const MAXLOAD = parseInt(this.parameters.load_max);
study.MAXLOAD = MAXLOAD;
const MINCUSTOMERS = parseInt(this.parameters.n_customer);
study.MINCUSTOMERS = MINCUSTOMERS;
const MAXPULLS = parseInt(this.parameters.n_customer) + 4;
study.parameters.maxpulls = MAXPULLS;
study.MAXPULLS = MAXPULLS;
study.fbMinCustomers = parseInt(study.MINCUSTOMERS);
const IMAGE = this.parameters.image;

// make lists sortable and save sort result and costs

$(function() {
    $('#sortable2').sortable({
        connectWith: '#sortable1',
        items: "li:not(.unsortable)",
        placeholder: "ui-state-highlight",        
        over: function(event, ui) {
          if (study.configuration.problemState === "hard") {
            ui.helper.children($("span.list-item")).addClass(`invisible`)};
            },
        update: provideFeedback(numCustomer=numCustomer, image=IMAGE),
    }).disableSelection();
    $('#sortable1').sortable({
      opacity: 0.6,
      connectWith: '#sortable2',
      placeholder: "ui-state-highlight",
      over: function(event, ui) {
              if (study.configuration.problemState === "hard"){ 
                ui.helper.children($("span.list-item")).removeClass(`invisible`)};
      },  
    }).disableSelection();
});

// to exit loop
if (this.state.response === 'end'){
  this.parent.parent.parent.parent.end()
};

/*if (typeof study.conditions !== 'undefined' && study.conditions.constraints.pull.value == parseInt(Math.round(study.numCustomer)/2))
  {alert("Bitte übernehmen Sie!")}*/

/*if (typeof study.conditions !== 'undefined'){
  if(study.numPotRoutes % 2 == 0 && study.conditions.constraints.pull.value == Math.round(numCustomer)/2)
  {alert("Bitte übernehmen Sie!")};
}*/
},
                    "before:prepare": function anonymous(
) {
  this.options.events['click button#continue'] = e => {
    this.options.datastore.state.response = 'continue'; 
    if (typeof study.conditions !== "undefined" && isSolutionValid(conditions=study.conditions)) {
     // study.numPotCustomers = study.numPotCustomers + study.fbMinCustomers;
      study.numRoutes = ++study.numRoutes;
    //study.numSatCustomers = study.numSatCustomers + study.conditions.result.length-2;
      this.end();
      } else {
        alert("ungültige Route");
        e.preventDefault();
};
};

this.options.events['click button#skip'] = e => {
    this.options.datastore.state.response = 'skip';
    //study.numPotCustomers = study.numPotCustomers + study.fbMinCustomers;
    this.end();};

}
                  },
                  "title": "Screen_routing_Experimental_before Pause",
                  "content": "\u003Cmain class=\"experiment\"\u003E\r\n  \u003Cdiv class=wrapper\u003E\r\n\r\n\r\n  \u003Cdiv class=\"trial\"\u003E\r\n      \u003Ch4 class=\"trial\"\u003E\r\n        \u003Cspan id=\"block-count\"\u003E\u003C\u002Fspan\u003E. Block | \r\n        \u003Cspan id=\"trial-count\"\u003E\u003C\u002Fspan\u003E. Route\r\n      \u003C\u002Fh4\u003E\r\n  \u003C\u002Fdiv\u003E\r\n\r\n    \u003Cdiv class=\"constraints\"\u003E\r\n      \u003Ch4\u003EVorgaben\u003C\u002Fh4\u003E\r\n       \u003Ctable class=\"tab-constraints\"\u003E\r\n          \u003Ctr\u003E\r\n            \u003Ctd\u003EZiel:\u003C\u002Ftd\u003E\r\n            \u003Ctd\u003E${this.parameters.n_customer} \u003C\u002Ftd\u003E\r\n            \u003Ctd\u003E Kunden\u003C\u002Ftd\u003E\r\n          \u003C\u002Ftr\u003E\r\n          \u003Ctr\u003E\r\n            \u003Ctd\u003EVerfügbare Züge:\u003C\u002Ftd\u003E\r\n            \u003Ctd\u003E${parseInt(this.parameters.n_customer) + 5}\u003C\u002Ftd\u003E\r\n            \u003Ctd\u003E \u003C\u002Ftd\u003E\r\n          \u003C\u002Ftr\u003E\r\n          \u003Ctr\u003E\r\n            \u003Ctd\u003EVerfügbare Zeit:\u003C\u002Ftd\u003E\r\n            \u003Ctd\u003E${this.parameters.time_max}\u003C\u002Ftd\u003E\r\n            \u003Ctd\u003EMinuten\u003C\u002Ftd\u003E\r\n          \u003C\u002Ftr\u003E\r\n          \u003Ctr\u003E\r\n            \u003Ctd\u003EVerfügbare Menge:\u003C\u002Ftd\u003E\r\n            \u003Ctd\u003E${this.parameters.load_max}\u003C\u002Ftd\u003E\r\n            \u003Ctd\u003EPortionen\u003C\u002Ftd\u003E\r\n          \u003C\u002Ftr\u003E\r\n        \u003C\u002Ftable\u003E\r\n     \u003C\u002Fdiv\u003E\r\n\r\n    \u003Cdiv class=\"graphdisplay\"\u003E\r\n        \u003Cimg class=\"graph\" src=${this.files[this.parameters.image]}\u003E\r\n    \u003C\u002Fdiv\u003E\r\n  \r\n    \u003Cdiv class=\"skip\"\u003E\r\n      \u003Cbutton id=\"skip\" class=\"left\"\u003E\r\n        Überspringen \u003C\u002Fbutton\u003E\r\n    \u003C\u002Fdiv\u003E\r\n\r\n    \u003Cdiv class=\"list1 left available\"\u003E\r\n        \u003Ch4\u003EVerfügbare Orte\u003C\u002Fh4\u003E\r\n        \u003Col id=\"sortable1\" class=\"connectedSortable\"\u003E \u003C!-- phys--hard\"\u003E--\u003E\r\n          \u003Cli id=\"1\" class=\"ui-state-default easy\"\u003E\u003Cspan class=\"list-item\"\u003E${this.parameters.customer_01}\u003C\u002Fspan\u003E\u003C\u002Fli\u003E\r\n          \u003Cli id=\"2\" class=\"ui-state-default\"\u003E\u003Cspan class=\"list-item\"\u003E${this.parameters.customer_02}\u003C\u002Fspan\u003E\u003C\u002Fli\u003E\r\n          \u003Cli id=\"3\" class=\"ui-state-default\"\u003E\u003Cspan class=\"list-item\"\u003E${this.parameters.customer_03}\u003C\u002Fspan\u003E\u003C\u002Fli\u003E\r\n          \u003Cli id=\"4\" class=\"ui-state-default\"\u003E\u003Cspan class=\"list-item\"\u003E${this.parameters.customer_04}\u003C\u002Fspan\u003E\u003C\u002Fli\u003E\r\n          \u003Cli id=\"5\" class=\"ui-state-default\"\u003E\u003Cspan class=\"list-item\"\u003E${this.parameters.customer_05}\u003C\u002Fspan\u003E\u003C\u002Fli\u003E\r\n          \u003Cli id=\"6\" class=\"ui-state-default\"\u003E\u003Cspan class=\"list-item\"\u003E${this.parameters.customer_06}\u003C\u002Fspan\u003E\u003C\u002Fli\u003E\r\n          \u003Cli id=\"7\" class=\"ui-state-default\"\u003E\u003Cspan class=\"list-item\"\u003E${this.parameters.customer_07}\u003C\u002Fspan\u003E\u003C\u002Fli\u003E\r\n          \u003Cli id=\"8\" class=\"ui-state-default\"\u003E\u003Cspan class=\"list-item\"\u003E${this.parameters.customer_08}\u003C\u002Fspan\u003E\u003C\u002Fli\u003E\r\n          \u003Cli id=\"9\" class=\"ui-state-default\"\u003E\u003Cspan class=\"list-item\"\u003E${this.parameters.customer_09}\u003C\u002Fspan\u003E\u003C\u002Fli\u003E\r\n          \u003Cli id=\"10\" class=\"ui-state-default\"\u003EMyMenu\u003C\u002Fli\u003E\r\n        \u003C\u002Fol\u003E\r\n    \u003C\u002Fdiv\u003E\r\n\r\n    \u003Cdiv class=\"list2 arranged\"\u003E\r\n        \u003Ch4\u003EGeplante Route\u003C\u002Fh4\u003E\r\n        \u003Col id=\"sortable2\" class=\"connectedSortable\" start=\"0\"\u003E\r\n        \u003C\u002Fol\u003E\r\n    \u003C\u002Fdiv\u003E\r\n\r\n    \u003Cdiv class=\"exit\"\u003E\r\n      \u003Cbutton id=\"end\" class=\"end left\" type=\"submit\"\u003E\r\n        Abbrechen\r\n      \u003C\u002Fbutton\u003E\r\n    \u003C\u002Fdiv\u003E\r\n\r\n  \u003Cdiv class=\"feedback\"\u003E\r\n      \u003Ch4\u003EStand dieser Route\u003C\u002Fh4\u003E\r\n\r\n       \u003Cp id=\"feedback\"\u003E\u003C\u002Fp\u003E\r\n       \r\n       \u003Ctable id=\"loc-fb\" class=\"tab-local-feedback invisible\"\u003E\r\n          \u003Ctr\u003E\r\n            \u003Ctd id=\"loc-fb-goal-label\"\u003EBenötigte Kunden:\u003C\u002Ftd\u003E\r\n            \u003Ctd id=\"loc-fb-goal-value\"\u003E${this.parameters.n_customer} \u003C\u002Ftd\u003E\r\n            \u003Ctd\u003E\u003C\u002Ftd\u003E\r\n          \u003C\u002Ftr\u003E\r\n          \u003Ctr\u003E\r\n            \u003Ctd id=\"loc-fb-pull-label\"\u003EVerfügbare Züge:\u003C\u002Ftd\u003E\r\n            \u003Ctd id=\"loc-fb-pull-value\"\u003E ${parseInt(this.parameters.n_customer) + 4}\u003C\u002Ftd\u003E\r\n            \u003Ctd\u003E\u003C\u002Ftd\u003E\r\n          \u003C\u002Ftr\u003E\r\n          \u003Ctr\u003E\r\n            \u003Ctd id=\"loc-fb-time-label\"\u003EVerfügbare Zeit:\u003C\u002Ftd\u003E\r\n            \u003Ctd id=\"loc-fb-time-value\"\u003E${this.parameters.time_max} \u003C\u002Ftd\u003E\r\n            \u003Ctd\u003EMin.\u003C\u002Ftd\u003E\r\n          \u003C\u002Ftr\u003E\r\n          \u003Ctr\u003E\r\n            \u003Ctd id=\"loc-fb-load-label\"\u003EVerfügbare Menge:\u003C\u002Ftd\u003E\r\n            \u003Ctd id=\"loc-fb-load-value\"\u003E${this.parameters.load_max} \u003C\u002Ftd\u003E\r\n            \u003Ctd\u003EPort.\u003C\u002Ftd\u003E\r\n          \u003C\u002Ftr\u003E\r\n        \u003C\u002Ftable\u003E\r\n     \u003C\u002Fdiv\u003E\r\n\r\n     \u003Cdiv class=\"global-feedback\"\u003E\r\n      \u003Ch4\u003E Ergebnis aller Routen\u003C\u002Fh4\u003E\r\n      \u003Cdiv class=\"alert\"\u003E\r\n        \u003Ctable class=\"tab-global-feedback\"\u003E\r\n          \u003Ctr\u003E\r\n            \u003Ctd\u003EFertige Routen:\u003C\u002Ftd\u003E\r\n            \u003Ctd id=\"numRoutes\"\u003E\u003C\u002Ftd\u003E\r\n          \u003C\u002Ftr\u003E\r\n          \u003Ctr\u003E\r\n            \u003Ctd\u003E Mögliche Routen \u003C\u002Ftd\u003E\r\n            \u003Ctd id=\"potRoutes\"\u003E\u003C\u002Ftd\u003E\r\n          \u003C\u002Ftr\u003E\r\n\r\n        \u003C\u002Ftable\u003E\r\n      \u003C\u002Fdiv\u003E\r\n     \u003C\u002Fdiv\u003E\r\n    \u003Cdiv class=\"pause\"\u003E\r\n      \u003Cbutton disabled id=\"pause\" class=\"left\"\u003E\r\n        Pause \r\n      \u003C\u002Fbutton\u003E\r\n    \u003C\u002Fdiv\u003E\r\n    \u003Cdiv class=\"forward\"\u003E\r\n      \u003Cbutton id=\"continue\" class=\"right\"\u003E \r\n        Weiter\r\n      \u003C\u002Fbutton\u003E   \r\n    \u003C\u002Fdiv\u003E\r\n  \u003C\u002Fdiv\u003E\r\n\u003C\u002Fmain\u003E\r\n",
                  "timeline": []
                },
                {
                  "type": "lab.html.Form",
                  "content": "\n\u003Cimg class=\"interrupt center\" src=${this.files[study.interruptionContent.image]}\u003E\n\u003Cform class=\"center\"\u003E\n  \n\u003Ch3\u003E${study.interruptionContent.question}\u003C\u002Fh3\u003E \n\u003Cp\u003E Hinweis: Mehrere Antworten können richtig sein. \u003C\u002Fp\u003E\n  \u003Cfieldset\u003E\n    \u003Cul class=\"checkbox-interruption\"\u003E\n      \u003Cli\u003E \n        \u003Clabel\u003E\n          \u003Cinput type=\"checkbox\" name=\"optionA\" value=\"true\"\u003E\n          ${study.interruptionContent.optionA}\n        \u003C\u002Flabel\u003E\n      \u003C\u002Fli\u003E\n      \u003Cli\u003E \n        \u003Clabel\u003E\n           \u003Cinput type=\"checkbox\" name=\"optionB\" value=\"true\"\u003E\n              ${study.interruptionContent.optionB}\n        \u003C\u002Flabel\u003E\n      \u003C\u002Fli\u003E\n      \u003Cli\u003E  \n        \u003Clabel\u003E\n          \u003Cinput type=\"checkbox\" name=\"optionC\" value=\"true\"\u003E\n             ${study.interruptionContent.optionC}\n        \u003C\u002Flabel\u003E\n      \u003C\u002Fli\u003E\n    \u003C\u002Ful\u003E \n  \u003C\u002Ffieldset\u003E \n \n\u003C\u002Fform\u003E\n  \u003Cfooter\u003E \n    \u003Cform\u003E\n      \u003Cbutton class=\"right\" id=\"continueInterruption\" type=\"submit\"\u003E\n        Weiter\n      \u003C\u002Fbutton\u003E\n    \u003C\u002Fform\u003E\n\u003C\u002Ffooter\u003E",
                  "scrollTop": true,
                  "files": {
                    "1.1.02-131.jpg": "embedded\u002Fb90db14f689186415b67fe56cffce230abf029af9fef4fd5219c2e36a6efcfad.jpg",
                    "1.2.37-015.jpg": "embedded\u002Fe399db320c85f8aac479590980c8e654ae61c8fa5a9c375b607d09b8e9316023.jpg",
                    "1.3.01-022.jpg": "embedded\u002F277f974c105df4db7ab8bd9cefa094251d6b254251445343272856b9c8dfccde.jpg",
                    "1.3.01-111.jpg": "embedded\u002F2ffd15a3924e4d32e7905252095f3d535c442395372a9647d1190174eecb4469.jpg",
                    "1.4.41-137.jpg": "embedded\u002Fe823f1f3f57dbd61f0d05b3685fce36738bb6100f23e16d15a036413a7310f75.jpg"
                  },
                  "responses": {
                    "click button#continueInterruption": "continueInterruption"
                  },
                  "parameters": {},
                  "messageHandlers": {
                    "before:prepare": function anonymous(
) {
const response = this.options.datastore.state.response; 

this.options.skip = (
  response === 'end' || response === 'skip'  || 
  (study.configuration.declarative === 'easy' && response === 'continue'));

},
                    "after:end": function anonymous(
) {
console.log(this.options.datastore.state.response)
}
                  },
                  "title": "Form_InterruptionTask",
                  "tardy": true
                },
                {
                  "type": "lab.html.Screen",
                  "files": {},
                  "responses": {
                    "click button#continue": "continue",
                    "click button#end": "end"
                  },
                  "parameters": {},
                  "messageHandlers": {
                    "before:prepare": function anonymous(
) {
const response = this.options.datastore.state.response; 

this.options.skip = (
  study.configuration.problemState === "easy" ||
  study.configuration.declarative === "easy"|| 
    (
      study.configuration.declarative === "hard" && 
      (
        response === 'end' || 
        response === 'skip' || 
        response === 'continue')));

},
                    "run": function anonymous(
) {
let blockCounter = document.getElementById("block-count");
blockCounter.innerHTML = study.numBlock;
let trialCounter = document.getElementById("trial-count");
trialCounter.innerHTML = study.numPotRoutes;

// to exit loop
if (this.state.response === 'end'){
  this.parent.parent.parent.parent.end()
};
}
                  },
                  "title": "Screen_Constraints_before_Pause",
                  "content": "\u003Cmain class=\"experiment\"\u003E\r\n  \u003Cdiv class=wrapper\u003E\r\n\r\n    \u003Cdiv class=\"exit\"\u003E\r\n      \u003Cbutton id=\"end\" class=\"end left\" type=\"submit\"\u003E\r\n        Abbrechen\r\n      \u003C\u002Fbutton\u003E\r\n    \u003C\u002Fdiv\u003E\r\n\r\n  \u003Cdiv class=\"trial\"\u003E\r\n      \u003Ch4 class=\"trial\"\u003E\r\n        \u003Cspan id=\"block-count\"\u003E\u003C\u002Fspan\u003E. Block | \r\n        \u003Cspan id=\"trial-count\"\u003E\u003C\u002Fspan\u003E. Route\r\n      \u003C\u002Fh4\u003E\r\n  \u003C\u002Fdiv\u003E\r\n\r\n     \u003Cdiv class=\"constraints\"\u003E\r\n      \u003Ch4\u003EVorgaben\u003C\u002Fh4\u003E\r\n       \u003Ctable class=\"tab-constraints\"\u003E\r\n          \u003Ctr\u003E\r\n            \u003Ctd\u003EZiel:\u003C\u002Ftd\u003E\r\n            \u003Ctd\u003E${this.parameters.n_customer} \u003C\u002Ftd\u003E\r\n            \u003Ctd\u003E Kunden\u003C\u002Ftd\u003E\r\n          \u003C\u002Ftr\u003E\r\n          \u003Ctr\u003E\r\n            \u003Ctd\u003EVerfügbare Züge:\u003C\u002Ftd\u003E\r\n            \u003Ctd\u003E${parseInt(this.parameters.n_customer) + 5}\u003C\u002Ftd\u003E\r\n            \u003Ctd\u003E \u003C\u002Ftd\u003E\r\n          \u003C\u002Ftr\u003E\r\n          \u003Ctr\u003E\r\n            \u003Ctd\u003EVerfügbare Zeit:\u003C\u002Ftd\u003E\r\n            \u003Ctd\u003E${this.parameters.time_max}\u003C\u002Ftd\u003E\r\n            \u003Ctd\u003EMinuten\u003C\u002Ftd\u003E\r\n          \u003C\u002Ftr\u003E\r\n          \u003Ctr\u003E\r\n            \u003Ctd\u003EVerfügbare Menge:\u003C\u002Ftd\u003E\r\n            \u003Ctd\u003E${this.parameters.load_max}\u003C\u002Ftd\u003E\r\n            \u003Ctd\u003EPortionen\u003C\u002Ftd\u003E\r\n          \u003C\u002Ftr\u003E\r\n        \u003C\u002Ftable\u003E\r\n     \u003C\u002Fdiv\u003E\r\n\r\n    \u003Cdiv class=\"forward\"\u003E\r\n      \u003Cbutton id=\"continue\" class=\"right\"\u003E \r\n        Weiter\r\n      \u003C\u002Fbutton\u003E   \r\n    \u003C\u002Fdiv\u003E\r\n  \u003C\u002Fdiv\u003E\r\n\u003C\u002Fmain\u003E",
                  "tardy": true
                },
                {
                  "type": "lab.html.Screen",
                  "files": {
                    "rand_graph_0000.gv.png": "embedded\u002Fdbcf04b8e3309ce13f87a97d7a1dc20090b74499250e8775e7a3c1288cc9c1bc.png",
                    "rand_graph_0001.gv.png": "embedded\u002Fbf8e8a9b3b00590d3fa143135ed39abfa7730085d6de83709ff4dda2cdfad91b.png",
                    "rand_graph_0003.gv.png": "embedded\u002Fb4c0aa3034e7bc48cf40771839f691bd2da8ba3b67ec1779a05828e7c1c2bad8.png",
                    "rand_graph_0004.gv.png": "embedded\u002Fee3db615dac92a794bbe875e99dee27e2e0cdd3072bdf8f7c5728a45ac52ffec.png",
                    "rand_graph_0005.gv.png": "embedded\u002F593c495be8ef5adcfcf02a6ba19133620d68582c5b14539348bee1cc060f87e3.png",
                    "rand_graph_0006.gv.png": "embedded\u002F0f7500a30ec515674f4208de6bc93849f1beb334bf72fb296dbc98d13bdec358.png",
                    "rand_graph_0007.gv.png": "embedded\u002Fbebdf5317abe85c956fb5ba8cfaba7590f658ae9c7498e9cc0e9c57d5beb4cc9.png",
                    "rand_graph_7_0000.gv.png": "embedded\u002F8fe593bc06eba233e01f250b251204a31a8a30a60a7cddab9f51a627ff803e4b.png",
                    "rand_graph_7_0001.gv.png": "embedded\u002Fa4925f5e114f68289b17d6ca1c5acb7b7d3cd9b862eefc3f4eef7dfec6e3df52.png",
                    "rand_graph_0008.gv.png": "embedded\u002F9bd010485a15f39389fd6c1a73546d1d2eb3806bc4b52e125fba6c0f2ed8cc00.png",
                    "rand_graph_8_0000.gv.png": "embedded\u002F6f079b58104efdd85de276d42696c0bddc8211b0e2cff388643dd242f1818e9a.png",
                    "rand_graph_0011.gv.png": "embedded\u002F47852cd58fe8a66b200817379384a4c42c6c806341529b4e48f958de61695c08.png",
                    "rand_graph_0012.gv.png": "embedded\u002F9f83e0a294dfa17d7dbda828a377d91c66a07875c0754a24460b9fba8e2088a2.png",
                    "rand_graph_0014.gv.png": "embedded\u002F700085881f4f0c1be0c4757a618ccbb5b0b083d8381a3ff026c4c85ee4ca2e4f.png",
                    "rand_graph_0015.gv.png": "embedded\u002F36a5262b5466f738f30205aff3155c441acff39cb2e558a1e9fbaf8ec4799213.png",
                    "rand_graph_0020.gv.png": "embedded\u002Febfb39a80ea46eae9ce7c95b071f6ba1385a564976f73b656f5074a511a60345.png",
                    "rand_graph_0024.gv.png": "embedded\u002F0bdac2c38b5c1afafb5c2dc9a263f6a8da4109e3be416ad998bedc3a3a32ed87.png",
                    "rand_graph_0025.gv.png": "embedded\u002F593b4af410281dd78f5e50c97a6da2a204882efa0e26239b3f7e6c5c5ad468c6.png",
                    "rand_graph_0119.gv.png": "embedded\u002F0167745eb138b063a478b95384967e03fa44fcff510bd1289d2a2fa647c5d759.png"
                  },
                  "responses": {
                    "click button#end": "end"
                  },
                  "parameters": {},
                  "messageHandlers": {
                    "run": function anonymous(
) {
debugger;
//change layout according th selected factor level
if (study.configuration.declarative === "hard") {
		document.querySelector(".constraints").setAttribute('style','visibility:hidden');
	} else {
		document.querySelector(".constraints").removeAttribute('style');
	}

elementList = document.querySelectorAll("ol, img");
if (study.configuration.physical === "hard"){
  elementList.forEach(element => element.classList.add("phys--hard", "blur"));
  } else {
    elementList.forEach(element => element.classList.add("phys--easy"))
    };

// reload state of previous sreen
let itemPool = $('.list-items').children('li');

function resetList(wrapper, customOrder, itemPool){
  let items = wrapper.children('li');
  wrapper.append( $.map(customOrder, function(v){ return itemPool[v] }) );
}


/*let trialCounter = document.getElementById("trial-count");
let feedback = document.getElementById("feedback");
let fbNumRoutes = document.getElementById("numRoutes");
let fbPotRoutes = document.getElementById("potRoutes");*/

// make dom elements available for manipulations
let blockCounter = document.getElementById("block-count");
let trialCounter = document.getElementById("trial-count");
let fbNumRoutes = document.getElementById("numRoutes");
let fbPotRoutes = document.getElementById("potRoutes");
let feedback = document.getElementById("feedback");
study.feedback = feedback;

let locGoalLabel = document.getElementById("loc-fb-goal-label");
study.locGoalLabel = locGoalLabel;
let locGoalValue = document.getElementById("loc-fb-goal-value");
study.locGoalValue = locGoalValue;
let locTimeLabel = document.getElementById("loc-fb-time-label");
study.locTimeLabel = locTimeLabel;
let locTimeValue = document.getElementById("loc-fb-time-value");
study.locTimeValue = locTimeValue;
let locPullLabel = document.getElementById("loc-fb-pull-label");
study.locPullLabel = locPullLabel;
let locPullValue = document.getElementById("loc-fb-pull-value");
study.locPullValue = locPullValue;
let locLoadLabel = document.getElementById("loc-fb-load-label");
study.locLoadLabel = locLoadLabel;
let locLoadValue = document.getElementById("loc-fb-load-value");
study.locLoadValue = locLoadValue;
let tabVis = document.getElementById("loc-fb");
study.tabVis = tabVis;

blockCounter.innerHTML = study.numBlock;
trialCounter.innerHTML = study.numPotRoutes;
fbNumRoutes.innerHTML = study.numRoutes; 
fbPotRoutes.innerHTML = study.numPotRoutes -1;


// get number of customers
let numCustomer = Object.keys(study.nodes).length; //.toString();
study.numCustomer = numCustomer;
addMyMenu(numCustomer=study.numCustomer);

// define counter for number of sorting actions
const IMAGE = this.parameters.image;

// make lists sortable and save sort result and costs
$(function() {
    $('#sortable2').sortable({
        connectWith: '#sortable1',
        items: "li:not(.unsortable)", 
        placeholder: "ui-state-highlight",       
        over: function(event, ui) {
          if (study.configuration.problemState === "hard") {
            ui.helper.children($("span.list-item")).addClass(`invisible`)};
            }, 
       update: provideFeedback(numCustomer=numCustomer, image=IMAGE),
    }).disableSelection();
    $('#sortable1').sortable({
      opacity: 0.6,
      connectWith: '#sortable2', 
      placeholder: "ui-state-highlight",
      over: function(event, ui) {
              if (study.configuration.problemState === "hard"){ 
                ui.helper.children($("span.list-item")).removeClass(`invisible`)};
      },    
    }).disableSelection();
}); 	

$('#sortable2').on( "sortcreate", function() { 
      if (typeof study.conditions !== 'undefined'){
        resetList(wrapper=$('#sortable2'), customOrder=study.state.sorted2, itemPool=itemPool); 
        let feedbackAlertObject = giveFeedbackAlert(conditions=study.conditions); 
         study.feedback.innerHTML = feedbackAlertObject.messageContent;
         $(study.feedback).addClass(feedbackAlertObject.messageStyle); 
         let locFBValues =  giveFeedbackValues(conditions=study.conditions, configuration=study.configuration);
         if (typeof locFBValues !== 'undefined'){
            study.locGoalLabel.innerHTML = locFBValues.goalLabel;
            study.locGoalValue.innerHTML = locFBValues.goalValue;
            study.locTimeLabel.innerHTML = locFBValues.timeLabel;
            study.locTimeValue.innerHTML = locFBValues.timeValue;
            study.locLoadLabel.innerHTML = locFBValues.loadLabel;
            study.locLoadValue.innerHTML = locFBValues.loadValue;
            study.locPullLabel.innerHTML = locFBValues.pullLabel;
            study.locPullValue.innerHTML = locFBValues.pullValue;
            $(study.tabVis).addClass(locFBValues.tabVis)
            };       
        if (study.configuration.problemState === "hard"){ 
          let list2 = document.getElementById('sortable2').getElementsByTagName('span');
           list2.forEach(element => element.classList.add("invisible"));
        };
        /*  for (item of list2) {
         *  item.classList.add(`invisible`);
         * };};*/
        } else {
              let locFB = document.getElementById('loc-fb');
              locFB.classList.add("invisible");
            };
        });



// to exit loop
if (this.state.response === 'end'){
  this.parent.parent.parent.parent.parent.end()
};
},
                    "before:prepare": function anonymous(
) {
const response = this.options.datastore.state.response; 

this.options.skip = (
  study.configuration.problemState === 'easy' || 
  (study.configuration.problemState === 'hard' && 
  ((response === 'end' || response === 'skip' ) || 
  (study.configuration.declarative === 'easy' && response === 'continue'))));

  this.options.events['click button#continue'] = e => {
    if (typeof study.conditions !== "undefined" && isSolutionValid(conditions=study.conditions)) {
      // study.numPotCustomers = study.numPotCustomers + study.fbMinCustomers;
      study.numRoutes = ++study.numRoutes;
      // study.numSatCustomers = study.numSatCustomers + study.conditions.result.length-2;
      this.end();
      } else {
        alert("ungültige Route");
        e.preventDefault();
};
};

this.options.events['click button#skip'] = e => {
    this.options.datastore.state.response = 'skip';
    // study.numPotCustomers = study.numPotCustomers + study.fbMinCustomers;
    this.end();};
}
                  },
                  "title": "Screen_routing_Experimental_after_Pause",
                  "content": "\u003Cmain class=\"experiment\"\u003E\r\n  \u003Cdiv class=wrapper\u003E\r\n\r\n\r\n  \u003Cdiv class=\"trial\"\u003E\r\n      \u003Ch4 class=\"trial\"\u003E\r\n        \u003Cspan id=\"block-count\"\u003E\u003C\u002Fspan\u003E. Block | \r\n        \u003Cspan id=\"trial-count\"\u003E\u003C\u002Fspan\u003E. Route\r\n      \u003C\u002Fh4\u003E\r\n  \u003C\u002Fdiv\u003E\r\n\r\n    \u003Cdiv class=\"constraints\"\u003E\r\n      \u003Ch4\u003EVorgaben\u003C\u002Fh4\u003E\r\n       \u003Ctable class=\"tab-constraints\"\u003E\r\n          \u003Ctr\u003E\r\n            \u003Ctd\u003EZiel:\u003C\u002Ftd\u003E\r\n            \u003Ctd\u003E${this.parameters.n_customer} \u003C\u002Ftd\u003E\r\n            \u003Ctd\u003E Kunden\u003C\u002Ftd\u003E\r\n          \u003C\u002Ftr\u003E\r\n          \u003Ctr\u003E\r\n            \u003Ctd\u003EVerfügbare Züge:\u003C\u002Ftd\u003E\r\n            \u003Ctd\u003E${parseInt(this.parameters.n_customer) + 5}\u003C\u002Ftd\u003E\r\n            \u003Ctd\u003E \u003C\u002Ftd\u003E\r\n          \u003C\u002Ftr\u003E\r\n          \u003Ctr\u003E\r\n            \u003Ctd\u003EVerfügbare Zeit:\u003C\u002Ftd\u003E\r\n            \u003Ctd\u003E${this.parameters.time_max}\u003C\u002Ftd\u003E\r\n            \u003Ctd\u003EMinuten\u003C\u002Ftd\u003E\r\n          \u003C\u002Ftr\u003E\r\n          \u003Ctr\u003E\r\n            \u003Ctd\u003EVerfügbare Menge:\u003C\u002Ftd\u003E\r\n            \u003Ctd\u003E${this.parameters.load_max}\u003C\u002Ftd\u003E\r\n            \u003Ctd\u003EPortionen\u003C\u002Ftd\u003E\r\n          \u003C\u002Ftr\u003E\r\n        \u003C\u002Ftable\u003E\r\n     \u003C\u002Fdiv\u003E\r\n\r\n    \u003Cdiv class=\"graphdisplay\"\u003E\r\n        \u003Cimg class=\"graph\" src=${this.files[this.parameters.image]}\u003E\r\n    \u003C\u002Fdiv\u003E\r\n\r\n    \u003Cdiv class=\"skip\"\u003E\r\n      \u003Cbutton id=\"skip\" class=\"left\"\u003E\r\n        Überspringen \u003C\u002Fbutton\u003E\r\n    \u003C\u002Fdiv\u003E\r\n  \r\n    \u003Cdiv class=\"list1 left available\"\u003E\r\n        \u003Ch4\u003EVerfügbare Orte\u003C\u002Fh4\u003E\r\n        \u003Col id=\"sortable1\" class=\"connectedSortable list-items\"\u003E\r\n          \u003Cli id=\"1\" class=\"ui-state-default\"\u003E\u003Cspan class=\"list-item\"\u003E${this.parameters.customer_01}\u003C\u002Fspan\u003E\u003C\u002Fli\u003E\r\n          \u003Cli id=\"2\" class=\"ui-state-default\"\u003E\u003Cspan class=\"list-item\"\u003E${this.parameters.customer_02}\u003C\u002Fspan\u003E\u003C\u002Fli\u003E\r\n          \u003Cli id=\"3\" class=\"ui-state-default\"\u003E\u003Cspan class=\"list-item\"\u003E${this.parameters.customer_03}\u003C\u002Fspan\u003E\u003C\u002Fli\u003E\r\n          \u003Cli id=\"4\" class=\"ui-state-default\"\u003E\u003Cspan class=\"list-item\"\u003E${this.parameters.customer_04}\u003C\u002Fspan\u003E\u003C\u002Fli\u003E\r\n          \u003Cli id=\"5\" class=\"ui-state-default\"\u003E\u003Cspan class=\"list-item\"\u003E${this.parameters.customer_05}\u003C\u002Fspan\u003E\u003C\u002Fli\u003E\r\n          \u003Cli id=\"6\" class=\"ui-state-default\"\u003E\u003Cspan class=\"list-item\"\u003E${this.parameters.customer_06}\u003C\u002Fspan\u003E\u003C\u002Fli\u003E\r\n          \u003Cli id=\"7\" class=\"ui-state-default\"\u003E\u003Cspan class=\"list-item\"\u003E${this.parameters.customer_07}\u003C\u002Fspan\u003E\u003C\u002Fli\u003E\r\n          \u003Cli id=\"8\" class=\"ui-state-default\"\u003E\u003Cspan class=\"list-item\"\u003E${this.parameters.customer_08}\u003C\u002Fspan\u003E\u003C\u002Fli\u003E\r\n          \u003Cli id=\"9\" class=\"ui-state-default\"\u003E\u003Cspan class=\"list-item\"\u003E${this.parameters.customer_09}\u003C\u002Fspan\u003E\u003C\u002Fli\u003E\r\n          \u003Cli id=\"10\" class=\"ui-state-default\"\u003EMyMenu\u003C\u002Fli\u003E\r\n        \u003C\u002Fol\u003E\r\n    \u003C\u002Fdiv\u003E\r\n\r\n    \u003Cdiv class=\"list2 arranged\"\u003E\r\n        \u003Ch4\u003EGeplante Route\u003C\u002Fh4\u003E\r\n        \u003Col id=\"sortable2\" class=\"connectedSortable white-font\" start=\"0\"\u003E\r\n        \u003C\u002Fol\u003E\r\n    \u003C\u002Fdiv\u003E\r\n\r\n  \u003Cdiv class=\"exit\"\u003E\r\n      \u003Cbutton id=\"end\" class=\"end left\" type=\"submit\"\u003E\r\n        Abbrechen\r\n      \u003C\u002Fbutton\u003E\r\n    \u003C\u002Fdiv\u003E\r\n\r\n  \u003Cdiv class=\"feedback\"\u003E\r\n      \u003Ch4\u003EStand dieser Route\u003C\u002Fh4\u003E\r\n\r\n       \u003Cp id=\"feedback\"\u003E\u003C\u002Fp\u003E\r\n       \r\n       \u003Ctable id=\"loc-fb\" class=\"tab-local-feedback\"\u003E\r\n          \u003Ctr\u003E\r\n            \u003Ctd id=\"loc-fb-goal-label\"\u003E\u003C\u002Ftd\u003E\r\n            \u003Ctd id=\"loc-fb-goal-value\"\u003E\u003C\u002Ftd\u003E\r\n            \u003Ctd\u003E\u003C\u002Ftd\u003E\r\n          \u003C\u002Ftr\u003E\r\n          \u003Ctr\u003E\r\n            \u003Ctd id=\"loc-fb-pull-label\"\u003E\u003C\u002Ftd\u003E\r\n            \u003Ctd id=\"loc-fb-pull-value\"\u003E\u003C\u002Ftd\u003E\r\n            \u003Ctd\u003E\u003C\u002Ftd\u003E\r\n          \u003C\u002Ftr\u003E\r\n          \u003Ctr\u003E\r\n            \u003Ctd id=\"loc-fb-time-label\"\u003E\u003C\u002Ftd\u003E\r\n            \u003Ctd id=\"loc-fb-time-value\"\u003E\u003C\u002Ftd\u003E\r\n            \u003Ctd\u003E\u003C\u002Ftd\u003E\r\n          \u003C\u002Ftr\u003E\r\n          \u003Ctr\u003E\r\n            \u003Ctd id=\"loc-fb-load-label\"\u003E\u003C\u002Ftd\u003E\r\n            \u003Ctd id=\"loc-fb-load-value\"\u003E\u003C\u002Ftd\u003E\r\n            \u003Ctd\u003E\u003C\u002Ftd\u003E\r\n          \u003C\u002Ftr\u003E\r\n        \u003C\u002Ftable\u003E\r\n     \u003C\u002Fdiv\u003E\r\n\r\n\r\n    \u003Cdiv class=\"global-feedback\"\u003E\r\n      \u003Ch4\u003E Ergebnis aller Routen\u003C\u002Fh4\u003E\r\n      \u003Cdiv class=\"alert\"\u003E\r\n        \u003Ctable class=\"tab-global-feedback\"\u003E\r\n          \u003Ctr\u003E\r\n            \u003Ctd\u003EFertige Routen:\u003C\u002Ftd\u003E\r\n            \u003Ctd id=\"numRoutes\"\u003E\u003C\u002Ftd\u003E\r\n          \u003C\u002Ftr\u003E\r\n          \u003Ctr\u003E\r\n            \u003Ctd\u003E Mögliche Routen \u003C\u002Ftd\u003E\r\n            \u003Ctd id=\"potRoutes\"\u003E\u003C\u002Ftd\u003E\r\n          \u003C\u002Ftr\u003E\r\n        \u003C\u002Ftable\u003E\r\n      \u003C\u002Fdiv\u003E\r\n     \u003C\u002Fdiv\u003E\r\n    \r\n    \u003Cdiv class=\"pause\"\u003E\r\n      \u003Cbutton disabled id=\"pause\" class=\"left\"\u003E\r\n        Pause \r\n      \u003C\u002Fbutton\u003E\r\n    \u003C\u002Fdiv\u003E\r\n    \u003Cdiv class=\"forward\"\u003E\r\n      \u003Cbutton id=\"continue\" class=\"right\"\u003E \r\n        Weiter\r\n      \u003C\u002Fbutton\u003E   \r\n    \u003C\u002Fdiv\u003E\r\n    \r\n\u003C\u002Fdiv\u003E\r\n\u003C\u002Fmain\u003E\r\n",
                  "tardy": true
                }
              ]
            }
          },
          {
            "type": "lab.html.Screen",
            "files": {},
            "responses": {
              "click button#continue": "continue"
            },
            "parameters": {},
            "messageHandlers": {},
            "title": "Screen_Fragebogenhinweis",
            "content": "\u003Cheader\u003E\r\n\r\n  \u003Ch1\u003EFragebögen\u003C\u002Fh1\u003E\r\n\u003C\u002Fheader\u003E\r\n\r\n\u003Cmain\u003E\r\nBitte füllen Sie die Fragebögen aus.\r\nSetzen Sie den Versuch fort, indem Sie auf Weiter tippen.\r\n\u003C\u002Fmain\u003E\r\n\u003Cfooter\u003E \r\n\r\n\u003Cdiv class=\"btn-group right\"\u003E\r\n   \u003Cbutton id=\"continue\"\u003E\r\n    Weiter\r\n  \u003C\u002Fbutton\u003E\r\n\u003C\u002Fdiv\u003E\r\n\u003C\u002Ffooter\u003E"
          }
        ]
      }
    },
    {
      "type": "lab.html.Screen",
      "files": {},
      "responses": {
        "click button#continue": "continue"
      },
      "parameters": {},
      "messageHandlers": {},
      "title": "Screen_Ende",
      "content": "\u003Cheader\u003E\r\n\r\n  \u003Ch1\u003EEnde\u003C\u002Fh1\u003E\r\n\u003C\u002Fheader\u003E\r\n\r\n\u003Cmain\u003E\r\n  Vielen Dank, Ihre Aufgabe ist beendet. \r\nDer Versuchsleitende informiert Sie darüber, wie es weitergeht.\r\n\u003C\u002Fmain\u003E\r\n\u003Cfooter\u003E \r\n\r\n\u003Cdiv class=\"btn-group right\"\u003E\r\n   \u003Cbutton type=\"submit\" id=\"continue\"\u003E\r\n    Beenden\r\n  \u003C\u002Fbutton\u003E\r\n\u003C\u002Fdiv\u003E\r\n\u003C\u002Ffooter\u003E\r\n\r\n "
    },
    {
      "type": "lab.html.Screen",
      "files": {
        "rand_graph_0000.gv.png": "embedded\u002F995c4f70a584ee4ff9d3b7c944756cb53209a28026841a786eba06dfc157be81.png",
        "rand_graph_0001.gv.png": "embedded\u002F9bb330fbd3e262ffb17255352e64977a207adcfde9f2fce5fb6f8e865cdcdac6.png",
        "rand_graph_0003.gv.png": "embedded\u002Fa486c12da4d63cd57b548a9d1988b475017d4f5768a22194fe7fa2ea0e7e91d8.png",
        "rand_graph_0004.gv.png": "embedded\u002Fc5f65ddb3909549f3f5fd1f23109f2e7a9ac9d9074af9ab675aa9feb52a0337d.png",
        "rand_graph_0005.gv.png": "embedded\u002Fffc96931d9c268e37d13ebaa3d663336733afcbfabf7eccb1e6e3356188b6714.png",
        "rand_graph_0006.gv.png": "embedded\u002Fc8c177b52eb88b582e33fd322060ec8275312f7206a4f117cc6a28db009de7e8.png",
        "rand_graph_0007.gv.png": "embedded\u002F9e9a4471ec2733c63f0a384f3ae0b16b5776d6db4aa7aa60174bc7ca5faec076.png",
        "rand_graph_0008.gv.png": "embedded\u002Febe1c5b872bb47a105374a319aa6910b674b4c07f0390e9dab34e844af8c3b64.png",
        "rand_graph_0011.gv.png": "embedded\u002F456e10247cbb0de0707e7fcd92861bc40989b9788c81308c2143b2d15e009cda.png",
        "rand_graph_0012.gv.png": "embedded\u002Fbf71cfa136ff1184fe25576661299ca7de22c739d264e524ca4805b1b79af90d.png",
        "rand_graph_0014.gv.png": "embedded\u002F79ca0303210034dc385397ff04fffa1791ea0ed3f1318768cecd6c94202d2027.png",
        "rand_graph_0015.gv.png": "embedded\u002F19008f3467525f901f07a1160c221ce4f9ef041697f8c3af7b6698b745b3c26e.png",
        "rand_graph_0020.gv.png": "embedded\u002F8fc241c5a06b114194224d7073175446f49d9ad57f93b10d4014a9972611c9d5.png",
        "rand_graph_0024.gv.png": "embedded\u002F96b25020fb370056c23aad1cbccedc4145613b1fa4e0a3ec8fec5fc109f80dca.png",
        "rand_graph_0025.gv.png": "embedded\u002F115f5eda92771bbbdf2d59ed4289bfbccd2429e7d6ef761b70011a5fae4f6f76.png"
      },
      "responses": {
        "click(isSolutionValid(condition)) button#continue": "continue",
        "click button#end": "end"
      },
      "parameters": {},
      "messageHandlers": {},
      "title": "Screen_routing_Experimental_afterPause",
      "content": "\u003Cmain class=\"experiment\"\u003E\r\n  \u003Cdiv class=wrapper\u003E\r\n    \u003Cdiv class=\"exit\"\u003E\r\n      \u003Cbutton id=\"end\" class=\"end left\" type=\"submit\"\u003E\r\n        Experiment beenden\r\n      \u003C\u002Fbutton\u003E\r\n    \u003C\u002Fdiv\u003E\r\n\r\n    \u003Cdiv class=\"constraints\"\u003E\r\n      \u003Ch4\u003ERoutenvorgaben \u003C\u002Fh4\u003E\r\n       \u003Ctable class=\"tab-constraints\"\u003E\r\n          \u003Ctr\u003E\r\n            \u003Ctd\u003EZiel:\u003C\u002Ftd\u003E\r\n            \u003Ctd\u003E${this.parameters.n_customer} \u003C\u002Ftd\u003E\r\n            \u003Ctd\u003E Kunden\u003C\u002Ftd\u003E\r\n          \u003C\u002Ftr\u003E\r\n          \u003Ctr\u003E\r\n            \u003Ctd\u003EVerfügbare Zeit:\u003C\u002Ftd\u003E\r\n            \u003Ctd\u003E${this.parameters.time_max}\u003C\u002Ftd\u003E\r\n            \u003Ctd\u003EMinuten\u003C\u002Ftd\u003E\r\n          \u003C\u002Ftr\u003E\r\n          \u003Ctr\u003E\r\n            \u003Ctd\u003EFassungsvermögen:\u003C\u002Ftd\u003E\r\n            \u003Ctd\u003E${this.parameters.load_max}\u003C\u002Ftd\u003E\r\n            \u003Ctd\u003EPortionen\u003C\u002Ftd\u003E\r\n          \u003C\u002Ftr\u003E\r\n        \u003C\u002Ftable\u003E\r\n     \u003C\u002Fdiv\u003E\r\n\r\n    \u003Cdiv class=\"graphdisplay\"\u003E\r\n        \u003Cimg class=\"graph\" src=${this.files[this.parameters.image]}\u003E\r\n    \u003C\u002Fdiv\u003E\r\n  \r\n    \u003Cdiv class=\"list1 left available\"\u003E\r\n     \u003C!-- \u003Csection class=\"available\"\u003E--\u003E\r\n        \u003Ch4\u003EVerfügbare Orte\u003C\u002Fh4\u003E\r\n       \u003C!-- \u003Cul id=\"sortable1\" class=\"connectedSortable\"\u003E\r\n          \u003Cli id=\"1\" class=\"ui-state-default\"\u003E\u003Cspan class=\"list-item\"\u003E${this.parameters.customer_01}\u003C\u002Fspan\u003E\u003C\u002Fli\u003E\r\n          \u003Cli id=\"2\" class=\"ui-state-default\"\u003E\u003Cspan class=\"list-item\"\u003E${this.parameters.customer_02}\u003C\u002Fspan\u003E\u003C\u002Fli\u003E\r\n          \u003Cli id=\"3\" class=\"ui-state-default\"\u003E\u003Cspan class=\"list-item\"\u003E${this.parameters.customer_03}\u003C\u002Fspan\u003E\u003C\u002Fli\u003E\r\n          \u003Cli id=\"4\" class=\"ui-state-default\"\u003E\u003Cspan class=\"list-item\"\u003E${this.parameters.customer_04}\u003C\u002Fspan\u003E\u003C\u002Fli\u003E\r\n          \u003Cli id=\"5\" class=\"ui-state-default\"\u003E\u003Cspan class=\"list-item\"\u003E${this.parameters.customer_05}\u003C\u002Fspan\u003E\u003C\u002Fli\u003E\r\n          \u003Cli id=\"6\" class=\"ui-state-default\"\u003E\u003Cspan class=\"list-item\"\u003E${this.parameters.customer_06}\u003C\u002Fspan\u003E\u003C\u002Fli\u003E\r\n          \u003Cli id=\"7\" class=\"ui-state-default\"\u003E\u003Cspan class=\"list-item\"\u003E${this.parameters.customer_07}\u003C\u002Fspan\u003E\u003C\u002Fli\u003E\r\n          \u003Cli id=\"8\" class=\"ui-state-default\"\u003E\u003Cspan class=\"list-item\"\u003E${this.parameters.customer_08}\u003C\u002Fspan\u003E\u003C\u002Fli\u003E\r\n          \u003Cli id=\"9\" class=\"ui-state-default\"\u003E\u003Cspan class=\"list-item\"\u003E${this.parameters.customer_09}\u003C\u002Fspan\u003E\u003C\u002Fli\u003E\r\n          \u003Cli id=\"10\" class=\"ui-state-default\"\u003EMyMenu\u003C\u002Fli\u003E\r\n        \u003C\u002Ful\u003E--\u003E\r\n\u003Cul id=\"sortable\"\u003E\r\n  \u003Cli id=\"0\"\u003EItem 1\u003C\u002Fli\u003E\r\n  \u003Cli id=\"1\"\u003EItem 2\u003C\u002Fli\u003E\r\n  \u003Cli id=\"2\"\u003EItem 3\u003C\u002Fli\u003E\r\n  \u003Cli id=\"3\"\u003EItem 4\u003C\u002Fli\u003E\r\n\u003C\u002Ful\u003E\r\n      \u003C!--\u003C\u002Fsection\u003E--\u003E\r\n    \u003C\u002Fdiv\u003E\r\n\r\n    \u003Cdiv class=\"list2 arranged\"\u003E\r\n      \u003C!--\u003Csection class=\"arranged\"\u003E--\u003E\r\n        \u003Ch4\u003EGeplante Route\u003C\u002Fh4\u003E\r\n        \u003Cul id=\"sortable2\" class=\"connectedSortable white-font\"\u003E\r\n          \u003Cli id=\"10\" class=\"unsortable ui-state-default\"\u003EMyMenu\u003C\u002Fli\u003E\r\n        \u003C\u002Ful\u003E\r\n      \u003C!--\u003C\u002Fsection\u003E--\u003E\r\n    \u003C\u002Fdiv\u003E\r\n\r\n    \u003Cdiv class=\"feedback\"\u003E\r\n      \u003Ch4\u003E Zwischenstand dieser Route:\u003C\u002Fh4\u003E\r\n     \u003Cp id=\"feedback\" class=\"alert\"\u003ESie benötigen noch ${this.parameters.n_customer} Kunden. \u003Cbr\u003E\u003Cbr\u003E\r\n      ${this.parameters.time_max} Minuten verbleiben.\u003Cbr\u003E\r\n      ${this.parameters.load_max} Portionen sind übrig.\u003C\u002Fp\u003E\r\n    \u003C\u002Fdiv\u003E\r\n\r\n    \u003Cdiv class=\"global-feedback\"\u003E\r\n      \u003Ch4\u003E Ergebnis aller Routen: \u003C\u002Fh4\u003E\r\n      \u003Ctable class=\"tab-global-feedback alert\"\u003E\r\n          \u003Ctr\u003E\r\n            \u003Ctd\u003EFertige Routenpläne:\u003C\u002Ftd\u003E\r\n            \u003Ctd\u003E0\u003C\u002Ftd\u003E\r\n            \u003Ctd\u003E von \u003C\u002Ftd\u003E\r\n            \u003Ctd\u003EGesamt\u003C\u002Ftd\u003E\r\n          \u003C\u002Ftr\u003E\r\n          \u003Ctr\u003E\r\n            \u003Ctd\u003EZufriedene Kunden:\u003C\u002Ftd\u003E\r\n            \u003Ctd\u003E0\u003C\u002Ftd\u003E\r\n            \u003Ctd\u003E von \u003C\u002Ftd\u003E\r\n            \u003Ctd\u003EGesamt\u003C\u002Ftd\u003E\r\n          \u003C\u002Ftr\u003E\r\n        \u003C\u002Ftable\u003E \r\n     \u003C\u002Fdiv\u003E\r\n    \r\n    \u003Cdiv class=\"forward\"\u003E\r\n      \u003Cform onsubmit=\"isSolutionValid(conditions=study.conditions);\"\u003E\r\n      \u003Cbutton id=\"continue\" class=\"left\"\u003E \r\n        Weiter\r\n      \u003C\u002Fbutton\u003E   \u003C\u002Fform\u003E\r\n    \u003C\u002Fdiv\u003E\r\n\u003C\u002Fdiv\u003E\r\n\u003C\u002Fmain\u003E\r\n",
      "skip": true
    }
  ]
})

// Let's go!
study.run()